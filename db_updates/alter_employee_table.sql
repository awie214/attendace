ALTER TABLE `employees`
	ADD COLUMN `division_id` INT(11) NULL DEFAULT NULL AFTER `department_id`,
	ADD COLUMN `sector_id` INT(11) NULL DEFAULT NULL AFTER `division_id`;