ALTER TABLE `users`
	ADD COLUMN `sector_id` INT(11) NULL DEFAULT NULL AFTER `department_id`,
	ADD COLUMN `division_id` INT(11) NULL DEFAULT NULL AFTER `sector_id`;