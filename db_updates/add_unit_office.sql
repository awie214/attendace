ALTER TABLE `employees`
	ADD COLUMN `unit_id` INT(11) NULL DEFAULT NULL AFTER `sector_id`,
	ADD COLUMN `office_id` INT(11) NULL DEFAULT NULL AFTER `unit_id`;