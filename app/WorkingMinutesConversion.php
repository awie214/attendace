<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingMinutesConversion extends Model
{
    //
    protected $table = 'working_minutes_conversion';
    protected $fillables = array('minutes','equivalent','created_by');
    
}
