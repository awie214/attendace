<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\Report;


class ReportController extends Controller
{
    use Report;

    protected $reports = [
        'pds-form' => 'PDS Form',
        'service-record-form' => 'Service Record Form',
        'employee-age-and-birthday' => 'Employees Age and Birthdate',
        'employee-contact-information' => 'Employees Contact Information',
        'employee-civilstatus-by-gender' => 'Emplyoees Civil Status by Gender',
        'employee-age-bracket-by-gender' => 'Employees Age Bracket By Gender',
        'employee-government-id' => 'Employees Government IDs',
        'employee-length-of-service' => 'Employees Length of Service',
        
        
        'employee-administrative-offense' => 'List of Employees with Administrative Offense',
        'employee-with-consanguity' => 'List of Employees with Consanguity',
        'employee-with-disability' => 'List of Employees with Disability',
        'employee-with-formal-charges' => 'List of Employees with Formal Charges',
        'employee-with-indigenous-group' => 'List of Employees Member of Indigenous Group',


        'employee-list' => 'Employee List',
        'employee-masterlist' => 'Employees\' Master List',
        'employee-list-per-training' => 'List of Employees Per Training Program',
        'employee-training-list' => 'Employee\'s List of Trainings Programs',
        'employee-list-per-course' => 'List of Employees Per Course',
        'employee-course-list' => 'Employee Courses',
        'employee-list-per-school' => 'List of Employees Per School',
        'employee-school-list' => 'Employee Schools',
        'employee-list-per-eligibility' => 'List of Employees Per Eligibility',
        'employee-by-status' => 'Employee List',
        'birthday-celebrants' => 'Birthday Celebrants For The Month',
        'hired-employees' => 'Hired Employees',
        'resigned-employees' => 'Separated Employees',
        'employee-employment-information' => 'Employment Information',
        'keyword-finder' => 'Keyword Finder',
    ];


    protected $educational_level = [
        '1' => 'Primary Level',
        '2' => 'Secondary Level',
        '3' => 'Vocational / Trade Course',
        '4' => 'College',
        '5' => 'Graduate Studies'
    ];

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'report';

        $this->default_attribute = [
            'name' => strtolower(trans('page.name'))
        ];
    }

    public function index()
    {
        $option = 'report';
        $data = ['module' => $this->module, 'option' => $option];

        return view('report.index', [
            'reports' => $this->reports,
            'module' => $this->module,
            'option' => $option,
        ]);
    }

    public function view_report(request $request, $option) 
    {
        $department             = $request->input('department');
        $sector                 = $request->input('sector');
        $office                 = $request->input('office');
        $unit                   = $request->input('unit');
        $division               = $request->input('division');
        $employment_status      = $request->input('employment_status');
        $employee_id            = $request->input('employee_id');
        $month                  = $request->input('month');
        $year                   = $request->input('year');
        $tro                    = $request->input('tro');
        $report                 = $this->reports;
        $educational_level      = $this->educational_level;
        $report_title           = $report[$option];
        $data_array             = array();
        $data                   = $this->list_employee($department, $sector, $office, $unit, $division, $employment_status);
        

        if ($option == 'employee-civilstatus-by-gender')
        {
            $married_male       = $this->count_employee_by_civil_status_and_gender(1, "M");
            $married_female     = $this->count_employee_by_civil_status_and_gender(1, "F");

            $single_male        = $this->count_employee_by_civil_status_and_gender(2, "M");
            $single_female      = $this->count_employee_by_civil_status_and_gender(2, "F");

            $annulled_male      = $this->count_employee_by_civil_status_and_gender(3, "M");
            $annulled_female    = $this->count_employee_by_civil_status_and_gender(3, "F");

            $separated_male     = $this->count_employee_by_civil_status_and_gender(4, "M");
            $separated_female   = $this->count_employee_by_civil_status_and_gender(4, "F");

            $no_civil_status_no_gender = $this->count_employee_by_civil_status_and_gender('', '');

            $data = [
                'single_male' => $single_male,
                'single_female' => $single_female,
                'married_male' => $married_male,
                'married_female' => $married_female,
                'annulled_male' => $annulled_male,
                'annulled_female' => $annulled_female,
                'separated_male' => $separated_male,
                'separated_female' => $separated_female,
                'no_civil_status_no_gender' => $no_civil_status_no_gender
            ];
        }
        else if ($option == 'employee-masterlist')
        {
            $last_name              = $request->input('last_name');
            $first_name             = $request->input('first_name');
            $middle_name            = $request->input('middle_name');
            $mobile_number          = $request->input('mobile_number');
            $telephone_number       = $request->input('telephone_number');
            $birth_date             = $request->input('birth_date');
            $birth_place            = $request->input('birth_place');
            $email_address          = $request->input('email_address');
            $gender                 = $request->input('gender');
            $civil_status           = $request->input('civil_status');
            $height                 = $request->input('height');
            $weight                 = $request->input('weight');
            $blood_type             = $request->input('blood_type');
            $pagibig                = $request->input('pagibig');
            $gsis                   = $request->input('gsis');
            $philhealth             = $request->input('philhealth');
            $sss                    = $request->input('sss');
            $govt_issued_id         = $request->input('govt_issued_id');
            $govt_issued_id_number  = $request->input('govt_issued_id_number');
            $govt_issued_id_place   = $request->input('govt_issued_id_place');
            $residential_address    = $request->input('residential_address');
            $permanent_address      = $request->input('permanent_address');
            $training               = $request->input('training');
            $eligibility            = $request->input('eligibility');
            $education              = $request->input('education');
            $work_experience        = $request->input('work_experience');

            //$data = $this->list_employee_by($department, $sector, $office, $unit, $division, $employment_status);
            $data = $this->get_master_list($employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro);
        }
        else if (
            $option == 'employee-list-per-school' ||
            $option == 'employee-list-per-training' ||
            $option == 'employee-list-per-course' ||
            $option == 'employee-list-per-eligibility'
        )
        {
            $type       = explode("-", $option)[3];
            $value      = $request->input($type);
            $column     = $type.'_id';
            $educ_level = $request->input('educ_level');

            if ($type == 'eligibility')
            {
                $data_array = $this->get_data_by('eligibilities', $value);    
            }
            else
            {
                $data_array = $this->get_data_by($type.'s', $value);
            }

            
            $data       = $this->list_employee($department, $sector, $office, $unit, $division, $employment_status, $type, $value, $column, $educ_level);
            //dd($data);
            if ($type == 'school' || $type == 'course')
            {
                foreach ($data as $key => $value) {
                    $value->educ_level = $educational_level[$value->educ_level];
                    $course = $this->get_data_by('courses', $value->course_id);
                    $value->course_id = $course->name;
                }
            }
            else if ($type == 'training')
            {
                foreach ($data as $key => $value) {
                    $type_name  = $this->get_training_type($value->training_type);
                    $value->training_type = $type_name->name;
                }
            }
        }
        else if (
            $option == 'employee-school-list' ||
            $option == 'employee-training-list' ||
            $option == 'employee-course-list'
        )
        {
            $type           = explode("-", $option)[1];
            $employee_id    = $request->input('employee_id');
            $data_array     = $this->get_data_by('employees', $employee_id);
            if ($type == 'training')
            {
                $data = $this->list_trainings_attended($employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro);
            }
            else
            {
                $data = $this->list_education_background($employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro);   
                foreach ($data as $key => $value) {
                    $value->education_level = $educational_level[$value->education_level];
                } 
            }
            //dd($data);
        }
        else if ($option == 'employee-by-status')
        {
            $data = $this->list_employee_by_status(2);
        }
        else if (
            $option == 'birthday-celebrants' ||
            $option == 'employee-age-and-birthday'
        )
        {
            $data = $this->list_celebrants_by($request->input('month'));
        }
        else if ($option == 'hired-employees')
        {
            $month      = $request->input('month');
            $year       = $request->input('year');
            $start_date = $request->input('start_date');
            $end_date   = $request->input('end_date');
            $data = $this->get_report_data($employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro);
        }
        else if ($option == 'resigned-employees')
        {
            $month      = $request->input('month');
            $year       = $request->input('year');
            $start_date = $request->input('start_date');
            $end_date   = $request->input('end_date');
            $data = $this->list_resigned_employees_by($month, $year, $start_date, $end_date);
        }
        else if ($option == 'employee-employment-information')
        {
            $data = $this->list_employment_information_by($department, $sector, $office, $unit, $division, $employment_status);
        }
        else if ($option == 'keyword-finder')
        {
            $column         = '';
            $table          = $request->input('drp_keyword');
            $search_string  = $request->input('keyword');
            $keyword_list   = $this->keyword_finder($table, $search_string);
            if ($table == 'courses')
            {
                $column = 'course_id';
                $table2 = 'employee_education';
            }
            else if ($table == 'schools')
            {
                $column = 'school_id';
                $table2 = 'employee_education';
            }
            else if ($table == 'eligibilities')
            {
                $column = 'eligibility_id';
                $table2 = 'employee_eligibility';
            }
            else if ($table == 'trainings')
            {
                $column = 'training_id';
                $table2 = 'employee_training';
            }
            if (count($keyword_list) > 0)
            {
                $array = array();
                foreach ($keyword_list as $key => $value) {
                    $emp_list = $this->get_employee_list_by($table2, $column, $value->id);
                    $array[$value->name] = $emp_list;
                }    
                $data = ['list' => $array, 'drp_keyword' => $table, 'keyword' => $search_string];
            }
        }
        else if ($option == 'employee-list')
        {
            $data = $this->get_report_data($employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro);
            //dd($data);
        }
    	return view('report.'.$option, 
            [
                'data' => $data,
                'report_title' => $report_title,
                'request' => $request,
                'data_array' => $data_array
            ]
        );
        //return response(['data' => $employee_list]);
    }

    public function view_service_record_rpt(request $request, $id)
    {
        $data       = $this->list_service_record($id);
        $employee   = $this->get_employee_by($id);

        $certify = $this->get_signatory_by($request->input('certified_by'));

        $first_name = array($certify->first_name);

        if($certify->extension_name) $first_name[] = $certify->extension_name;

        $certified_by = implode(' ', $first_name).' '.$certify->middle_name[0].'. '.$certify->last_name;



        return view('report.service-record-form',['data' => $data, 'employee' => $employee, 'certified_by' => $certified_by, 'position_name' => $certify->position, 'department_name' => $certify->department, 'division_name' => $certify->division]); 
    }
}
