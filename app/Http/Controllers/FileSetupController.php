<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class FileSetupController extends Controller
{
    private $default_rule;

    private $default_attribute;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'file_setup';

        $this->default_rule = [
            'name' => 'required|max:255'
        ];

        $this->default_attribute = [
            'name' => strtolower(trans('page.name'))
        ];
    }

    public function index()
    {
        return view('file-setup.index', ['module' => $this->module, 'option' => null]);
    }

    public function file_setup($option)
    {
        // default table id
        $default_table_id = $option.'_tbl';

        // add condition for custom columns
        
        if($option == 'appointment_status')
        {
            $default_columns = array(['data' => 'action', 'sortable' => false],
            ['data' => 'name'],
            ['data' => 'appointment_type'],
            ['data' => 'remarks']);   
        }
        elseif($option == 'salary_grade')
        {
            $default_columns = array(['data' => 'action', 'sortable' => false],
            ['data' => 'name'],
            ['data' => 'step'],
            ['data' => 'tranche'],
            ['data' => 'amount']);
        }
        elseif($option == 'plantilla_items')
        {
            $default_columns = array(['data' => 'action', 'sortable' => false],
            ['data' => 'name'],
            ['data' => 'position_id'],
            ['data' => 'salary_grade'],
            ['data' => 'salary_step']);
        }
        elseif($option == 'cities')
        {
            $default_columns = array(['data' => 'action', 'sortable' => false],
            ['data' => 'name'],
            ['data' => 'province_id'],
            ['data' => 'zip_code']);
        }
        else
        {
            $default_columns = array(['data' => 'action', 'sortable' => false],
            ['data' => 'code'],
            ['data' => 'name'],
            ['data' => 'remarks']);
        }

        // default datatables json
        $default_json_url = url('/file-setup/datatables/'.$option);

        $view = 'file-setup.index';

        return view($view, ['module' => $this->module, 'option' => $option, 'default_table_id' => $default_table_id, 'default_columns' => $default_columns, 'default_json_url' => $default_json_url]);
    }

    public function file_setup_datatables($option)
    {
        if(in_array($option,
            [
                'agencies', 'departments','offices','plantilla_items','countries','cities',
                'occupations','citizenships','provinces','schools','courses','eligibilities',
                'organizations','providers','trainings','divisions','positions','appointment_status',
                'employment_status','salary_grade','branches','locations','sections','units',
                'absences','leaves','work_schedules','holidays','office_suspensions'
            ]
        ))
        {
            if($option == 'agencies')
            {
                $data = $this->list_agencies();
            }
            elseif($option == 'departments')
            {
                $data = $this->list_departments();
            }
            elseif($option == 'offices')
            {
                $data = $this->list_offices();
            }
            elseif($option == 'plantilla_items')
            {
                $data = $this->list_plantilla_items();
            }
            elseif($option == 'countries')
            {
                $data = $this->list_countries();
            }
            elseif($option == 'cities')
            {
                $data = $this->list_cities();
            }
            elseif ($option == 'occupations')
            {
                $data = $this->list_occupations();
            }
            elseif ($option == 'citizenships')
            {
                $data = $this->list_citizenships();
            }
            elseif ($option == 'provinces')
            {
                $data = $this->list_provinces();
            }
            elseif ($option == 'schools')
            {
                $data = $this->list_schools();
            }
            elseif ($option == 'courses')
            {
                $data = $this->list_courses();
            }
            elseif ($option == 'eligibilities')
            {
                $data = $this->list_eligibilities();
            }
            elseif ($option == 'providers')
            {
                $data = $this->list_providers();
            }
            elseif ($option == 'organizations')
            {
                $data = $this->list_organizations();
            }
            elseif ($option == 'trainings')
            {
                $data = $this->list_trainings();
            }
            elseif ($option == 'divisions')
            {
                $data = $this->list_divisions();
            }
            elseif ($option == 'positions')
            {
                $data = $this->list_positions();
            }
            elseif ($option == 'appointment_status')
            {
                $data = $this->list_appointment_status();
            }
            elseif ($option == 'employment_status')
            {
                $data = $this->list_employment_status();
            }
            elseif ($option == 'salary_grade')
            {
                $data = $this->list_salary_grade();
            }
            elseif ($option == 'branches')
            {
                $data = $this->list_branches();
            }
            elseif ($option == 'locations')
            {
                $data = $this->list_locations();
            }
            elseif ($option == 'sections')
            {
                $data = $this->list_sections();
            }
            elseif ($option == 'units')
            {
                $data = $this->list_units();
            }
            elseif ($option == 'absences')
            {
                $data = $this->list_absences();
            }
            elseif ($option == 'leaves')
            {
                $data = $this->list_leaves();
            }
            elseif ($option == 'holidays')
            {
                $data = $this->list_holidays();
            }
            elseif ($option == 'work_schedules')
            {
                $data = $this->list_work_schedules();
            }
            elseif ($option == 'office_suspensions')
            {
                $data = $this->list_office_suspensions();
            }
            else
            {
                $data = array();
            }

            return Datatables::of($data)
            ->addColumn('action', function($data) use ($option) {
                return '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                
                                <a href="'.url('/file-setup/'.$option.'/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <span class="dropdown-item" onclick="delete_record('.$data->id.');">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </span>
                            </div>
                        </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
        } 
    }

    public function create_file_setup($option)
    {
        $view = 'file-setup.create';
        if ($option == 'plantilla_items')
        {
            $positions = $this->list_positions();   
            return view($view, ['module' => $this->module, 'option' => $option, 'positions' => $positions]);
        }
        else
        {
            return view($view, ['module' => $this->module, 'option' => $option]);
        }
        
    }

    public function get_record($option, $id)
    {
        $table = $option;
        $id    = $id;
        $data = $this->get_table_record($table, $id);
        return response(['data' => $data]);
    }

    public function delete_record($option, $id)
    {
        $table = $option;
        $id    = $id;

        try
        {
            DB::beginTransaction();

            DB::table($option)
            ->where('id', '=',$request->get('id'))
            ->update([
            'deleted_at' => DB::raw('now()')
            ]); 

            DB::commit();
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response('success', 204);
    }
    
    public function edit_file_setup($option, $id)
    {
        $view = 'file-setup.edit';

        if ($option == 'plantilla_items')
        {
            $positions = $this->list_positions();   
            return view($view, ['module' => $this->module, 'option' => $option, 'positions' => $positions, 'id' => $id]);
        }
        else
        {
            return view($view, ['module' => $this->module, 'option' => $option, 'id' => $id, 'title' => 'file_setup']);
        }
        
    }

    public function update_file_setup(request $request, $option)
    {
        try
        {
            DB::beginTransaction();

            DB::table($option)
                ->where('id', '=', $request->get('id'))
                ->update([
                'code' => $request->get('code'),
                'name' => $request->get('name'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ]); 

            DB::commit();
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response('success', 204);
    }
    public function store_file_setup(request $request, $option)
    {
        try 
        {  
            $rule = $this->default_rule;
            $attribute_name = $this->default_attribute;

            /*if($option == 'test')
            {
                // merge 
            }
            else
            {
                $rule = array_merge([
                    'code' => 'max:255'
                ], $rule);

                $attribute_name = array_merge([
                    'code' => strtolower(trans('page.code'))
                ], $attribute_name);
            }*/

            $this->validate_request($request->all(), $rule, $attribute_name);

            DB::beginTransaction();

            if($option == 'agencies')
            {   
                DB::table('agencies')
                ->insert([
                'code' => $request->get('code'),
                'name' => $request->get('name'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ]); 
            }
            elseif ($option == 'departments') 
            {
                DB::table('departments')
                ->insert([
                'code' => $request->get('code'),
                'name' => $request->get('name'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ]); 
            } 
            elseif ($option == 'offices') 
            {
                DB::table('offices')
                ->insert([
                'code' => $request->get('code'),
                'name' => $request->get('name'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ]); 
            }

            DB::commit();
        } 
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return back()->with('danger', $e->getMessage());
            
            return response(['errors' => $data], 422);
        }
    }
}
