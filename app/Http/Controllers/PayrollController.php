<?php

namespace App\Http\Controllers;

use App\Http\Traits\Payroll;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class PayrollController extends Controller
{
    use Payroll;

    public function __construct()
    {
        $this->middleware('auth');
        $this->module = 'payroll';
    }


    public function index($option)
    {
        $array = array();
        $folder = $option;
        $json_url = '';
        $columns = '';
        if ($option == 'loans-info')
        {
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'loan_name'],
                ['data' => 'amortization'],
                ['data' => 'start_date'],
                ['data' => 'end_date'],
            );
        }
        else if ($option == "benefits-info")
        {
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'effectivity_date'],
                ['data' => 'benefit_name'],
                ['data' => 'amount'],
                ['data' => 'pay_period'],
            );
        }
        else if ($option == "deductions-info")
        {
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'start_date'],
                ['data' => 'deduction_name'],
                ['data' => 'amount'],
                ['data' => 'pay_period'],
            );
        }
        else if ($option == 'employee-policy')
        {
            $array['banks'] = $this->list_banks();
            $array['gsis_policy'] = $this->list_gsispolicy();
            $array['philhealth_policy'] = $this->list_philhealthpolicy();
            $array['pagibig_policy'] = $this->list_pagibigpolicy();
            $array['responsibility_center'] = $this->list_responsibilitycenters();

        }
        $data = ['module' => $this->module, 'option' => $option, 'json_url' => $json_url, 'columns' => $columns, 'array' => $array];
        $file = 'payroll.'.$folder.'.index';
        return view($file, $data);
    }

    public function payroll_datatables($option, $employee_id)
    {
        if ($option == 'loans-info')
        {
            $data = $this->list_employees_loan($employee_id);
        }
        else if ($option == 'benefits-info')
        {
            $data = $this->list_employees_benefit($employee_id);   
        }
        else if ($option == 'deductions-info')
        {
            $data = $this->list_employees_deduction($employee_id);   
        }
        else
        {
            $data = array();
        }
        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) use ($option) {
           return '<div class="tools">
	                    <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
	                    <div role="menu" class="dropdown-menu" x-placement="bottom-start">
	                        <a href="'.url('/attendance/'.$option.'/'.$data->id.'/edit').'" class="dropdown-item">
	                            <i class="icon icon-left mdi mdi-edit"></i>Edit
	                        </a>
	                        <span class="dropdown-item" onclick="delete_record(\''.$option.'\','.$data->id.')">
	                            <i class="icon icon-left mdi mdi-delete"></i>Delete
	                        </span>
	                    </div>
	                </div>';
        });

        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }

    public function employee_setup_create($option, $employee_id)
    {
    	$array = array();
    	if ($option == 'loans-info')
    	{
    		$array = $this->list_loans();
    	}
        else if ($option == 'benefits-info')
        {
            $array = $this->list_benefits();
        }
        else if ($option == 'deductions-info')
        {
            $array = $this->list_deductions();
        }
    	$data = ['module' => $this->module, 'option' => $option, 'employee_id' => $employee_id, 'array' => $array];
        $file = 'payroll.'.$option.'.create';
        return view($file, $data);
    }

    public function store(Request $request, $option)
    {

        try {
            if ($option == 'loans-info')
            {
                $table              = 'employees_loan';
                $employee_id        = $request->get('employee_id');
                $loan_id           	= $request->get('loan_id');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $terminated        	= $request->get('terminated');
                $date_granted     	= $request->get('date_granted');
                $date_terminated  	= $request->get('date_terminated');
                $total_loan         = $request->get('total_loan');
                $loan_balance       = $request->get('loan_balance');
                $amortization       = $request->get('amortization');

                
                DB::beginTransaction();
                $insert_data = [
                    'employee_id' => $employee_id,
                    'loan_id' => $loan_id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'date_granted' => $date_granted,
                    'date_terminated' => $date_terminated,
                    'terminated' => $terminated,
                    'total_loan' => $total_loan,
                    'loan_balance' => $loan_balance,
                    'amortization' => $amortization,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                DB::table($table)
                ->insert($insert_data);
                DB::commit();
            }  
            else if ($option == 'benefits-info')
            {
                $table              = 'employees_benefit';
                $employee_id        = $request->get('employee_id');
                $benefit_id         = $request->get('benefit_id');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $effectivity_date   = $request->get('effectivity_date');
                $amount             = $request->get('amount');
                $description        = $request->get('description');

                
                DB::beginTransaction();
                $insert_data = [
                    'employee_id' => $employee_id,
                    'benefit_id' => $benefit_id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'amount' => $amount,
                    'description' => $description,
                    'effectivity_date' => $effectivity_date,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                DB::table($table)
                ->insert($insert_data);
                DB::commit();
            }

            else if ($option == 'deductions-info')
            {
                $table              = 'employees_deduction';
                $employee_id        = $request->get('employee_id');
                $deduction_id       = $request->get('deduction_id');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $amount             = $request->get('amount');
                $date_terminated    = $request->get('date_terminated');
                $terminated         = $request->get('terminated');

                
                DB::beginTransaction();
                $insert_data = [
                    'employee_id' => $employee_id,
                    'deduction_id' => $deduction_id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'amount' => $amount,
                    'date_terminated' => $date_terminated,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                DB::table($table)
                ->insert($insert_data);
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201);
    }
}
