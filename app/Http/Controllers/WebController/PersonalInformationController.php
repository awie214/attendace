<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use App\Http\Traits\Employee;
use App\Http\Traits\Dashboard;
use App\User;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class PersonalInformationController extends Controller
{
    use Employee, Dashboard;

    protected $reports_pds = [
        'employee-list' => 'Employee List',
        'employee-age-and-birthday' => 'Employees\' Age and Birthdate',
        'employee-masterlist' => 'Employees\' Master List',
        'employee-employment-information' => 'Employment Information',
        'employee-school-list' => "Employees' Schools",
        'employee-training-list' => "Employee's List of Trainings Programs",
        'employee-list-per-training' => 'List of Employees Per Training Program',
        'employee-list-per-course' => 'List of Employees Per Course',
        'employee-list-per-school' => 'List of Employees Per School',
        'employee-list-per-eligibility' => 'List of Employees Per Eligibility',
        
        /*'employee-contact-information' => 'Employees Contact Information',*/
        'employee-civilstatus-by-gender' => 'Employees Civil Status by Sex',
        'employee-age-bracket-by-gender' => 'Employees Age Bracket By Sex',
        'employee-government-id' => 'Employees Government IDs',
        'resigned-employees' => 'Separated Employees',
        'hired-employees' => 'Hired Employees',
        'keyword-finder' => 'Keyword Finder',
        //'employee-course-list' => 'Employees\'s Courses',
        /*'employee-length-of-service' => 'Employees Length of Service',
        
        'employee-administrative-offense' => 'List of Employees with Administrative Offense',
        'employee-with-consanguity' => 'List of Employees with Consanguity',
        'employee-with-disability' => 'List of Employees with Disability',
        'employee-with-formal-charges' => 'List of Employees with Formal Charges',
        'employee-with-indigenous-group' => 'List of Employees Member of Indigenous Group',*/
    ];

    protected $columns_list = [
        'employee_number' => 'Employee No.',
        'last_name' => 'Last Name',
        'first_name' => 'First Name',
        'middle_name' => 'Middle Name',
        'middle_initial' => 'Middle Initial',
        'mobile_number' => 'Mobile Number',
        'telephone_number' => 'Telephone Number',
        'birth_date' => 'Birth Date',
        'birth_place' => 'Birth Place',
        'email_address' => 'Email Address',
        'gender' => 'Sex',
        'civil_status' => 'Civil Status',
        'height' => 'Height',
        'weight' => 'Weight',
        'blood_type' => 'Blood Type',
        'pagibig' => 'PAGIBIG',
        'gsis' => 'GSIS',
        'philhealth' => 'PHILHEALTH',
        'sss' => 'SSS',
        'tin' => 'TIN',
        /*'govt_issued_id' => 'Govt\'t Issued ID',
        'govt_issued_id_number' => 'Govt\'t Issued ID Number',
        'govt_issued_id_place' => 'Govt\'t Issued ID Place',*/
        'residential_address' => 'Residential Address',
        'permanent_address' => 'Permanent Address',
        'position_name' => 'Position Title',
        'plantilla_item_name' => 'Item No.',
        'sector_name' => 'Sector',
        'department_name' => 'Department',
        'unit_name' => 'Unit',
        'office_name' => 'Entity',
        'salary_grade' => 'Salary Grade',
        'salary_step' => 'Salary Step',
        'monthly_salary' => 'Basic Salary'
    ];

    private $personal_info_attribute;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'personal_information';
    
        $this->personal_info_attribute = [
        'last_name' => strtolower(trans('page.surname')),
        'first_name' => strtolower(trans('page.first_name')),
        'middle_name' => strtolower(trans('page.middle_name')),
        'extension_name' => strtolower(trans('page.ext_name')),
        'sex' => strtolower(trans('page.sex')),
        'civil_status' => strtolower(trans('page.civil_status')),
        'other_civil_status' => strtolower(trans('page.please_specify')),
        'birth_date' => strtolower(trans('page.birth_date')),
        'birth_place' => strtolower(trans('page.birth_place')),
        'height' => strtolower(trans('page.height')),
        'blood_type' => strtolower(trans('page.blood_type')),
        'gsis' => strtolower(trans('page.gsis_id_no')),
        'pagibig' => strtolower(trans('page.pagibig_id_no')),
        'philhealth' => strtolower(trans('page.philhealth_no')),
        'sss' => strtolower(trans('page.sss_no')),
        'tin' => strtolower(trans('page.tin_no')),
        'telepone_no' => strtolower(trans('page.tel_no')),
        'mobile_no' => strtolower(trans('page.mobile_no')),
        'email_address' => strtolower(trans('page.email_address')),
        'is_filipino' => strtolower(trans('page.is_filipino')),
        'dual_citizenship' => strtolower(trans('page.dual_citizenship')),
        'residential_house_no' => strtolower(trans('page.house_blk_lot_no')),
        'residential_street' => strtolower(trans('page.street')),
        'residential_subdivision' => strtolower(trans('page.subdivision')),
        'residential_barangay' => strtolower(trans('page.barangay')),
        'same_as_above' => strtolower(trans('page.same_as_above')),
        'permanent_house_no' => strtolower(trans('page.house_blk_lot_no')),
        'permanent_street' => strtolower(trans('page.street')),
        'permanent_subdivision' => strtolower(trans('page.subdivision')),
        'permanent_barangay' => strtolower(trans('page.barangay')),
        'employee_number'  => strtolower(trans('page.agency_emp_no')),
        ];
    }

    public function index(request $request, $option)
    {
        try
        {
            $this->is_personal_information_option_exist($option);

            $data = ['module' => $this->module, 'option' => $option];

            if(Auth::user()->isUser())
            {
                $data['employee_id'] = Auth::user()->employee_id;
            }

            if($option == 'dashboard0')
            {
                $month                      = date('m',time());
                $year                       = date('Y',time());
                $assignment_array           = array();
                $employement_status         = array();

                $count_male                 = $this->count_by('gender', 'M');
                $count_female               = $this->count_by('gender', 'F');
                $count_undefined            = $this->count_by('gender', null);

                $count_plantilla            = $this->count_by('employment_category', '1');
                $count_non_plantilla        = $this->count_by('employment_category', '0');
                $count_undefined_plantilla  = $this->count_by('employment_category', null);

                $latest_announcement        = $this->latest_announcement();
                $get_active_quotation       = $this->get_active_quotation();

                $birthday_celebrants        = $this->count_celebrants_by($month);
                $assignment_list            = $this->list_assignments();

                
                foreach($this->list_employment_status() as $key => $val)
                {
                    $name = 'count_'.strtolower(str_replace(' ', '_', $val->name));

                    $employement_status[$name] = $this->count_by('employment_status', $val->id);
                }

                $place_of_assignment = array();

                foreach($this->list_assignments() as $key => $val)
                {
                    $place_of_assignment[] = array('field' => $val->name, 'count' => $this->count_by('place_of_assignnment', $val->id));
                }

                $data = array_merge($data, [
                        'module' => $this->module, 
                        'count_male' => $count_male, 
                        'count_female' => $count_female, 
                        'count_undefined' => $count_undefined, 
                        'count_plantilla' => $count_plantilla, 
                        'count_non_plantilla' => $count_non_plantilla, 
                        'count_undefined_plantilla' => $count_undefined_plantilla, 
                        'latest_announcement' => $latest_announcement, 
                        'count_undefined_employment_status' => $this->count_by('employment_status', null), 
                        'get_active_quotation' => $get_active_quotation,
                        'birthday_celebrants' => $birthday_celebrants,
                        'hired_employees' => $this->count_hired_employees_by($month, $year),
                        'resign_employees' => $this->count_resigned_employees_by($month, $year),
                        'place_of_assignment' => $place_of_assignment
                    ]);

                $data = array_merge($employement_status, $data);



                $file = 'personal_information.dashboard.index';
            }
            elseif($option == '201_file')
            {
                $data = array_merge($data, ['default_table_id' => 'employees_tbl', 'default_json_url' => url('/personal_information/datatables/'.$option), 'default_columns' => array(['data' => 'action', 'sortable' => false], ['data' => 'employee_number'], ['data' => 'last_name'], ['data' => 'first_name']), 'file' => 'personal_information.pds_file.201_file.table']);

                $file = 'personal_information.pds_file.index';
            }
            elseif($option == '201_update')
            {
                $data = array_merge($data, ['default_table_id' => 'employees_update_tbl', 'default_json_url' => url('/personal_information/datatables/'.$option), 'default_columns' => array(['data' => 'action', 'sortable' => false], ['data' => 'eu_created_at'], ['data' => 'employee_number'], ['data' => 'field_name'], ['data' => 'old_value'], ['data' => 'new_value']), 'file' => 'personal_information.pds_file.201_update.table']);

                $file = 'personal_information.pds_file.index';
            }
            elseif($option == 'pds_attachment')
            {
                $data = array_merge($data, ['employees' => $this->list_employees(), 'file' => 'personal_information.pds_file.201_attachment.table', 'columns' => array(['data' => 'action', 'sortable' => false], ['data' => 'file_name'], ['data' => 'file_type'], ['data' => 'remarks'])]);

                if($request->input('id')) $data['employee_id'] = $request->input('id');

                $file = 'personal_information.pds_file.index';
            }
            elseif($option == 'pds_file')
            {
                $data = array_merge($data, ['employees' => $this->list_employees(), 'file' => 'personal_information.pds_file.profile.profile_form']);

                if($request->input('id')) $data['employee_id'] = $request->input('id');

                $file = 'personal_information.pds_file.index';
            }
            elseif($option == 'employee_information')
            {
                $data = array_merge($data, ['employees' => $this->list_employees()]);

                $file = 'personal_information.employee_information.index';
            }
            elseif($option == 'employee_service_record')
            {
                if($request->input('id')) $data['employee_id'] = $request->input('id');
                
                $data = array_merge($data, ['columns' => array(['data' => 'action', 'sortable' => false], ['data' => 'position_name'], ['data' => 'agency_name'], 
                    //[ 'data' => 'workexp_start_date', 'type' => 'num', 'render' => [ "_" => 'display', "sort" => 'timestamp' ] ]
                    [ 'name' => 'workexp_start_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'workexp_start_date.display', "sort" => 'workexp_start_date' ] ],
                    [ 'name' => 'workexp_end_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'workexp_end_date.display', "sort" => 'workexp_end_date' ] ]), 'signatories' => $this->list_signatories()]);

                $file = 'personal_information.employee_service_record.index';
            }
            elseif($option == 'profile')
            {
                $data = array_merge($data, ['employees' => $this->list_employees(), 'file' => 'personal_information.pds_file.profile.profile_form']);

                if($request->input('id')) $data['employee_id'] = $request->input('id');

                $file = 'personal_information.pds_file.index';
            }
            elseif($option == 'change_rate')
            {
                $request->session()->forget('info');

                if($request->input('id')) $data['employee_id'] = $request->input('id');

                $data = array_merge($data, ['default_table_id' => 'change_rate_tbl','employees' => $this->list_employees(), 'default_columns' => array(['data' => 'action', 'sortable' => false], [ 'name' => 'effective_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'effective_date.display', "sort" => 'effective_date' ] ], ['data' => 'new_rate', 'class' => 'text-right']), 'file' => 'personal_information.hr_action.change_rate.table']);

                $file = 'personal_information.hr_action.index';
            }
            elseif($option == 'reassignment')
            {
                if($request->input('id')) $data['employee_id'] = $request->input('id');

                $data = array_merge($data, ['default_table_id' => 'reassignment_tbl','employees' => $this->list_employees(), 'default_columns' => array(['data' => 'action', 'sortable' => false], [ 'name' => 'effective_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'effective_date.display', "sort" => 'effective_date' ] ], ['data' => 'reassignment_no']), 'file' => 'personal_information.hr_action.reassignment.table']);

                $file = 'personal_information.hr_action.index';
            }
            elseif($option == 'designation')
            {
                if($request->input('id')) $data['employee_id'] = $request->input('id');

                $data = array_merge($data, ['default_table_id' => 'designation_tbl','employees' => $this->list_employees(), 'default_columns' => array(['data' => 'action', 'sortable' => false], [ 'name' => 'effective_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'effective_date.display', "sort" => 'effective_date' ] ]), 'file' => 'personal_information.hr_action.designation.table']);

                $file = 'personal_information.hr_action.index';
            }
            elseif($option == 'renewal')
            {

                $data = array_merge($data, 
                    [
                        'default_table_id' => 'renewal_tbl', 
                        'default_json_url' => url('/personal_information/renewal/datatables/'),
                        'default_columns' => array(
                            ['data' => 'action', 'sortable' => false], 
                            ['data' => 'employee_name'], 
                            ['name' => 'start_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'start_date.display', "sort" => 'start_date' ] ],
                            ['name' => 'end_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'end_date.display', "sort" => 'end_date' ] ]
                        ), 
                        'file' => 'personal_information.renewal.index'
                    ]);

                $file = 'personal_information.renewal.index';
            }
            elseif($option == 'report')
            {
                $data = array_merge($data, [
                    'reports' => $this->reports_pds,
                    'columns_list' => $this->columns_list,
                    'departments' => $this->list_departments(),
                    'offices' => $this->list_offices(),
                    'divisions' => $this->list_divisions(),
                    'employment_status' => $this->list_employment_status(),
                    'sectors' => $this->list_sectors(),
                    'units' => $this->list_units(),
                    'schools' => $this->list_schools(),
                    'courses' => $this->list_courses(),
                    'trainings' => $this->list_trainings(),
                    'employees' => $this->list_employees(),
                    'eligibilities' => $this->list_eligibilities(),
                ]);

                $file = 'personal_information.report.index';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        return view($file, $data);
    }

    public function view_personal_info(request $request, $option, $id)
    {
        try
        {
            $this->is_personal_information_option_exist($option);

            $default_data = ['module' => $this->module, 'option' => $option, 'action' => 'view'];

            if($option == 'pds_file')
            {
                if($selected_target = $request->input('selected_target')) $default_data['selected_target'] = $selected_target;
                
                $data = array_merge($default_data, ['module' => $this->module, 'option' => $option, 'file' => 'personal_information.pds_file.201_file.show', 'fieldnames' => $this->fieldnames,  'employee_id' => $id, 'list_cities' => $this->list_cities(), 'list_provinces' => $this->list_provinces(), 'list_bloodtypes' => $this->list_bloodtypes(), 'list_civilstatus' => $this->list_civilstatus(), 'list_occupations' => $this->list_occupations(), 'list_countries' => $this->list_countries()]);

                $file = 'personal_information.pds_file.view';
            }
            elseif($option == 'employee_service_record')
            {
                $data = array_merge($default_data, ['agencies' => $this->list_agencies(), 'departments' => $this->list_departments(), 'offices' => $this->list_offices(), 'plantilla_items' => $this->list_plantilla_items(), 'divisions' => $this->list_divisions(), 'positions' => $this->list_positions(), 'appointment_status' => $this->list_appointment_status(), 'employment_status' => $this->list_employment_status(), 'branches' => $this->list_branches(), 'locations' => $this->list_locations(), 'sections' => $this->list_sections(), 'sectors' => $this->list_sectors(), 'units' => $this->list_units(), 'service_record' => $this->get_work_experience($id), 'list_sections' => $this->list_sections(), 'appointment_type' => $this->list_appointment_type(), 'fs_salary_grade' => $this->salary_grade, 'fs_salary_step' => $this->step, 'service_record_id' => $id]);

                $file = 'personal_information.employee_service_record.view';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }

        return view($file, $data);
    }

    public function create_personal_info(request $request, $option)
    {
        try
        {
            $this->is_personal_information_option_exist($option);

            $default_data = ['module' => $this->module, 'option' => $option];

            if($option == '201_file')
            {
                $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_file.personal_info_form', 'list_cities' => $this->list_cities(), 'list_provinces' => $this->list_provinces(), 'list_bloodtypes' => $this->list_bloodtypes(), 'list_civilstatus' => $this->list_civilstatus(), 'list_countries' => $this->list_countries()]);

                $file = 'personal_information.pds_file.create';
            }   
            elseif($option == '201_update')
            {
                $id = $request->input('id');

                $data = array_merge($default_data, ['module' => $this->module, 'option' => $option, 'file' => 'personal_information.pds_file.201_update.add_form', 'list_employees' => $this->list_employees(), 'fieldnames' => $this->fieldnames,  'employee_id' => $id, 'list_schools' => $this->list_schools()]);

                $file = 'personal_information.pds_file.create';
            }
            elseif($option == 'employee_service_record')
            {
                $id = $request->input('id');

                if($this->count_employee_by($id) == 0) throw new Exception(trans('page.no_record_found'));

                $data = array_merge($default_data, ['agencies' => $this->list_agencies(), 'departments' => $this->list_departments(), 'offices' => $this->list_offices(), 'plantilla_items' => $this->list_plantilla_items(), 'divisions' => $this->list_divisions(), 'positions' => $this->list_positions(), 'appointment_status' => $this->list_appointment_status(), 'employment_status' => $this->list_employment_status(), 'salary_grade' => $this->list_salary_grade(), 'branches' => $this->list_branches(), 'locations' => $this->list_locations(), 'sections' => $this->list_sections(), 'departments' => $this->list_departments(), 'sectors' => $this->list_sectors(), 'units' => $this->list_units(), 'count_record' => $this->count_service_record($id), 'employee' => $this->get_employee($id), 'data' => $this->get_latest_workexp($id), 'employee_id' => $id, 'list_sections' => $this->list_sections(), 'list_sectors' => $this->list_sectors(), 'appointment_type' => $this->list_appointment_type(), 'fs_salary_grade' => $this->salary_grade, 'fs_salary_step' => $this->step, 'list_employees' => $this->list_employees()]);

                $file = 'personal_information.employee_service_record.create';
            }
            elseif($option == 'pds_attachment')
            {   
                $id = $request->input('id');

                if($this->count_employee_by($id) == 0) throw new Exception(trans('page.no_record_found'));
                
                $default_data['this_url'] = url('personal_information/');

                if(Auth::user()->isUser())
                {
                    $default_data['this_url'] = url('user/personal_information/');
                }
                
                $data = array_merge($default_data, ['module' => $this->module, 'option' => $option, 'file' => 'personal_information.pds_file.201_attachment.add_form', 'employee_id' => $id]);

                $file = 'personal_information.pds_file.create';
            }
            elseif($option == 'pds_file')
            {
                $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_file.personal_info_form', 'list_cities' => $this->list_cities(), 'list_provinces' => $this->list_provinces(), 'list_bloodtypes' => $this->list_bloodtypes(), 'list_civilstatus' => $this->list_civilstatus(), 'list_countries' => $this->list_countries()]);

                //$data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_file.new_employee_form', 'list_employment_status' => $this->list_employment_status(), 'list_divisions' => $this->list_divisions(), 'list_sectors' => $this->list_sectors(), 'list_departments' => $this->list_departments(), 'list_units' => $this->list_units(), 'list_plantilla_items' => $this->list_plantilla_items()]);

                $file = 'personal_information.pds_file.create';
            }
            elseif($option == 'change_rate')
            {
                $id = $request->input('id');

                $employee = $this->check_exist_employee($id);

                $default_data['this_url'] = url('personal_information/');

                $data = array_merge($default_data, ['file' => 'personal_information.hr_action.change_rate.form', 'employee' => $employee, 'employee_id' => $id]);

                $file = 'personal_information.hr_action.create';

                $request->session()->flash('info', 'Adding new change of rate may apply to the latest work experience and employment information.');
            }
            elseif($option == 'reassignment')
            {
                $id = $request->input('id');

                $employee = $this->check_exist_employee($id);

                $default_data['this_url'] = url('personal_information/');

                $data = array_merge($default_data, ['file' => 'personal_information.hr_action.reassignment.form', 'employee' => $employee, 'employee_id' => $id, 'list_sectors' => $this->list_sectors(), 'list_departments' => $this->list_departments(), 'list_divisions' => $this->list_divisions(), 'list_units' => $this->list_units(), 'list_offices' => $this->list_offices(), 'list_designations' => $this->list_designations()]);

                $file = 'personal_information.hr_action.create';
            }
            elseif($option == 'designation')
            {
                $id = $request->input('id');

                $employee = $this->check_exist_employee($id);

                $default_data['this_url'] = url('personal_information/');

                //$data = array_merge($default_data, ['file' => 'personal_information.hr_action.designation.form', 'employee' => $employee, 'employee_id' => $id, 'list_positions' => $this->list_positions()]);

                $data = array_merge($default_data, ['file' => 'personal_information.hr_action.designation.form', 'employee' => $employee, 'employee_id' => $id, 'list_positions' => $this->list_positions(), 'list_sectors' => $this->list_sectors(), 'list_departments' => $this->list_departments(), 'list_divisions' => $this->list_divisions(), 'list_units' => $this->list_units(), 'list_offices' => $this->list_offices(), 'list_designations' => $this->list_designations()]);

                $file = 'personal_information.hr_action.create';
            }
            elseif ($option == 'renewal') 
            {
                $data = array_merge($default_data, ['file' => 'personal_information.renewal.form', 'cancel_url' => 'personal_information.renewal.index', 'frm_action' => url('/personal_information/'.$option.'/store')]);
                $file = 'personal_information.renewal.create';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        return view($file, $data);
    }

    public function store_personal_info(request $request, $option)
    {
        try
        {
            $this->is_personal_information_option_exist($option);

            $lastID = '';

            if($option == 'pds_file')
            {
                $validate_cities = implode(',', $this->get_value_by($this->list_cities(), 'id'));
                $validate_bloodtypes = implode(',', $this->get_value_by($this->list_bloodtypes(), 'id'));
                $validate_civilstatus = implode(',', $this->get_value_by($this->list_civilstatus(), 'id'));

                $rule = [
                'last_name' => 'required|max:255',
                'first_name' => 'required|max:255',
                'middle_name' => 'sometimes|nullable|max:255',
                'extension_name' => 'sometimes|nullable|max:255',
                'sex' => 'sometimes|nullable|in:M,F',
                'civil_status' => 'in:'.$validate_civilstatus,
                'other_civil_status' => 'sometimes|nullable|max:255',
                'birth_date' => 'sometimes|nullable',
                'birth_place' => 'required|max:255',
                'height' => 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/',
                'weight' => 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/',
                'blood_type' => 'sometimes|nullable|in:'.$validate_bloodtypes,
                'gsis' => 'sometimes|nullable|max:255',
                'pagibig' => 'sometimes|nullable|max:255',
                'philhealth' => 'sometimes|nullable|max:255',
                'sss' => 'sometimes|nullable|max:255',
                'tin' => 'sometimes|nullable|max:255',
                'employee_number' => 'sometimes|nullable|max:255',
                'telepone_no' => 'sometimes|nullable|max:255',
                'mobile_no' => 'sometimes|nullable|max:255',
                'email_address' => 'sometimes|nullable|email',
                'citizenship' => 'sometimes|nullable|in:is_filipino,is_dual'];

                $citizenship = $request->get('citizenship');

                if($citizenship == 'is_dual')
                {
                    $rule['dual_citizenship'] = 'required|in:by_birth,by_naturalization';
                    $rule['indicate_details'] = 'sometimes|nullable|max:50';
                }

                $rule = array_merge($rule, ['residential_house_no' => 'sometimes|nullable|max:255',
                'residential_street' => 'sometimes|nullable|max:255',
                'residential_subdivision' => 'sometimes|nullable|max:255',
                'residential_barangay' => 'sometimes|nullable|max:255',
                'residential_city' => 'sometimes|nullable|in:'.$validate_cities,
                'same_as_above' => 'in:0,1',
                'permanent_house_no' => 'sometimes|nullable|max:255',
                'permanent_street' => 'sometimes|nullable|max:255',
                'permanent_subdivision' =>  'sometimes|nullable|max:255',
                'permanent_barangay' => 'sometimes|nullable|max:255',
                'permanent_city' => 'sometimes|nullable|in:'.$validate_cities,
                ]);

                $this->validate_request($request->all(), $rule, $this->personal_info_attribute);

                $residential_house_no = $request->get('residential_house_no');
                $residential_street = $request->get('residential_street');
                $residential_subdivision = $request->get('residential_subdivision');
                $residential_barangay = $request->get('residential_barangay');
                $residential_zip_code = $request->get('residential_zip_code');
                $residential_province = null;

                if($residential_city = $request->get('residential_city'))
                {
                    $residential_city_info = $this->select_city_by($residential_city);
                    $residential_province = $residential_city_info->province_id;
                }   


                $same_as_above = $request->get('same_as_above');

                $permanent_house_no = $request->get('permanent_house_no');
                $permanent_street = $request->get('permanent_street');
                $permanent_subdivision = $request->get('permanent_subdivision');
                $permanent_barangay = $request->get('permanent_barangay');
                $permanent_zip_code = $request->get('permanent_zip_code');
                $permanent_province = null;

                if($permanent_city = $request->get('permanent_city'))
                {
                    $permanent_city_info =  $this->select_city_by($permanent_city);
                    $permanent_province = $permanent_city_info->province_id;
                }

                if($same_as_above == 1)
                {
                    $permanent_house_no = $residential_house_no;
                    $permanent_street = $residential_street;
                    $permanent_subdivision = $residential_subdivision;
                    $permanent_barangay = $residential_barangay;
                    $permanent_city = $residential_city;
                    $permanent_province = $residential_province;
                    $permanent_zip_code = $residential_zip_code; 
                }

                $civil_status = $request->get('civil_status');
                $other_civil_status = $request->get('other_civil_status');

                if($civil_status !== 5) 
                {
                    $other_civil_status = null;
                }

                $insert_data = [
                'last_name' => strtoupper($request->get('last_name')),
                'first_name' => strtoupper($request->get('first_name')),
                'middle_name' => strtoupper($request->get('middle_name')),
                'extension_name' => strtoupper($request->get('extension_name')),
                'birthday' => date("Y-m-d", strtotime($request->get('birth_date'))),
                'birth_place' => $request->get('birth_place'),
                'email_address' => $request->get('email_address'),
                'gender' => $request->get('sex'),
                'civil_status_id' => $civil_status,
                'other_civil_status' => $other_civil_status,
                'height' => $request->get('height'),
                'weight' => $request->get('weight'),
                'blood_type_id' => $request->get('blood_type'),
                'gsis' => $request->get('gsis'),
                'pagibig' => $request->get('pagibig'),
                'philhealth' => $request->get('philhealth'),
                'sss' => $request->get('sss'),
                'tin' => $request->get('tin'),
                'employee_number' => $request->get('employee_number'),
                'telephone_number' => $request->get('telepone_no'),
                'mobile_number' => $request->get('mobile_no'),
                'residential_house_number' => $residential_house_no,
                'residential_street' => $residential_street,
                'residential_subdivision' => $residential_subdivision,
                'residential_brgy' => $residential_barangay,
                'residential_city_id' => $residential_city,
                'residential_province_id' => $residential_province,
                'residential_zip_code' =>  $residential_zip_code,
                'residential_country_id' => 1,
                'permanent_house_number' => $permanent_house_no,
                'permanent_street' => $permanent_street,
                'permanent_subdivision' => $permanent_subdivision,
                'permanent_brgy' => $permanent_barangay,
                'permanent_city_id' => $permanent_city,
                'permanent_province_id' => $permanent_province,
                'permanent_zip_code' => $permanent_zip_code,
                'permanent_country_id' => 1,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                if($citizenship == 'is_filipino')
                {
                    $insert_data['filipino'] = '1';
                    $insert_data['naturalized'] = null;
                    $insert_data['naturalized_details'] = null;
                }
                elseif($citizenship == 'is_dual')
                {
                    $insert_data['filipino'] = null;

                    if($request->get('dual_citizenship') == 'by_birth')
                    {
                        $insert_data['naturalized'] = '0';
                    }
                    elseif($request->get('dual_citizenship') == 'by_naturalization')
                    {
                        $insert_data['naturalized'] = '1';
                    }

                    $insert_data['naturalized_details'] = $request->get('indicate_details');
                }
                else
                {
                    $insert_data['filipino'] = null;
                    $insert_data['naturalized'] = null; 
                    $insert_data['naturalized_details'] = null;  
                }

                DB::beginTransaction();

                DB::table('employees')
                ->insert($insert_data);

                $lastID = DB::getPdo()->lastInsertId();

                DB::table('employee_informations')
                ->insert([
                'employee_id' => $lastID,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()'),
                ]);

                /**
                DB::table('users')
                ->insert([
                'name' => $request->get('employee_number'),
                'username' => $request->get('employee_number'),
                'password' => bcrypt('123456'),
                'employee_id' => $lastID,
                'level' => 2,
                'created_at' => DB::raw('now()')
                ]);
                **/

                DB::commit();
            }
            elseif($option == '201_update')
            {
                $emp_id = $request->get('emp_id');
                $form = $request->get('form');

                if(!$employee = $this->check_exist_employee_by($emp_id)) throw new Exception(trans('page.no_record_found'));

                if(!in_array($form, ['personal', 'add_children', 'remove_children', 'add_education', 'remove_education'])) throw new Exception(trans('page.no_record_found'));

                if($form == 'personal')
                {
                    $field_name = $request->get('field_name');
            
                    $new_value = $request->get('new_value');
                    if($field_name == 'civil_status_id') $new_value_others = $request->get('new_value_others');
                    
                    $rule = array(
                    'field_name' => 'required|in:'.implode(',', array_column($this->fieldnames, 'field_name'))
                    );

                    $update_employee = [];
                    $update_employee_family = [];
                    $insert_employee_updates = [];

                    if(in_array($field_name, ['last_name', 'first_name', 'middle_name', 'birth_place', 'employee_number', 'residential_house_no', 'residential_barangay', 'permanent_house_no', 'permanent_barangay']))
                    {
                        $rule['new_value'] = 'required|max:255';
                        $update_employee[$field_name] = strtoupper($new_value); 
                    }
                    elseif(in_array($field_name, ['birthday']))
                    {
                        $rule['new_value'] = 'required'; // validate with regex
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['email_address']))
                    {
                        $rule['new_value'] = 'required|email';
                    }
                    elseif(in_array($field_name, ['extension_name', 'nickname', 'height', 'weight', 'gsis', 'pagibig', 'philhealth', 'sss', 'tin', 'telephone_number', 'mobile_number', 'contact_number', 'residential_street', 'residential_subdivision', 'permanent_street', 'permanent_subdivision', 'father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                    {
                        $rule['new_value'] = 'sometimes|nullable|max:255';

                        if(in_array($field_name, ['father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                        {       
                            $update_employee_family[$field_name] = strtoupper($new_value);
                        }
                        else
                        {
                            $update_employee[$field_name] = strtoupper($new_value);
                        }
                    }
                    elseif(in_array($field_name, ['civil_status_id']))
                    {
                        $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_civilstatus(), 'id') );
                        $update_employee[$field_name] = $new_value;

                        if($new_value == '5')
                        {
                            $rule['new_value_others'] = 'required|max:255';
                            $update_employee[$field_name] = $new_value;
                            $update_employee['other_civil_status'] = $new_value_others;

                            $insert_employee_updates['new_value'] = $new_value_others;
                        } 
                    }
                    elseif(in_array($field_name, ['gender']))
                    {
                        $rule['new_value'] = 'in:'.implode(',', config('params.sex'));
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['blood_type_id']))
                    {
                        $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_bloodtypes(), 'id'));
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['filipino']))
                    {
                        $rule['new_value'] = 'sometimes|nullable|in:0,1';
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['occupation_id']))
                    {
                        $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_occupations(), 'id'));
                        $update_employee_family[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['spouse_telephone_number']))
                    {
                        $rule['new_value'] = 'sometimes|nullable|max:255';
                        $update_employee_family['telephone_number'] = $new_value;
                    }
                    elseif(in_array($field_name, ['residential_city_id', 'permanent_city_id']))
                    {
                        $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_cities(), 'id'));
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['image_path']))
                    {
                        $rule['new_value'] = 'required|image|mimes:jpeg,png,jpg,jpeg|max:5048';
                    }

                    $this->validate_request($request->all(), $rule, $this->personal_info_attribute);

                    $old_value = $employee->$field_name;

                    if($field_name == 'civil_status_id')
                    {
                        if($old_value == 5) $old_value = $employee->other_civil_status;
                    }

                    $insert_employee_updates = array_merge($insert_employee_updates, [
                    'field_name' => $field_name,
                    'employee_id' => $emp_id,
                    'old_value' => $old_value,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ]);

                    if(in_array($field_name, ['residential_city_id', 'permanent_city_id']))
                    {
                        $selected_city = $this->select_city_by($new_value);

                        if($field_name == 'residential_city_id')
                        {
                            $update_employee['residential_province_id'] = $selected_city->province_id;
                            $update_employee['residential_zip_code'] = $selected_city->zip_code;
                        }
                        elseif($field_name == 'permanent_city_id')
                        {
                            $update_employee['permanent_province_id'] = $selected_city->province_id;
                            $update_employee['permanent_zip_code'] = $selected_city->zip_code;
                        }
                    }
                    elseif(in_array($field_name, ['image_path']))
                    {
                        $file = Input::File('new_value');
                        $file_token = Str::random(50);
                        $file_extension = $file->getClientOriginalExtension();
                        $file_name = 'avatar_'.$file_token.'.'.$file_extension;

                        $insert_employee_updates['new_value'] = $file_name;

                        $folder_path = public_path().'/tieza/attachment/'.$emp_id;

                        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                        $request->file('new_value')->move($folder_path, $file_name);

                        $update_employee['image_path'] = $file_name;


                    }

                    DB::beginTransaction();

                    if(!empty($update_employee))
                    {
                        DB::table('employees')
                        ->where('employees.id', '=', $emp_id)
                        ->update($update_employee);
                    }
                    
                    if(!empty($update_employee_family))
                    {
                        DB::table('employee_family')
                        ->where('employee_family.employee_id', '=', $emp_id)
                        ->update($update_employee_family);
                    }

                    if(!array_key_exists('new_value', $insert_employee_updates)) $insert_employee_updates['new_value'] = $new_value;

                    DB::table('employee_updates')
                    ->insert($insert_employee_updates);

                    $lastID = $employee;

                    DB::commit();
                }
                elseif($form == 'add_children')
                {
                    $rule = [
                    'children_name' => 'required|max:255',
                    'children_birthday' => 'required',
                    'children_gender' => 'in:,'.implode(',', array_keys(config('params.sex'))),
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_children')
                    ->insert([
                    'employee_id' => $request->get('emp_id'),
                    'child_name' => $request->get('children_name'),
                    'child_birthday' => $request->get('children_birthday'),
                    'child_gender' => $request->get('children_gender'),
                    'created_by' => Auth::user()->id
                    ]);
      
                    DB::commit();
                } 
                elseif($form == 'remove_children')
                {
                    DB::beginTransaction();

                    DB::table('employee_children')
                    ->where('employee_children.id', $request->get('children_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()'),
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'add_education')
                {
                    $rule = [
                    'education_level' => 'required|in:1,2,3,4,5',
                    'education_school' => 'in:'.implode(',', $this->get_value_by($this->list_schools(), 'id')),
                    'education_course' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_courses(), 'id')),
                    'education_start_year' => 'required',
                    'education_end_year' => 'required',
                    'education_present' => 'required|in:0,1',
                    'education_year_graduated' => 'sometimes|nullable',
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_education')
                    ->insert([
                    'employee_id' => $request->get('emp_id'),
                    'education_level' => $request->get('education_level'),
                    'school_id' => $request->get('education_school'),
                    'course_id' => $request->get('education_course'),
                    'start_year' => $request->get('education_start_year'),
                    'end_year' => $request->get('education_end_year'),
                    'year_graduated' => $request->get('education_present') == 1 ? 0 : $request->get('education_year_graduated'),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'remove_education')
                {
                    DB::beginTransaction();

                    DB::table('employee_education')
                    ->where('employee_education.id', $request->get('eduaction_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()'),
                    ]);
      
                    DB::commit();
                }
            }
            elseif($option == 'employee_service_record')
            {
                $employee_id = $request->get('employee_id');

                if(!$employee = $this->check_exist_employee_by($employee_id)) throw new Exception(trans('page.no_record_found'));

                $former_incumbent = $request->get('former_incumbent');
                $workexp_start_date = $request->get('workexp_start_date');
                $workexp_end_date = $request->get('workexp_end_date');
                $workexp_present = $request->get('workexp_present');
                $date_of_assumption = $request->get('date_of_assumption');

                $plantilla_item = $request->get('plantilla_item');
                $position = $request->get('position');
                $agency = $request->get('agency');
                $sector = $request->get('sector');
                $department = $request->get('department');
                $office = $request->get('office');
                $division = $request->get('division');
                $branch = $request->get('branch');
                $section = $request->get('section');
                $unit = $request->get('unit');
                $location = $request->get('location');
 
                $employment_status = $request->get('employment_status');
                $nature_of_appointment = $request->get('nature_of_appointment');
                $pay_rate = $request->get('pay_rate');
                $salary_grade = $request->get('salary_grade');
                $salary_step = $request->get('salary_step');
                $salary_amount = str_replace(',', '', $request->get('salary_amount'));
                $lwop = $request->get('lwop');
                $lwop_date = $request->get('lwop_date');
                $seperated_date = $request->get('seperated_date');
                $reason = $request->get('reason');

                $rule = [
                'workexp_start_date' => 'required',
                'workexp_present' => 'in:0,1',
                ];

                if($workexp_present == 0)
                {
                    $rule['workexp_end_date'] = 'required';
                }
                
                $rule = array_merge($rule, [
                'plantilla_item' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_plantilla_items(), 'id')),
                'position' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_positions(), 'id')),
                'agency' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_agencies(), 'id')),
                'branch' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_branches(), 'id')),
                'location' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_locations(), 'id')),
                'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id')),
                'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                
                'section' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sections(), 'id')),
                'unit' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_units(), 'id')),
                'employment_status' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_employment_status(), 'id')),
                'nature_of_appointment' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_appointment_status(), 'id')),

                'pay_rate' => 'required|in:'.implode(',', array_keys(config('params.pay_rates'))),
                'salary_grade' => 'sometimes|nullable|in:'.implode(',', $this->salary_grade),
                'salary_step' => 'sometimes|nullable|in:'.implode(',', $this->step),
                'salary_amount' => 'required',
                'lwop' => 'sometimes|nullable|max:50',
                'reason' => 'sometimes|nullable|max:255',
                ]);

                $this->validate_request($request->all(), $rule);

                DB::beginTransaction();

                $insert_data = [
                'employee_id' => $employee_id,
                'workexp_start_date' => $workexp_start_date,
                'workexp_present' => $workexp_present,
                'appointment_status_id' => $nature_of_appointment,
                'plantilla_item_id' => $plantilla_item,
                'position_id' => $position,
                'agency_id' => $agency,
                'department_id' => $department,
                'office_id' => $office,
                'division_id' => $division,
                'branch_id' => $branch,
                'section_id' => $section,
                'sector_id' => $sector,
                'unit_id' => $unit,
                'location_id' => $location,
                'employment_status_id' => $employment_status,
                'pay_rate' => $pay_rate,
                'salary_grade' => $salary_grade,
                'salary_step' => $salary_step,
                'monthly_salary' => $salary_amount,
                'lwop' => $lwop,
                'lwop_date' => $lwop_date,
                'seperated_date' => $seperated_date,
                'reason' => $reason,
                'government_service' => 1,
                'former_incumbent' => $former_incumbent,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                if($workexp_present == 0)
                {
                    $insert_data['workexp_end_date'] = $workexp_end_date;
                }
                elseif($workexp_present == 1)
                {
                    DB::table('employee_work_experience')
                    ->where('employee_work_experience.employee_id', $employee_id)
                    ->where('employee_work_experience.workexp_present', '1')
                    ->where('employee_work_experience.deleted_at', null)
                    ->update(['workexp_end_date' => null]);

                    DB::table('employee_work_experience')
                    ->where('employee_work_experience.employee_id', $employee_id)
                    ->where('employee_work_experience.deleted_at', null)
                    ->update(['workexp_present' => '0']);

                    $empinfo_data = [
                        'appointment_status_id' => $nature_of_appointment,
                        'plantilla_item_id' => $plantilla_item,
                        'position_id' => $position,
                        'agency_id' => $agency,
                        'department_id' => $department,
                        'office_id' => $office,
                        'division_id' => $division,
                        'branch_id' => $branch,
                        'section_id' => $section,
                        'sector_id' => $sector,
                        'unit_id' => $unit,
                        'location_id' => $location,
                        'employment_status_id' => $employment_status,
                        'salary_grade' => $salary_grade,
                        'salary_step' => $salary_step,
                        'monthly_salary' => $salary_amount,
                        'assumption_date' => $date_of_assumption
                    ];

                    if($this->count_empinfo_by($employee_id) == 1)
                    {
                        $empinfo_data = array_merge($empinfo_data, [
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                        ]);

                        DB::table('employee_informations')
                        ->where('employee_id', '=', $employee_id)
                        ->update($empinfo_data);
                    }
                    else
                    {
                        $empinfo_data = array_merge($empinfo_data, [
                        'employee_id' => $employee_id,
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                        ]);

                        DB::table('employee_informations')
                        ->insert($empinfo_data);
                    }

                }

                DB::table('employee_work_experience')
                ->insert($insert_data);

                $lastID = DB::getPdo()->lastInsertId();

                DB::commit();
            }
            elseif($option == 'pds_attachment')
            {
                $rule = [
                'file_name' => 'required|max:255',
                'remarks' => 'sometimes|nullable|max:255',
                'file' => 'required|mimes:jpeg,png,jpg,jpeg,bmp,pdf,docx,doc,csv,xlsx,txt|max:5048'
                ];

                $this->validate_request($request->all(), $rule);

                // when authenticated user frm

                $employee_id = $request->get('employee_id');
                if(!$employee = $this->check_exist_employee_by($employee_id)) throw new Exception(trans('page.no_record_found'));

                $file_name = $request->get('file_name');
                $file = Input::File('file');
                $file_token = Str::random(50);
                $file_extension = $file->getClientOriginalExtension();

                if(in_array($file_extension, ['jpeg', 'png', 'jpg', 'jpeg', 'bmp']))
                {
                    $folder = 'Picture';
                }
                elseif(in_array($file_extension, ['pdf', 'docx', 'doc', 'csv', 'xlsx', 'txt']))
                {
                    $folder = 'Documents';
                }

                $folder_path = public_path().'/tieza/attachment/'.$employee_id.'/'.$folder;

                if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);
                
                $request->file('file')->move($folder_path, $file_name.'_'.$file_token.'.'.$file_extension);

                DB::beginTransaction();

                DB::table('employee_attachment')
                ->insert([
                'file_name' => $file_name,
                'employee_id' => $employee_id,
                'token_code' => $file_token,
                'file_type' => $folder,
                'file_extension' => $file_extension,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ]);

                $lastID = DB::getPdo()->lastInsertId();

                DB::commit();
            }
            elseif($option == 'change_rate')
            {
                $emp_id = $request->get('emp_id');

                $this->check_exist_employee($emp_id);

                $this->check_change_rate_effectivity($emp_id);

                $rule = [
                'effective_date' => 'required',
                'new_rate' => 'required',
                'remarks' => 'sometimes|nullable|max:255',
                ];

                $this->validate_request($request->all(), $rule);

                $new_rate = str_replace(',', '', $request->get('new_rate'));

                $insert_change_rate_data = [
                'employee_id' => $request->get('emp_id'),
                'effective_date' => $request->get('effective_date'),
                'new_rate' => $new_rate,
                'remarks' => $request->get('remarks'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                if(!$latest_workexp = $this->get_latest_workexp($emp_id)) throw new Exception('No Work Experience was found. Please add first.');

                $insert_latest_workexp_data = [
                'employee_id' => $latest_workexp->employee_id,
                'agency_id' => $latest_workexp->agency_id,
                'position_id' => $latest_workexp->position_id,
                'workexp_start_date' => $latest_workexp->workexp_start_date,
                'workexp_end_date' => $latest_workexp->workexp_end_date,
                'workexp_present' => $latest_workexp->workexp_present,
                'pay_rate' => $latest_workexp->pay_rate,
                'salary_grade' => $latest_workexp->salary_grade,
                'salary_step' => $latest_workexp->salary_step,
                'monthly_salary' => $new_rate,
                'employment_status_id' => $latest_workexp->employment_status_id,
                'government_service' => $latest_workexp->government_service,
                'sector_id' => $latest_workexp->sector_id,
                'department_id' => $latest_workexp->department_id,
                'division_id' => $latest_workexp->division_id,
                'unit_id' => $latest_workexp->unit_id,
                'designation_id' => $latest_workexp->designation_id,
                'office_id' => $latest_workexp->office_id,
                'assignment_id' => $latest_workexp->assignment_id,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employee_work_experience')
                ->where('employee_work_experience.employee_id', $emp_id)
                ->where('employee_work_experience.deleted_at', null)
                ->update([
                'workexp_present' => 0
                ]);

                $insert_workexp = DB::table('employee_work_experience')
                ->insert($insert_latest_workexp_data);

                $insert_change_rate_data['workexp_id'] = DB::getPdo()->lastInsertId();

                DB::table('employee_change_rate')
                ->insert($insert_change_rate_data);

                DB::commit();
            }
            elseif($option == 'reassignment')
            {
                $emp_id = $request->get('emp_id');

                $this->check_exist_employee($emp_id);

                $rule = [
                'effective_date' => 'required',
                'reassignment_no' => 'required',
                'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                'unit' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_units(), 'id')),
                'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id')),
                'designation' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_designations(), 'id'))
                ];

                $this->validate_request($request->all(), $rule);

                $insert_data = [
                'employee_id' => $request->get('emp_id'),
                'effective_date' => $request->get('effective_date'),
                'reassignment_no' => $request->get('reassignment_no'),
                'sector_id' => $request->get('sector'),
                'department_id' => $request->get('department'),
                'division_id' => $request->get('division'),
                'unit_id' => $request->get('unit'),
                'office_id' => $request->get('office'),
                'designation_id' => $request->get('designation'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employee_reassignment')
                ->insert($insert_data);

                DB::commit();
            }
            elseif($option == 'designation')
            {
                $emp_id = $request->get('emp_id');

                $this->check_exist_employee($emp_id);

                $rule = [
                'effective_date' => 'required',
                'position_title' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_positions(), 'id')),
                'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                'unit' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_units(), 'id')),
                'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id')),
                'designation' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_designations(), 'id'))
                ];

                $this->validate_request($request->all(), $rule);

                $insert_data = [
                'employee_id' => $request->get('emp_id'),
                'effective_date' => $request->get('effective_date'),
                'position_id' => $request->get('position_title'),
                'sector_id' => $request->get('sector'),
                'department_id' => $request->get('department'),
                'division_id' => $request->get('division'),
                'unit_id' => $request->get('unit'),
                'office_id' => $request->get('office'),
                'designation_id' => $request->get('designation'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employee_designation')
                ->insert($insert_data);

                DB::commit();
            }
            elseif($option == 'renewal')
            {
                DB::beginTransaction();

                $start_date = $request->get('start_date');
                $end_date   = $request->get('end_date');
                $employees  = $request->get('employees');
                $employees  = explode(",", $employees);
                foreach ($employees as $key => $value) {
                    if ($value != '')
                    {
                        if ($this->check_if_employee_has_contract($value))
                        {
                            $employment = $this->get_employees_current_employment($value);
                            $empinfo    = $this->get_employment_info($value);
                            
                            $insert_data = [
                                'employee_id' => $value,
                                'workexp_start_date' => $start_date,
                                'workexp_end_date' => $end_date,
                                'workexp_present' => 1,
                                'former_incumbent' => $employment->former_incumbent,
                                'position_id' => $employment->position_id,
                                'plantilla_item_id' => $employment->plantilla_item_id,
                                'agency_id' => $employment->agency_id,
                                'location_id' => $employment->location_id,
                                'seperated_date' => $employment->seperated_date,
                                'section_id' => $employment->section_id,
                                'sector_id' => $employment->sector_id,
                                'branch_id' => $employment->branch_id,
                                'office_id' => $employment->office_id,
                                'department_id' => $employment->department_id,
                                'division_id' => $employment->division_id,
                                'unit_id' => $employment->unit_id,
                                'salary_grade' => $employment->salary_grade,
                                'salary_step' => $employment->salary_step,
                                'employment_status_id' => $employment->employment_status_id,
                                'appointment_status_id' => $employment->appointment_status_id,
                                'designation_id' => $employment->designation_id,
                                'appointment_type' => $employment->appointment_type,
                                'monthly_salary' => $employment->monthly_salary,
                                'government_service' => $employment->government_service,
                                'new_employee_number' => $employment->new_employee_number,
                                'lwop' => $employment->lwop,
                                'lwop_date' => $employment->lwop_date,
                                'reason' => $employment->reason,
                                'remarks' => $employment->remarks,
                                'pay_rate' => $employment->pay_rate,
                                'created_by' => Auth::user()->id,
                                'created_at' => DB::raw('now()')
                            ];
                            $update_data = [
                                'workexp_present' => 0,
                                'updated_by' => Auth::user()->id,
                                'updated_at' => DB::raw('now()')
                            ];

                            $update_empinfo = [
                                'start_date' => $start_date,
                                'end_date' => $end_date,
                                'updated_by' => Auth::user()->id,
                                'updated_at' => DB::raw('now()')
                            ];

                            $renewal_data = [
                                'employee_id' => $value,
                                'start_date' => $start_date,
                                'end_date' => $end_date,
                                'created_by' => Auth::user()->id,
                                'created_at' => DB::raw('now()')
                            ];

                            DB::table('employee_work_experience')
                            ->where('employee_work_experience.id', $employment->id)
                            ->update($update_data);

                            DB::table('employee_informations')
                            ->where('employee_informations.id', $empinfo->id)
                            ->update($update_empinfo);

                            DB::table('employee_work_experience')
                            ->insert($insert_data);

                            DB::table('employee_renewals')
                            ->insert($renewal_data);

                        }
                    }
                }
                
                DB::commit();
            }
        }
        catch(Exception $e)
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response(['success' => $lastID], 201);
    }

    public function edit_personal_info(request $request, $option, $id)
    {
        try
        {
            $this->is_personal_information_option_exist($option);

            $default_data = ['module' => $this->module, 'option' => $option];

            if($option == 'pds_file')
            {
                $default_data['this_url'] = url('personal_information/');

                if(Auth::user()->isUser())
                {
                    $default_data['this_url'] = url('user/personal_information/');
                }

                if($selected_target = $request->input('selected_target')) $default_data['selected_target'] = $selected_target;

                $data = array_merge($default_data, ['module' => $this->module, 'option' => $option, 'file' => 'personal_information.pds_file.201_update.show', 'fieldnames' => $this->fieldnames,  'employee_id' => $id, 'list_cities' => $this->list_cities(), 'list_provinces' => $this->list_provinces(), 'list_bloodtypes' => $this->list_bloodtypes(), 'list_civilstatus' => $this->list_civilstatus(), 'list_occupations' => $this->list_occupations(), 'list_countries' => $this->list_countries(), 'employee' => $this->get_employee($id)]);

                $file = 'personal_information.pds_file.edit';
            }
            elseif($option == 'employee_service_record')
            {
                $data = array_merge($default_data, ['agencies' => $this->list_agencies(), 'departments' => $this->list_departments(), 'offices' => $this->list_offices(), 'plantilla_items' => $this->list_plantilla_items(), 'divisions' => $this->list_divisions(), 'positions' => $this->list_positions(), 'appointment_status' => $this->list_appointment_status(), 'employment_status' => $this->list_employment_status(), 'branches' => $this->list_branches(), 'locations' => $this->list_locations(), 'sections' => $this->list_sections(), 'sectors' => $this->list_sectors(), 'units' => $this->list_units(), 'service_record' => $this->get_work_experience($id), 'list_sections' => $this->list_sections(), 'appointment_type' => $this->list_appointment_type(), 'fs_salary_grade' => $this->salary_grade, 'fs_salary_step' => $this->step, 'service_record_id' => $id,  'list_employees' => $this->list_employees()]);

                $file = 'personal_information.employee_service_record.edit';
            }
            elseif($option == 'change_rate')
            {
                $change_rate = $this->check_exist_employee_change_rate(null, $id);

                $default_data['this_url'] = url('personal_information/');

                $employee = $this->check_exist_employee($change_rate->employee_id);

                $data = array_merge($default_data, ['file' => 'personal_information.hr_action.change_rate.form', 'employee' => $employee, 'employee_id' => $employee->id, 'item_id' => $id]);

                $file = 'personal_information.hr_action.edit';
            }
            elseif($option == 'reassignment')
            {
                $reassignment = $this->check_exist_employee_reassignment(null, $id);

                $default_data['this_url'] = url('personal_information/');

                $employee = $this->check_exist_employee($reassignment->employee_id);

                $data = array_merge($default_data, ['file' => 'personal_information.hr_action.reassignment.form', 'employee' => $employee, 'employee_id' => $employee->id, 'item_id' => $id, 'list_sectors' => $this->list_sectors(), 'list_departments' => $this->list_departments(), 'list_divisions' => $this->list_divisions(), 'list_units' => $this->list_units(), 'list_offices' => $this->list_offices(), 'list_designations' => $this->list_designations()]);

                $file = 'personal_information.hr_action.edit';
            }
            elseif($option == 'designation')
            {
                $designation = $this->check_exist_employee_designation(null, $id);

                $default_data['this_url'] = url('personal_information/');

                $employee = $this->check_exist_employee($designation->employee_id);

                //$data = array_merge($default_data, ['file' => 'personal_information.hr_action.designation.form', 'employee' => $employee, 'employee_id' => $employee->id, 'item_id' => $id, 'list_positions' => $this->list_positions()]);

                $data = array_merge($default_data, ['file' => 'personal_information.hr_action.designation.form', 'employee' => $employee, 'employee_id' => $employee->id, 'item_id' => $id, 'list_positions' => $this->list_positions(), 'list_sectors' => $this->list_sectors(), 'list_departments' => $this->list_departments(), 'list_divisions' => $this->list_divisions(), 'list_units' => $this->list_units(), 'list_offices' => $this->list_offices(), 'list_designations' => $this->list_designations()]);

                $file = 'personal_information.hr_action.edit';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }

        return view($file, $data);
    }

    public function update_personal_info(request $request, $option)
    {
        try
        {   
            $this->is_personal_information_option_exist($option);

            $lastID = '';

            $update_data = [
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
            ];

            if($option == 'pds_file')
            {
                $emp_id = $request->get('emp_id');
                $form = $request->get('form');

                if(!$employee = $this->check_exist_employee_by($emp_id)) throw new Exception(trans('page.no_record_found'));

                if(!in_array($form, ['personal', 'add_children', 'edit_children', 'remove_children', 'add_education', 'edit_education', 'remove_education', 'add_eligibility', 'edit_eligibility', 'remove_eligibility', 'add_other_info', 'edit_other_info', 'remove_other_info', 'add_reference', 'edit_reference', 'remove_reference', 'edit_pdsq', 'add_voluntary_work', 'edit_voluntary_work', 'remove_voluntary_work', 'add_training_program', 'edit_training_program', 'remove_training_program', 'add_work_experience', 'edit_work_experience', 'remove_work_experience', 'family', 'profile_picture', 'personal_info', 'family_background'])) throw new Exception(trans('page.no_record_found'));

                if($form == 'profile_picture')
                {
                    $rule['profile_picture'] = 'required|image|mimes:jpeg,png,jpg,jpeg|max:5048';
                
                    $this->validate_request($request->all(), $rule, $this->personal_info_attribute, 'yes');

                    $file = Input::File('profile_picture');
                    $file_token = Str::random(50);
                    $file_extension = $file->getClientOriginalExtension();
                    $file_name = 'avatar_'.$file_token.'.'.$file_extension;

                    $folder_path = public_path().'/tieza/attachment/'.$emp_id;

                    if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                    $request->file('profile_picture')->move($folder_path, $file_name);

                    $update_employee['image_path'] = $file_name;

                    DB::beginTransaction();

                    DB::table('employees')
                    ->where('employees.id', '=', $emp_id)
                    ->update([
                    'image_path' => $file_name
                    ]);
                    
                    DB::commit();

                    $lastID = $employee;
                }
                elseif($form == 'personal_info')
                {
                    $validate_cities = implode(',', $this->get_value_by($this->list_cities(), 'id'));
                    $validate_bloodtypes = implode(',', $this->get_value_by($this->list_bloodtypes(), 'id'));
                    $validate_civilstatus = implode(',', $this->get_value_by($this->list_civilstatus(), 'id'));

                    $rule = [
                    'last_name' => 'required|max:255',
                    'first_name' => 'required|max:255',
                    'middle_name' => 'sometimes|nullable|max:255',
                    'extension_name' => 'sometimes|nullable|max:255',
                    'sex' => 'sometimes|nullable|in:M,F',
                    'civil_status' => 'sometimes|nullable|in:'.$validate_civilstatus,
                    'other_civil_status' => 'sometimes|nullable|max:255',
                    'birth_date' => 'required',
                    'birth_place' => 'required|max:255',
                    'height' => 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/',
                    'weight' => 'sometimes|nullable|regex:/^\d+(\.\d{1,2})?$/',
                    'blood_type' => 'sometimes|nullable|in:'.$validate_bloodtypes,
                    'gsis' => 'sometimes|nullable|max:255',
                    'pagibig' => 'sometimes|nullable|max:255',
                    'philhealth' => 'sometimes|nullable|max:255',
                    'sss' => 'sometimes|nullable|max:255',
                    'tin' => 'sometimes|nullable|max:255',
                    'agency_number' => 'sometimes|nullable|max:50',
                    'employee_number' => 'sometimes|nullable|max:255',
                    'telepone_no' => 'sometimes|nullable|max:255',
                    'mobile_no' => 'sometimes|nullable|max:255',
                    'email_address' => 'sometimes|nullable|email',
                    'citizenship' => 'sometimes|nullable|in:is_filipino,is_dual',
                    'tshirt_size' => 'sometimes|nullable|max:50',
                    ];

                    $citizenship = $request->get('citizenship');

                    if($citizenship == 'is_dual')
                    {
                        $rule['dual_citizenship'] = 'required|in:by_birth,by_naturalization';
                        $rule['indicate_details'] = 'sometimes|nullable|max:50';
                    }

                    $rule = array_merge($rule, ['residential_house_no' => 'sometimes|nullable|max:255',
                    'residential_street' => 'sometimes|nullable|max:255',
                    'residential_subdivision' => 'sometimes|nullable|max:255',
                    'residential_barangay' => 'sometimes|nullable|max:255',
                    'residential_city' => 'sometimes|nullable|in:'.$validate_cities,
                    'residential_zip_code' => 'sometimes|nullable',
                    'same_as_above' => 'in:0,1',
                    'permanent_house_no' => 'sometimes|nullable|max:255',
                    'permanent_street' => 'sometimes|nullable|nullable|max:255',
                    'permanent_subdivision' =>  'sometimes|nullable|max:255',
                    'permanent_barangay' => 'sometimes|nullable|max:255',
                    'permanent_city' => 'sometimes|nullable|in:'.$validate_cities,
                    'permanent_zip_code' => 'sometimes|nullable',
                    ]);

                    $this->validate_request($request->all(), $rule, $this->personal_info_attribute);

                    $residential_house_no = $request->get('residential_house_no');
                    $residential_street = $request->get('residential_street');
                    $residential_subdivision = $request->get('residential_subdivision');
                    $residential_barangay = $request->get('residential_barangay');

                    $residential_zip_code = $request->get('residential_zip_code');
                    $residential_province = null;

                    if($residential_city = $request->get('residential_city'))
                    {
                        $residential_city_info = $this->select_city_by($residential_city);
                        $residential_province = $residential_city_info->province_id;
                    }
                    
                    $same_as_above = $request->get('same_as_above');

                    $permanent_house_no = $request->get('permanent_house_no');
                    $permanent_street = $request->get('permanent_street');
                    $permanent_subdivision = $request->get('permanent_subdivision');
                    $permanent_barangay = $request->get('permanent_barangay');

                    $permanent_province = null;
                    $permanent_zip_code = $request->get('permanent_zip_code');

                    if($permanent_city = $request->get('permanent_city'))
                    {
                        $permanent_city_info =  $this->select_city_by($permanent_city);
                        $permanent_province = $permanent_city_info->province_id;
                    }
                
                    if($same_as_above == 1)
                    {
                        $permanent_house_no = $residential_house_no;
                        $permanent_street = $residential_street;
                        $permanent_subdivision = $residential_subdivision;
                        $permanent_barangay = $residential_barangay;
                        $permanent_city = $residential_city;
                        $permanent_province = $residential_province;
                        $permanent_zip_code = $residential_zip_code;
                    }

                    $civil_status = $request->get('civil_status');
                    $other_civil_status = $request->get('other_civil_status');

                    if($civil_status !== 5) 
                    {
                        $other_civil_status = null;
                    }

                    $update_data = [
                    'last_name' => strtoupper($request->get('last_name')),
                    'first_name' => strtoupper($request->get('first_name')),
                    'middle_name' => strtoupper($request->get('middle_name')),
                    'extension_name' => strtoupper($request->get('extension_name')),
                    'birthday' => date("Y-m-d", strtotime($request->get('birth_date'))),
                    'birth_place' => $request->get('birth_place'),
                    'email_address' => $request->get('email_address'),
                    'gender' => $request->get('sex'),
                    'civil_status_id' => $civil_status,
                    'other_civil_status' => $other_civil_status,
                    'height' => number_format($request->get('height'), 2, '.', ''),
                    'weight' => number_format($request->get('weight'), 2, '.', ''),
                    'blood_type_id' => $request->get('blood_type'),
                    'gsis' => $request->get('gsis'),
                    'pagibig' => $request->get('pagibig'),
                    'philhealth' => $request->get('philhealth'),
                    'sss' => $request->get('sss'),
                    'tin' => $request->get('tin'),
                    'employee_number' => $request->get('agency_number'),
                    'telephone_number' => $request->get('telepone_no'),
                    'mobile_number' => $request->get('mobile_no'),
                    'residential_house_number' => $residential_house_no,
                    'residential_street' => $residential_street,
                    'residential_subdivision' => $residential_subdivision,
                    'residential_brgy' => $residential_barangay,
                    'residential_city_id' => $residential_city,
                    'residential_province_id' => $residential_province,
                    'residential_zip_code' =>  $residential_zip_code,
                    'residential_country_id' => 1,
                    'permanent_house_number' => $permanent_house_no,
                    'permanent_street' => $permanent_street,
                    'permanent_subdivision' => $permanent_subdivision,
                    'permanent_brgy' => $permanent_barangay,
                    'permanent_city_id' => $permanent_city,
                    'permanent_province_id' => $permanent_province,
                    'permanent_zip_code' => $permanent_zip_code,
                    'permanent_country_id' => 1,
                    'tshirt_size' => $request->get('tshirt_size'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ];


                    if($citizenship == 'is_filipino')
                    {
                        $update_data['filipino'] = '1';
                        $update_data['naturalized'] = null;
                        $update_data['naturalized_details'] = null;
                    }
                    elseif($citizenship == 'is_dual')
                    {
                        $update_data['filipino'] = null;

                        if($request->get('dual_citizenship') == 'by_birth')
                        {
                            $update_data['naturalized'] = '0';
                        }
                        elseif($request->get('dual_citizenship') == 'by_naturalization')
                        {
                            $update_data['naturalized'] = '1';
                        }

                        $update_data['naturalized_details'] = $request->get('indicate_details');
                    }
                    else
                    {
                        $update_data['filipino'] = null;
                        $update_data['naturalized'] = null; 
                        $update_data['naturalized_details'] = null;  
                    }

                    DB::beginTransaction();

                    DB::table('employees')
                    ->where('employees.id', $emp_id)
                    ->update($update_data);

                    DB::commit();

                    $lastID = $request->get('tin');
                }
                elseif($form == 'family_background')
                {
                    $rule = [
                    'spouse_last_name'=> 'sometimes|nullable|max:255', 
                    'spouse_first_name'=> 'sometimes|nullable|max:255', 
                    'spouse_middle_name'=> 'sometimes|nullable|max:255', 
                    'spouse_extension_name'=> 'sometimes|nullable|max:255', 
                    'spouse_employer_name'=> 'sometimes|nullable|max:255', 
                    'spouse_occupation' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_occupations(), 'id')),
                    'spouse_business_address'=> 'sometimes|nullable|max:255', 
                    'spouse_telephone_number' => 'sometimes|nullable|max:255',
                    'father_last_name'=> 'sometimes|nullable|max:255', 
                    'father_first_name'=> 'sometimes|nullable|max:255', 
                    'father_middle_name'=> 'sometimes|nullable|max:255', 
                    'father_extension_name'=> 'sometimes|nullable|max:255', 
                    'mother_last_name'=> 'sometimes|nullable|max:255', 
                    'mother_first_name'=> 'sometimes|nullable|max:255', 
                    'mother_middle_name'=> 'sometimes|nullable|max:255'
                    ];

                    $this->validate_request($request->all(), $rule, $this->personal_info_attribute);

                    $update_data = [
                    'father_last_name' => $request->get('father_last_name'),
                    'father_first_name' => $request->get('father_first_name'), 
                    'father_middle_name' => $request->get('father_middle_name'), 
                    'father_extension_name' => $request->get('father_extension_name'), 
                    'mother_last_name' => $request->get('mother_last_name'), 
                    'mother_first_name' => $request->get('mother_first_name'), 
                    'mother_middle_name' => $request->get('mother_middle_name'), 
                    'spouse_last_name' => $request->get('spouse_last_name'), 
                    'spouse_first_name' => $request->get('spouse_first_name'), 
                    'spouse_middle_name' => $request->get('spouse_middle_name'), 
                    'spouse_extension_name' => $request->get('spouse_extension_name'), 
                    'employer_name' => $request->get('spouse_employer_name'), 
                    'occupation_id' => $request->get('spouse_occupation'), 
                    'business_address' => $request->get('spouse_business_address'), 
                    'telephone_number' => $request->get('spouse_telephone_number')
                    ];

                    foreach($update_data as $key => $val)
                    {
                        $update_data[$key] = strtoupper($val);
                    }

                    if($employee_family = $this->get_employee_family_by($emp_id))
                    {
                        $update_data['updated_by'] = Auth::user()->id;
                        $update_data['updated_at'] = DB::raw('now()');

                        DB::beginTransaction();

                        DB::table('employee_family')
                        ->where('employee_family.employee_id', $emp_id)
                        ->update($update_data);

                        DB::commit();
                    }
                    else
                    {
                        $update_data['employee_id'] = $emp_id;
                        $update_data['created_by'] = Auth::user()->id;
                        $update_data['created_at'] = DB::raw('now()');

                        DB::beginTransaction();

                        DB::table('employee_family')
                        ->insert($update_data);

                        DB::commit();
                    }
                }
                elseif($form == 'personal')
                {
                    $field_name = $request->get('field_name');
            
                    $new_value = $request->get('new_value');
                    if($field_name == 'civil_status_id') $new_value_others = $request->get('new_value_others');
                    
                    $rule = array(
                    'field_name' => 'required|in:'.implode(',', array_column($this->fieldnames, 'field_name'))
                    );

                    $update_employee = [];
                    $update_employee_family = [];
                    $insert_employee_updates = [];

                    if(in_array($field_name, ['last_name', 'first_name', 'middle_name', 'birth_place', 'employee_number', 'residential_house_no', 'residential_barangay', 'permanent_house_no', 'permanent_barangay']))
                    {
                        $rule['new_value'] = 'required|max:255';
                        $update_employee[$field_name] = strtoupper($new_value); 
                    }
                    elseif(in_array($field_name, ['birthday']))
                    {
                        $rule['new_value'] = 'required'; // validate with regex
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['email_address']))
                    {
                        $rule['new_value'] = 'required|email';
                    }
                    elseif(in_array($field_name, ['extension_name', 'nickname', 'height', 'weight', 'gsis', 'pagibig', 'philhealth', 'sss', 'tin', 'telephone_number', 'mobile_number', 'contact_number', 'residential_street', 'residential_subdivision', 'permanent_street', 'permanent_subdivision', 'father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                    {
                        $rule['new_value'] = 'sometimes|nullable|max:255';

                        if(in_array($field_name, ['father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                        {       
                            $update_employee_family[$field_name] = strtoupper($new_value);
                        }
                        else
                        {
                            $update_employee[$field_name] = strtoupper($new_value);
                        }
                    }
                    elseif(in_array($field_name, ['civil_status_id']))
                    {
                        $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_civilstatus(), 'id') );
                        $update_employee[$field_name] = $new_value;

                        if($new_value == '5')
                        {
                            $rule['new_value_others'] = 'required|max:255';
                            $update_employee[$field_name] = $new_value;
                            $update_employee['other_civil_status'] = $new_value_others;

                            $insert_employee_updates['new_value'] = $new_value_others;
                        } 
                    }
                    elseif(in_array($field_name, ['gender']))
                    {
                        $rule['new_value'] = 'in:'.implode(',', config('params.sex'));
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['blood_type_id']))
                    {
                        $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_bloodtypes(), 'id'));
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['filipino']))
                    {
                        $rule['new_value'] = 'sometimes|nullable|in:0,1';
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['occupation_id']))
                    {
                        $rule['new_value'] = 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_occupations(), 'id'));
                        $update_employee_family[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['spouse_telephone_number']))
                    {
                        $rule['new_value'] = 'sometimes|nullable|max:255';
                        $update_employee_family['telephone_number'] = $new_value;
                    }
                    elseif(in_array($field_name, ['residential_city_id', 'permanent_city_id']))
                    {
                        $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_cities(), 'id'));
                        $update_employee[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['image_path']))
                    {
                        $rule['new_value'] = 'required|image|mimes:jpeg,png,jpg,jpeg|max:5048';
                    }

                    $this->validate_request($request->all(), $rule, $this->personal_info_attribute);

                    $old_value = $employee->$field_name;

                    if($field_name == 'civil_status_id')
                    {
                        if($old_value == 5) $old_value = $employee->other_civil_status;
                    }

                    $insert_employee_updates = array_merge($insert_employee_updates, [
                    'field_name' => $field_name,
                    'employee_id' => $emp_id,
                    'old_value' => $old_value,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ]);

                    if(in_array($field_name, ['residential_city_id', 'permanent_city_id']))
                    {
                        $selected_city = $this->select_city_by($new_value);

                        if($field_name == 'residential_city_id')
                        {
                            $update_employee['residential_province_id'] = $selected_city->province_id;
                            $update_employee['residential_zip_code'] = $selected_city->zip_code;
                        }
                        elseif($field_name == 'permanent_city_id')
                        {
                            $update_employee['permanent_province_id'] = $selected_city->province_id;
                            $update_employee['permanent_zip_code'] = $selected_city->zip_code;
                        }
                    }
                    elseif(in_array($field_name, ['image_path']))
                    {
                        $file = Input::File('new_value');
                        $file_token = Str::random(50);
                        $file_extension = $file->getClientOriginalExtension();
                        $file_name = 'avatar_'.$file_token.'.'.$file_extension;

                        $insert_employee_updates['new_value'] = $file_name;

                        $folder_path = public_path().'/tieza/attachment/'.$emp_id;

                        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                        $request->file('new_value')->move($folder_path, $file_name);

                        $update_employee['image_path'] = $file_name;


                    }

                    DB::beginTransaction();

                    if(!empty($update_employee))
                    {
                        DB::table('employees')
                        ->where('employees.id', '=', $emp_id)
                        ->update($update_employee);
                    }
                    
                    if(!empty($update_employee_family))
                    {
                        DB::table('employee_family')
                        ->where('employee_family.employee_id', '=', $emp_id)
                        ->update($update_employee_family);
                    }

                    if(!array_key_exists('new_value', $insert_employee_updates)) $insert_employee_updates['new_value'] = $new_value;

                    DB::table('employee_updates')
                    ->insert($insert_employee_updates);

                    $lastID = $employee;

                    DB::commit();
                }
                elseif($form == 'family')
                {
                    $field_name = $request->get('field_name_family');

                    $new_value = $request->get('new_value_family');

                    $rule = array(
                    'field_name_family' => 'required|in:'.implode(',', array_column($this->fieldnames, 'field_name'))
                    );

                    $update_employee = [];
                    $update_employee_family = [];
                    $insert_employee_updates = [];

                    if(in_array($field_name, ['father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                    {
                        $rule['new_value_family'] = 'sometimes|nullable|max:255';

                        if(in_array($field_name, ['father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                        {       
                            $update_employee_family[$field_name] = strtoupper($new_value);
                        }
                        else
                        {
                            $update_employee[$field_name] = strtoupper($new_value);
                        }
                    }
                    elseif(in_array($field_name, ['occupation_id']))
                    {
                        $rule['new_value_family'] = 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_occupations(), 'id'));
                        $update_employee_family[$field_name] = $new_value;
                    }
                    elseif(in_array($field_name, ['spouse_telephone_number']))
                    {
                        $rule['new_value_family'] = 'sometimes|nullable|max:255';
                        $update_employee_family['telephone_number'] = $new_value;
                    }

                    $this->validate_request($request->all(), $rule, $this->personal_info_attribute);

                    $old_value = $employee->$field_name;

                    $insert_employee_updates = array_merge($insert_employee_updates, [
                    'field_name' => $field_name,
                    'employee_id' => $emp_id,
                    'old_value' => $old_value,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ]);

                    DB::beginTransaction();

                    if(!empty($update_employee))
                    {
                        DB::table('employees')
                        ->where('employees.id', '=', $emp_id)
                        ->update($update_employee);
                    }
                    
                    if(!empty($update_employee_family))
                    {
                        DB::table('employee_family')
                        ->where('employee_family.employee_id', '=', $emp_id)
                        ->update($update_employee_family);
                    }

                    if(!array_key_exists('new_value', $insert_employee_updates)) $insert_employee_updates['new_value'] = $new_value;

                    DB::table('employee_updates')
                    ->insert($insert_employee_updates);

                    $lastID = $employee;

                    DB::commit();
                }
                elseif($form == 'add_children')
                {
                    $rule = [
                    'children_name' => 'required|max:255',
                    'children_birthday' => 'required',
                    'children_gender' => 'in:,'.implode(',', array_keys(config('params.sex'))),
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_children')
                    ->insert([
                    'employee_id' => $request->get('emp_id'),
                    'child_name' => $request->get('children_name'),
                    'child_birthday' => $request->get('children_birthday'),
                    'child_gender' => $request->get('children_gender'),
                    'created_by' => Auth::user()->id
                    ]);
      
                    DB::commit();
                } 
                elseif($form == 'edit_children')
                {
                    $rule = [
                    'children_name' => 'required|max:255',
                    'children_birthday' => 'required',
                    'children_gender' => 'in:,'.implode(',', array_keys(config('params.sex'))),
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_children')
                    ->where('employee_children.id', $request->get('item_id'))
                    ->update([
                    'child_name' => $request->get('children_name'),
                    'child_birthday' => $request->get('children_birthday'),
                    'child_gender' => $request->get('children_gender'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'remove_children')
                {
                    DB::beginTransaction();

                    DB::table('employee_children')
                    ->where('employee_children.id', $request->get('children_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()'),
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'add_education')
                {
                    $rule = [
                    'education_level' => 'required|in:1,2,3,4,5',
                    'education_school' => 'required|in:'.implode(',', $this->get_value_by($this->list_schools(), 'id')),
                    'education_start_year' => 'required|digits:4|integer|min:1900',
                    'education_end_year' => 'sometimes|nullable|digits:4|integer|min:1900',
                    'education_present' => 'required|in:0,1',
                    'education_year_graduated' => 'sometimes|nullable|digits:4|integer|min:1900',
                    'education_highest_level' => 'sometimes|nullable|max:50',
                    'education_scholarship_academic' => 'sometimes|nullable|max:255',
                    ];

                    $education_level = $request->get('education_level');
                    $education_course = $request->get('education_course');

                    if(in_array($education_level, ['1', '2']))
                    {
                        $rule['education_course'] = 'in:'.implode(',', array_keys(config('params.basic_education'))); 
                    }
                    else
                    {
                        $rule['education_course'] = 'required|in:'.implode(',', $this->get_value_by($this->list_courses(), 'id'));
                    }

                    $this->validate_request($request->all(), $rule);

                  
                    $education_present = $request->get('education_present');


                    $insert_data = [
                    'employee_id' => $request->get('emp_id'),
                    'education_level' => $request->get('education_level'),
                    'school_id' => $request->get('education_school'),
                    'course_id' => $request->get('education_course'),
                    'start_year' => $request->get('education_start_year'),
                    'end_year' => $request->get('education_end_year'),
                    'education_present' => $request->get('education_present'),
                    'year_graduated' => $request->get('education_year_graduated'),
                    'highest_year_level' => $request->get('education_highest_level'),
                    'academic_honors' => $request->get('education_scholarship_academic'),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ];
                  
                    DB::beginTransaction();

                    DB::table('employee_education')
                    ->insert($insert_data);
      
                    DB::commit();
                }
                elseif($form == 'edit_education')
                {
                    $rule = [
                    'education_level' => 'required|in:1,2,3,4,5',
                    'education_school' => 'required|in:'.implode(',', $this->get_value_by($this->list_schools(), 'id')),
                    'education_start_year' => 'required|digits:4|integer|min:1900',
                    'education_end_year' => 'sometimes|nullable|digits:4|integer|min:1900',
                    'education_present' => 'required|in:0,1',
                    'education_year_graduated' => 'sometimes|nullable|digits:4|integer|min:1900',
                    'education_highest_level' => 'sometimes|nullable|max:50',
                    'education_scholarship_academic' => 'sometimes|nullable|max:255',
                    ];

                    $education_level = $request->get('education_level');
                    $education_course = $request->get('education_course');

                    if(in_array($education_level, ['1', '2']))
                    {
                        $rule['education_course'] = 'in:'.implode(',', array_keys(config('params.basic_education'))); 
                    }
                    else
                    {
                        $rule['education_course'] = 'required|in:'.implode(',', $this->get_value_by($this->list_courses(), 'id'));
                    }

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_education')
                    ->where('employee_education.id', $request->get('item_id'))
                    ->update([
                    'education_level' => $request->get('education_level'),
                    'school_id' => $request->get('education_school'),
                    'course_id' => $request->get('education_course'),
                    'start_year' => $request->get('education_start_year'),
                    'end_year' => $request->get('education_end_year'),
                    'education_present' => $request->get('education_present'),
                    'year_graduated' => $request->get('education_year_graduated'),
                    'academic_honors' => $request->get('education_scholarship_academic'),
                    'highest_year_level' => $request->get('education_highest_level'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'remove_education')
                {
                    DB::beginTransaction();

                    DB::table('employee_education')
                    ->where('employee_education.id', $request->get('education_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()'),
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'add_eligibility')
                {
                    $rule = [
                    'eligibility' => 'required|in:'.implode(',', $this->get_value_by($this->list_eligibilities(), 'id')),
                    'rating' => 'sometimes|nullable',
                    'examination_date' => 'required',
                    'examination_place' => 'required',
                    //'license_number' => 'required',
                    //'validity_date' => 'required',
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_eligibility')
                    ->insert([
                    'employee_id' => $request->get('emp_id'),
                    'eligibility_id' => $request->get('eligibility'),
                    'rating' => $request->get('rating'),
                    'examination_date' => $request->get('examination_date'),
                    'examination_place' => $request->get('examination_place'),
                    'license_number' => $request->get('license_number'),
                    'validity_date' => $request->get('validity_date'),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'edit_eligibility')
                {
                    $rule = [
                    'eligibility' => 'required|in:'.implode(',', $this->get_value_by($this->list_eligibilities(), 'id')),
                    'rating' => 'sometimes|nullable',
                    'examination_date' => 'required',
                    'examination_place' => 'required',
                    //'license_number' => 'required',
                    //'validity_date' => 'required',
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_eligibility')
                    ->where('employee_eligibility.id', $request->get('item_id'))
                    ->update([
                    'eligibility_id' => $request->get('eligibility'),
                    'rating' => $request->get('rating'),
                    'examination_date' => $request->get('examination_date'),
                    'examination_place' => $request->get('examination_place'),
                    'license_number' => $request->get('license_number'),
                    'validity_date' => $request->get('validity_date'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'remove_eligibility')
                {
                    DB::beginTransaction();

                    DB::table('employee_eligibility')
                    ->where('employee_eligibility.id', $request->get('eligibility_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()'),
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'add_other_info')
                {
                    $rule = [
                    'category' => 'required|in:'.implode(',', array_keys(config('params.other_info_category'))),
                    'details' => 'required'
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_other_info')
                    ->insert([
                    'employee_id' => $request->get('emp_id'),
                    'other_info_category' => $request->input('category'),
                    'details' => $request->get('details'),
                    'created_by' => Auth::user()->id
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'edit_other_info')
                {
                    $rule = [
                    'details' => 'required'
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_other_info')
                    ->where('employee_other_info.id', $request->get('item_id'))
                    ->update([
                    'details' => $request->get('details'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'remove_other_info')
                {
                    DB::beginTransaction();

                    DB::table('employee_other_info')
                    ->where('employee_other_info.id', $request->get('other_info_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()'),
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'add_reference')
                {
                    $rule = [
                    'name' => 'required|max:255',
                    'address' => 'sometimes|nullable|max:255',
                    'contact_no' => 'sometimes|nullable|max:255'
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_reference')
                    ->insert([
                    'employee_id' => $request->get('emp_id'),
                    'name' => $request->get('name'),
                    'address' => $request->get('address'),
                    'telephone_number' => $request->get('contact_no'),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'edit_reference')
                {
                    $rule = [
                    'name' => 'required|max:255',
                    'address' => 'sometimes|nullable|max:255',
                    'contact_no' => 'sometimes|nullable|max:255'
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_reference')
                    ->where('employee_reference.id', $request->get('item_id'))
                    ->update([
                    'name' => $request->get('name'),
                    'address' => $request->get('address'),
                    'telephone_number' => $request->get('contact_no'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'remove_reference')
                {
                    DB::beginTransaction();

                    DB::table('employee_reference')
                    ->where('employee_reference.id', $request->get('reference_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'edit_pdsq')
                {

                    $answer_34a = $request->get('34a');
                    $answer_34b = $request->get('34b');
                    $answer_34b_explanation = $request->get('34a_explanation');
                    $answer_35a = $request->get('35a');
                    $answer_35a_explanation = $request->get('35a_explanation');
                    $answer_35b = $request->get('35b');
                    $answer_35b_explanation = $request->get('35b_explanation');
                    $answer_35b_case_status = $request->get('35b_case_status');
                    $answer_35b_date_filed = $request->get('35b_date_filed');
                    $answer_36a = $request->get('36a');
                    $answer_36a_explanation = $request->get('36a_explanation');
                    $answer_37a = $request->get('37a');
                    $answer_37a_explanation = $request->get('37a_explanation');
                    $answer_38a = $request->get('38a');
                    $answer_38a_explanation = $request->get('38a_explanation');
                    $answer_38b = $request->get('38b');
                    $answer_38b_explanation = $request->get('38b_explanation');
                    $answer_39a = $request->get('39a');
                    $answer_39a_explanation = $request->get('39a_explanation');
                    $answer_40a = $request->get('40a');
                    $answer_40a_explanation = $request->get('40a_explanation');
                    $answer_40b = $request->get('40b');
                    $answer_40b_explanation = $request->get('40b_explanation');
                    $answer_40c = $request->get('40c');
                    $answer_40c_explanation = $request->get('40c_explanation');

                    $rule = array();

                    $rule['34a'] = 'required|in:0,1';
                    if($answer_34a == 1) $rule['34a_explanation'] = 'sometimes|nullable|max:500';

                    $rule['34b'] = 'required|in:0,1';
                    $rule['35b'] = 'required|in:0,1';

                    if($answer_35b == 1) 
                    {
                        $rule['35b_explanation'] = 'sometimes|nullable|max:500';
                        $rule['35b_case_status'] = 'sometimes|nullable|max:500';
                    }

                    $rule['36a'] = 'required|in:0,1';
                    if($answer_36a == 1) $rule['34b_explanation'] = 'sometimes|nullable|max:500';

                    $rule['37a'] = 'required|in:0,1';
                    if($answer_37a == 1) $rule['34b_explanation'] = 'sometimes|nullable|max:500';

                    $rule['38a'] = 'required|in:0,1';
                    if($answer_38a == 1) $rule['34b_explanation'] = 'sometimes|nullable|max:500';

                    $rule['38b'] = 'required|in:0,1';
                    if($answer_38b == 1) $rule['34b_explanation'] = 'sometimes|nullable|max:500';

                    $rule['39a'] = 'required|in:0,1';
                    if($answer_39a == 1) $rule['34b_explanation'] = 'sometimes|nullable|max:500';

                    $rule['40a'] = 'required|in:0,1';
                    if($answer_40a == 1) $rule['34b_explanation'] = 'sometimes|nullable|max:500';

                    $rule['40b'] = 'required|in:0,1';
                    if($answer_40b == 1) $rule['34b_explanation'] = 'sometimes|nullable|max:500';

                    $rule['40c'] = 'required|in:0,1';
                    if($answer_40c == 1) $rule['34b_explanation'] = 'sometimes|nullable|max:500';

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_pdsq')
                    ->where('employee_pdsq.employee_id', $request->get('emp_id'))
                    ->update([
                    '34a' => $answer_34a,
                    '34b' => $answer_34b,
                    '34b_explanation' => $answer_34b_explanation,
                    '35a' => $answer_35a,
                    '35a_explanation' => $answer_35a_explanation,
                    '35b' => $answer_35b,
                    '35b_explanation' => $answer_35b_explanation,
                    '35b_date_filed' => $answer_35b_date_filed,
                    '35b_case_status' => $answer_35b_case_status,
                    '36a' => $answer_36a,
                    '36a_explanation' => $answer_36a_explanation,
                    '37a' => $answer_37a,
                    '37a_explanation' => $answer_37a_explanation,
                    '38a' => $answer_38a,
                    '38a_explanation' => $answer_38a_explanation,
                    '38b' => $answer_38b,
                    '38b_explanation' => $answer_38b_explanation,
                    '39a' => $answer_39a,
                    '39a_explanation' => $answer_39a_explanation,
                    '40a' => $answer_40a,
                    '40a_explanation' => $answer_40a_explanation,
                    '40b' => $answer_40b,
                    '40b_explanation' => $answer_40b_explanation,
                    '40c' => $answer_40c,
                    '40c_explanation' => $answer_40c_explanation,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'add_voluntary_work')
                {
                    $rule = [
                    'organization_name' => 'required|in:'.implode(',', $this->get_value_by($this->list_organizations(), 'id')),
                    'start_date' => 'required',
                    'voluntary_present' => 'required|in:0,1',
                    ];

                    if($request->get('voluntary_present') == 0) $rule['end_date'] = 'required';

                    $rule = array_merge($rule, [
                    'no_of_hours' => 'required',
                    'nature_of_work' => 'sometimes|nullable',
                    ]);

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_voluntary_work')
                    ->where('employee_voluntary_work.deleted_at', null)
                    ->where('employee_voluntary_work.employee_id', $emp_id)
                    ->where('employee_voluntary_work.voluntarywork_present', '1')
                    ->update([
                    'voluntarywork_present' => null,
                    ]);

                    DB::table('employee_voluntary_work')
                    ->insert([
                    'employee_id' => $request->get('emp_id'),
                    'organization_id' => $request->get('organization_name'),
                    'voluntarywork_start_date' => $request->get('start_date'),
                    'voluntarywork_end_date' => $request->get('end_date'),
                    'voluntarywork_total_hours' => $request->get('no_of_hours'),
                    'voluntarywork_present' => $request->get('voluntary_present'),
                    'work_nature' => $request->get('nature_of_work'),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'edit_voluntary_work')
                {
                    $rule = [
                    'organization_name' => 'required|in:'.implode(',', $this->get_value_by($this->list_organizations(), 'id')),
                    'start_date' => 'required',
                    'voluntary_present' => 'required|in:0,1',
                    ];

                    if($request->get('voluntary_present') == 0) $rule['end_date'] = 'required';

                    $rule = array_merge($rule, [
                    'no_of_hours' => 'required',
                    'nature_of_work' => 'sometimes|nullable',
                    ]);


                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_voluntary_work')
                    ->where('employee_voluntary_work.deleted_at', null)
                    ->where('employee_voluntary_work.employee_id', $emp_id)
                    ->where('employee_voluntary_work.voluntarywork_present', '1')
                    ->update([
                    'voluntarywork_present' => null,
                    ]);

                    DB::table('employee_voluntary_work')
                    ->where('employee_voluntary_work.id', $request->get('item_id'))
                    ->update([
                    'organization_id' => $request->get('organization_name'),
                    'voluntarywork_start_date' => $request->get('start_date'),
                    'voluntarywork_end_date' => $request->get('end_date'),
                    'voluntarywork_total_hours' => $request->get('no_of_hours'),
                    'voluntarywork_present' => $request->get('voluntary_present'),
                    'work_nature' => $request->get('nature_of_work'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'remove_voluntary_work')
                {
                    DB::beginTransaction();

                    DB::table('employee_voluntary_work')
                    ->where('employee_voluntary_work.id', $request->get('voluntary_work_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'add_training_program')
                {
                    $rule = [
                    'training_name' => 'required|in:'.implode(',', $this->get_value_by($this->list_trainings(), 'id')),
                    'training_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_training_types(), 'id')),
                    'start_date' => 'required',
                    'end_date' => 'required',
                    'no_of_hours' => 'required',
                    'sponsor' => 'required',
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_training')
                    ->insert([
                    'employee_id' => $request->get('emp_id'),
                    'training_id' => $request->get('training_name'),
                    'training_start_date' => $request->get('start_date'),
                    'training_end_date' => $request->get('end_date'),
                    'training_total_hours' => $request->get('no_of_hours'),
                    'training_sponsor' => $request->get('sponsor'),
                    'training_type' => $request->get('training_type'),
                    'training_description' => $request->get('key_tags'),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'edit_training_program')
                {
                    $rule = [
                    'training_name' => 'required|in:'.implode(',', $this->get_value_by($this->list_trainings(), 'id')),
                    'training_type' => 'required|in:'.implode(',', $this->get_value_by($this->list_training_types(), 'id')),
                    'start_date' => 'required',
                    'end_date' => 'required',
                    'no_of_hours' => 'required',
                    'sponsor' => 'required',
                    ];

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    DB::table('employee_training')
                    ->where('employee_training.id', $request->get('item_id'))
                    ->update([
                    'training_id' => $request->get('training_name'),
                    'training_start_date' => $request->get('start_date'),
                    'training_end_date' => $request->get('end_date'),
                    'training_total_hours' => $request->get('no_of_hours'),
                    'training_sponsor' => $request->get('sponsor'),
                    'training_type' => $request->get('training_type'),
                    'training_description' => $request->get('key_tags'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'remove_training_program')
                {
                    DB::beginTransaction();

                    DB::table('employee_training')
                    ->where('employee_training.id', $request->get('training_program_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
                elseif($form == 'add_work_experience')
                {
                    $rule = ['work_present' => 'in:0,1', 'start_date' => 'required'];

                    $work_present = $request->get('work_present');

                    if($work_present == 0) $rule['end_date'] = 'required';
                    
                    $rule = array_merge($rule, [
                    'employment_status' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_employment_status(), 'id')),
                    'position_name' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_positions(), 'id')),
                    'agency_name' => 'required|in:'.implode(',', $this->get_value_by($this->list_agencies(), 'id')),
                    'pay_rate' => 'in:'.implode(',', array_keys(config('params.pay_rates'))),
                    'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                    'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                    'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                    'unit' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_units(), 'id')),
                    'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id')),
                    'designation' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_designations(), 'id')),
                    'salary_amount' => 'required',
                    'gevernment_service' => 'in:0,1',
                    'place_of_assignment' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_assignments(), 'id'))
                    ]);

                    $employee_number = $request->get('employee_number');

                    $employee_data = array();

                    if($employee_number != '' &&  $work_present == '1')
                    {
                        $employee_data = ['employee_number' => $employee_number];
                    }

                    $this->validate_request($request->all(), $rule);

                    DB::beginTransaction();

                    $insert_data = [
                        'employee_id' => $request->get('emp_id'),
                        'agency_id' => $request->get('agency_name'),
                        'position_id' => $request->get('position_name'),
                        'workexp_start_date' => $request->get('start_date'),
                        'workexp_end_date' => $request->get('end_date'),
                        'workexp_present' => $work_present,
                        'pay_rate' => $request->get('pay_rate'),
                        'salary_grade' => $request->get('salary_grade'),
                        'salary_step' => $request->get('salary_step'),
                        'monthly_salary' => str_replace(',', '', $request->get('salary_amount')),
                        'employment_status_id' => $request->get('employment_status'),
                        'government_service' => $request->get('government_service'),
                        'sector_id' => $request->get('sector'),
                        'department_id' => $request->get('department'),
                        'division_id' => $request->get('division'),
                        'unit_id' => $request->get('unit'),
                        'designation_id' => $request->get('designation'),
                        'office_id' => $request->get('office'),
                        'assignment_id' => $request->get('place_of_assignment'),
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];

                    if($work_present == 1)
                    {
                        DB::table('employee_work_experience')
                        ->where('employee_work_experience.employee_id', $emp_id)
                        ->where('employee_work_experience.workexp_present', '1')
                        ->where('employee_work_experience.deleted_at', null)
                        ->update(['workexp_end_date' => null]);

                        DB::table('employee_work_experience')
                        ->where('employee_work_experience.employee_id', $emp_id)
                        ->where('employee_work_experience.deleted_at', null)
                        ->update(['workexp_present' => '0']);
                    }

                    if($request->get('employment_status') == 3)
                    {
                        $first_work_exp_start_date = null;

                        if($this->count_work_experience($emp_id) == 0)
                        {
                            $first_work_exp_start_date = $request->get('start_date');
                        }
                        else
                        {
                            if($get_assumption_date = $this->get_first_work_exp($emp_id)) $first_work_exp_start_date = $get_assumption_date->workexp_start_date;
                        }

                        $empinfo_data = [
                            'position_id' => $request->get('position_name'),
                            'agency_id' => $request->get('agency_name'),
                            'department_id' => $request->get('department'),
                            'office_id' => $request->get('office'),
                            'division_id' => $request->get('division'),
                            'unit_id' => $request->get('unit'),
                            'employment_status_id' => $request->get('employment_status'),
                            'monthly_salary' => str_replace(',', '', $request->get('salary_amount')),
                        ];

                        if($this->count_empinfo_by($emp_id) == 1)
                        {
                            $empinfo_data = [
                            'hired_date' => $first_work_exp_start_date,
                            'updated_by' => Auth::user()->id,
                            'updated_at' => DB::raw('now()')
                            ];

                            DB::table('employee_informations')
                            ->where('employee_id', '=', $emp_id)
                            ->update($empinfo_data);
                        }
                        else
                        {
                            $empinfo_data['hired_date'] = $first_work_exp_start_date;

                            $empinfo_data = array_merge($empinfo_data, [
                            'employee_id' => $emp_id,
                            'created_by' => Auth::user()->id,
                            'created_at' => DB::raw('now()')
                            ]);

                            DB::table('employee_informations')
                            ->insert($empinfo_data);
                        }
                    }

                    DB::table('employee_work_experience')
                    ->insert($insert_data);

                    if($employee_data)
                    {
                        DB::table('employees')
                        ->where('employees.id', $request->get('emp_id'))
                        ->update($employee_data);
                    }

                    DB::commit();
                }
                elseif($form == 'edit_work_experience')
                {
                    $rule = ['work_present' => 'in:0,1', 'start_date' => 'required'];

                    $work_present = $request->get('work_present');

                    if($work_present == 0) $rule['end_date'] = 'required';
                    
                    $rule = array_merge($rule, [
                    'employment_status' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_employment_status(), 'id')),
                    'position_name' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_positions(), 'id')),
                    'agency_name' => 'required|in:'.implode(',', $this->get_value_by($this->list_agencies(), 'id')),
                    'pay_rate' => 'in:'.implode(',', array_keys(config('params.pay_rates'))),
                    'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                    'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                    'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                    'unit' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_units(), 'id')),
                    'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id')),
                    'designation' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_designations(), 'id')),
                    'salary_amount' => 'required',
                    'gevernment_service' => 'in:0,1',
                    'place_of_assignment' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_assignments(), 'id'))
                    ]);

                    $this->validate_request($request->all(), $rule);

                    $update_data = [
                    'agency_id' => $request->get('agency_name'),
                    'position_id' => $request->get('position_name'),
                    'workexp_start_date' => $request->get('start_date'),
                    'workexp_end_date' => $request->get('end_date'),
                    'workexp_present' => $work_present,
                    'pay_rate' => $request->get('pay_rate'),
                    'salary_grade' => $request->get('salary_grade'),
                    'salary_step' => $request->get('salary_step'),
                    'monthly_salary' => str_replace(',', '', $request->get('salary_amount')),
                    'employment_status_id' => $request->get('employment_status'), 
                    'sector_id' => $request->get('sector'),
                    'department_id' => $request->get('department'),
                    'division_id' => $request->get('division'),
                    'unit_id' => $request->get('unit'),
                    'designation_id' => $request->get('designation'),
                    'office_id' => $request->get('office'),
                    'government_service' => $request->get('government_service'),
                    'assignment_id' => $request->get('place_of_assignment'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ];

                    DB::beginTransaction();

                    if($request->get('employment_status') == 3)
                    {
                        $first_work_exp_start_date = null;

                        if($get_assumption_date = $this->get_first_work_exp($emp_id)) 
                        {
                            $first_work_exp_start_date = $get_assumption_date->workexp_start_date;

                            if($get_assumption_date->id == $request->get('item_id'))
                            {
                                $first_work_exp_start_date = $request->get('start_date');
                            }
                        }

                       //dd($first_work_exp_start_date);
                        
                        $empinfo_data = [
                            'position_id' => $request->get('position_name'),
                            'agency_id' => $request->get('agency_name'),
                            'department_id' => $request->get('department'),
                            'office_id' => $request->get('office'),
                            'division_id' => $request->get('division'),
                            'unit_id' => $request->get('unit'),
                            'employment_status_id' => $request->get('employment_status'),
                            'monthly_salary' => str_replace(',', '', $request->get('salary_amount')),
                        ];

                        if($this->count_empinfo_by($emp_id) == 1)
                        {
                            $empinfo_data = [
                            'hired_date' => $first_work_exp_start_date,
                            'updated_by' => Auth::user()->id,
                            'updated_at' => DB::raw('now()')
                            ];

                            DB::table('employee_informations')
                            ->where('employee_id', '=', $emp_id)
                            ->update($empinfo_data);
                        }
                        else
                        {
                            $empinfo_data['hired_date'] = $first_work_exp_start_date;

                            $empinfo_data = array_merge($empinfo_data, [
                            'employee_id' => $emp_id,
                            'created_by' => Auth::user()->id,
                            'created_at' => DB::raw('now()')
                            ]);

                            DB::table('employee_informations')
                            ->insert($empinfo_data);
                        }
                    }

                    if($work_present == 1)
                    {
                        DB::table('employee_work_experience')
                        ->where('employee_work_experience.employee_id', $emp_id)
                        ->where('employee_work_experience.workexp_present', '1')
                        ->where('employee_work_experience.deleted_at', null)
                        ->update(['workexp_end_date' => null]);

                        DB::table('employee_work_experience')
                        ->where('employee_work_experience.employee_id', $emp_id)
                        ->where('employee_work_experience.deleted_at', null)
                        ->update(['workexp_present' => '0']);
                    }

                    DB::table('employee_work_experience')
                    ->where('employee_work_experience.id', $request->get('item_id'))
                    ->update($update_data);
      
                    DB::commit();
                }
                elseif($form == 'remove_work_experience')
                {
                    DB::beginTransaction();

                    DB::table('employee_work_experience')
                    ->where('employee_work_experience.id', $request->get('work_experience_id'))
                    ->update([
                    'updated_by' => Auth::user()->id,
                    'deleted_at' => DB::raw('now()')
                    ]);
      
                    DB::commit();
                }
            }
            elseif($option == 'employee_information')
            {
                $update_data = array_merge($update_data, [
                'hired_date' => $request->get('hired_date'),
                'assumption_date' => $request->get('assumption_date'),
                'resign_date' => $request->get('resign_date'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date')
                ]);

                DB::beginTransaction();

                DB::table("employee_informations")
                ->where('id', '=', $request->get('id'))
                ->update($update_data); 

                DB::commit();    
            }
            elseif($option == 'employee_service_record')
            {
                $employee_id = $request->get('employee_id');
                if(!$employee = $this->check_exist_employee_by($employee_id)) throw new Exception(trans('page.no_record_found'));

                $service_record_id = $request->get('service_record_id');
                if(!$service_record = $this->get_work_experience($service_record_id)) throw new Exception(trans('page.no_record_found'));

                $former_incumbent = $request->get('former_incumbent');
                $workexp_start_date = $request->get('workexp_start_date');
                $workexp_end_date = $request->get('workexp_end_date');
                $workexp_present = $request->get('workexp_present');

                $plantilla_item = $request->get('plantilla_item');
                $position = $request->get('position');
                $agency = $request->get('agency');
                $sector = $request->get('sector');
                $department = $request->get('department');
                $office = $request->get('office');
                $division = $request->get('division');
                $branch = $request->get('branch');
                $section = $request->get('section');
                $unit = $request->get('unit');
                $location = $request->get('location');

                $employment_status = $request->get('employment_status');
                $nature_of_appointment = $request->get('nature_of_appointment');
                $pay_rate = $request->get('pay_rate');
                $salary_grade = $request->get('salary_grade');
                $salary_step = $request->get('salary_step');
                $salary_amount = str_replace(',', '', $request->get('salary_amount'));
                $lwop = $request->get('lwop');
                $lwop_date = $request->get('lwop_date');
                $seperated_date = $request->get('seperated_date');
                $reason = $request->get('reason');

                $rule = [
                'workexp_start_date' => 'required',
                'workexp_present' => 'in:0,1',
                ];

                if($workexp_present == 0) $rule['workexp_end_date'] = 'required';
                
                $rule = array_merge($rule, [
                'plantilla_item' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_plantilla_items(), 'id')),
                'position' => 'required|in:'.implode(',', $this->get_value_by($this->list_positions(), 'id')),
                'agency' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_agencies(), 'id')),
                'branch' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_branches(), 'id')),
                'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id')),
                'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                
                'section' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sections(), 'id')),
                'unit' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_units(), 'id')),
                'employment_status' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_employment_status(), 'id')),
                'nature_of_appointment' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_appointment_status(), 'id')),

                'pay_rate' => 'required|in:'.implode(',', array_keys(config('params.pay_rates'))),
                'salary_grade' => 'sometimes|nullable|in:'.implode(',', $this->salary_grade),
                'salary_step' => 'sometimes|nullable|in:'.implode(',', $this->step),
                'salary_amount' => 'required',
                'lwop' => 'sometimes|nullable|max:50',
                'reason' => 'sometimes|nullable|max:255',
                ]);

                $this->validate_request($request->all(), $rule);




                DB::beginTransaction();

                $update_data = [
                'workexp_start_date' => $workexp_start_date,
                'workexp_present' => $workexp_present,
                'former_incumbent' => $former_incumbent,
                'appointment_status_id' => $nature_of_appointment,
                'plantilla_item_id' => $plantilla_item,
                'position_id' => $position,
                'agency_id' => $agency,
                'department_id' => $department,
                'office_id' => $office,
                'division_id' => $division,
                'branch_id' => $branch,
                'section_id' => $section,
                'sector_id' => $sector,
                'unit_id' => $unit,
                'location_id' => $location,
                'employment_status_id' => $employment_status,
                'pay_rate' => $pay_rate,
                'salary_grade' => $salary_grade,
                'salary_step' => $salary_step,
                'monthly_salary' => $salary_amount,
                'lwop' => $lwop,
                'lwop_date' => $lwop_date,
                'seperated_date' => $seperated_date,
                'reason' => $reason,
                'government_service' => 1,
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                //throw new Exception(implode(',' , $update_data));

                // throw new Exception($update_data['salary_step']);

                if($workexp_present == 0)
                {
                    $update_data['workexp_end_date'] = $workexp_end_date;
                }
                elseif($workexp_present == 1)
                {
                    DB::table('employee_work_experience')
                    ->where('employee_work_experience.employee_id', $employee_id)
                    ->where('employee_work_experience.workexp_present', '1')
                    ->where('employee_work_experience.deleted_at', null)
                    ->update(['workexp_end_date' => null]);

                    DB::table('employee_work_experience')
                    ->where('employee_work_experience.employee_id', $employee_id)
                    ->where('employee_work_experience.deleted_at', null)
                    ->update(['workexp_present' => '0']);

                    $empinfo_data = [
                        'appointment_status_id' => $nature_of_appointment,
                        'plantilla_item_id' => $plantilla_item,
                        'position_id' => $position,
                        'agency_id' => $agency,
                        'department_id' => $department,
                        'office_id' => $office,
                        'division_id' => $division,
                        'branch_id' => $branch,
                        'section_id' => $section,
                        'sector_id' => $sector,
                        'unit_id' => $unit,
                        'location_id' => $location,
                        'employment_status_id' => $employment_status,
                        'salary_grade' => $salary_grade,
                        'salary_step' => $salary_step,
                        'monthly_salary' => $salary_amount,
                    ];

                    if($this->count_empinfo_by($employee_id) == 1)
                    {
                        $empinfo_data = array_merge($empinfo_data, [
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                        ]);

                        DB::table('employee_informations')
                        ->where('employee_id', '=', $employee_id)
                        ->update($empinfo_data);
                    }
                }
                DB::table('employee_work_experience')
                ->where('employee_work_experience.id', $service_record_id)
                ->update($update_data);

                $lastID = DB::getPdo()->lastInsertId();

                DB::commit();
            }
            elseif($option == 'change_rate')
            {
                $emp_id = $request->get('emp_id');
                $id = $request->get('item_id');

                $this->check_exist_employee($emp_id);

                if(!$employee_change_rate = $this->check_exist_employee_change_rate($emp_id, $id)) throw new Exception('No Record Found');

                $rule = [
                //'effective_date' => 'required',
                'new_rate' => 'required',
                'remarks' => 'sometimes|nullable|max:255',
                ];

                $this->validate_request($request->all(), $rule);

                $new_rate = str_replace(',', '', $request->get('new_rate'));

                $update_data = [
                //'effective_date' => $request->get('effective_date'),
                'new_rate' => $new_rate,
                'remarks' => $request->get('remarks'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                if(!$work_exp = $this->get_work_experience($employee_change_rate->workexp_id)) throw new Exception('No Record Found.');

                DB::beginTransaction();

                DB::table('employee_change_rate')
                ->where('employee_change_rate.id', $id)
                ->update($update_data);

                DB::table('employee_work_experience')
                ->where('employee_work_experience.id', $employee_change_rate->workexp_id)
                ->update([
                'monthly_salary' => $new_rate
                ]);

                DB::commit();
            }
            elseif($option == 'reassignment')
            {
                $emp_id = $request->get('emp_id');
                $id = $request->get('item_id');

                $this->check_exist_employee($emp_id);

                $this->check_exist_employee_reassignment($emp_id, $id);

                $rule = [
                'effective_date' => 'required',
                'reassignment_no' => 'required',
                'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                'unit' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_units(), 'id')),
                'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id')),
                'designation' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_designations(), 'id'))
                ];

                $this->validate_request($request->all(), $rule);

                $update_data = [
                'effective_date' => $request->get('effective_date'),
                'reassignment_no' => $request->get('reassignment_no'),
                'sector_id' => $request->get('sector'),
                'department_id' => $request->get('department'),
                'division_id' => $request->get('division'),
                'unit_id' => $request->get('unit'),
                'office_id' => $request->get('office'),
                'designation_id' => $request->get('designation'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employee_reassignment')
                ->where('employee_reassignment.id', $id)
                ->update($update_data);

                DB::commit();
            }
            elseif($option == 'designation')
            {
                $emp_id = $request->get('emp_id');
                $id = $request->get('item_id');

                $this->check_exist_employee($emp_id);

                $this->check_exist_employee_designation($emp_id, $id);

                $rule = [
                'effective_date' => 'required',
                'position_title' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_positions(), 'id')),
                'sector' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_sectors(), 'id')),
                'department' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_departments(), 'id')),
                'division' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_divisions(), 'id')),
                'unit' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_units(), 'id')),
                'office' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_offices(), 'id')),
                'designation' => 'sometimes|nullable|in:'.implode(',', $this->get_value_by($this->list_designations(), 'id'))
                ];

                $this->validate_request($request->all(), $rule);

                $update_data = [
                'employee_id' => $request->get('emp_id'),
                'effective_date' => $request->get('effective_date'),
                'position_id' => $request->get('position_title'),
                'sector_id' => $request->get('sector'),
                'department_id' => $request->get('department'),
                'division_id' => $request->get('division'),
                'unit_id' => $request->get('unit'),
                'office_id' => $request->get('office'),
                'designation_id' => $request->get('designation'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employee_designation')
                ->where('employee_designation.id', $id)
                ->update($update_data);

                DB::commit();
            }
        }
        catch(Exception $e)
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response(['success' => $lastID], 201);
    }

    public function delete_personal_info(request $request, $option, $id)
    {
        try
        {
            $this->is_personal_information_option_exist($option);

            if($option == '201_update')
            {
                if(!$employee_update = $this->check_exist_employee_update_by($id)) throw new Exception(trans('page.no_record_found'));

                $list_civilstatus = $this->list_civilstatus();

                DB::beginTransaction();

                $last_record = $this->get_latest_employee_update($employee_update->employee_id, $employee_update->field_name);

                if($id >= $last_record->id)
                {
                    if(!in_array($last_record->field_name, array_column($this->fieldnames, 'field_name'))) throw new Exception('field not existing');

                    $field_name = $last_record->field_name == 'spouse_telephone_number' ? 'telephone_number' : $last_record->field_name;

                    $old_value = $last_record->old_value;

                    if(in_array($last_record->field_name, ['father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                    {
                        DB::table('employee_family')
                        ->where('employee_family.employee_id', '=', $last_record->employee_id)
                        ->update([
                        $field_name => $old_value
                        ]);
                    }
                    else
                    {   
                        $update_employee = [
                            $field_name => $old_value
                        ];
                        
                        if($field_name == 'civil_status_id')
                        {
                            if(in_array($old_value, $this->get_value_by($list_civilstatus, 'id')))
                            {
                                if($old_value !== 5) 
                                {
                                    $update_employee['other_civil_status'] = ''; 
                                }
                            }
                            else
                            {
                                $update_employee['civil_status_id'] = 5;
                                $update_employee['other_civil_status'] = $old_value;
                            }
                        }

                        DB::table('employees')
                        ->where('employees.id', '=', $last_record->employee_id)
                        ->update($update_employee);
                    }
                }

                DB::table('employee_updates')
                ->where('employee_updates.id', '=', $id)
                ->update([
                'is_deleted' => '1',
                'updated_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
            elseif($option == 'employee_service_record')
            {
                DB::beginTransaction();

                DB::table("employee_work_experience")
                ->where('id', '=',$id)
                ->update([
                'deleted_at' => DB::raw('now()'),
                'updated_by' => Auth::user()->id
                ]); 

                DB::commit();
            }
            elseif($option == '201_attachment')
            {
                DB::beginTransaction();

                DB::table("employee_attachment")
                ->where('id', '=',$id)
                ->update([
                'deleted_at' => DB::raw('now()'),
                'updated_by' => Auth::user()->id
                ]); 

                DB::commit();
            }
            elseif($option == 'change_rate')
            {
                if(!$employee_change_rate = $this->check_exist_employee_change_rate(null, $id)) throw new Exception('No Record Found');

                DB::beginTransaction();

                DB::table('employee_change_rate')
                ->where('employee_change_rate.id', '=',$id)
                ->update([
                'deleted_at' => DB::raw('now()'),
                'updated_by' => Auth::user()->id
                ]); 

                DB::table('employee_work_experience')
                ->where('employee_work_experience.id', $employee_change_rate->workexp_id)
                ->update([
                'workexp_present' => '0',
                'deleted_at' => DB::raw('now()'),
                'updated_by' => Auth::user()->id
                ]); 

                DB::commit();
            }
            elseif($option == 'reassignment')
            {
                DB::beginTransaction();

                DB::table("employee_reassignment")
                ->where('id', '=',$id)
                ->update([
                'deleted_at' => DB::raw('now()'),
                'updated_by' => Auth::user()->id
                ]); 

                DB::commit();
            }
            elseif($option == 'designation')
            {
                DB::beginTransaction();

                DB::table("employee_designation")
                ->where('id', '=',$id)
                ->update([
                'deleted_at' => DB::raw('now()'),
                'updated_by' => Auth::user()->id
                ]); 

                DB::commit();
            }
        }
        catch(Exception $e)
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response('success', 204);
    }

    public function download_201_attachment($option, $id)
    {
        try
        {
            if($employee_attachment = $this->get_employee_attachment_by($id))
            {
                $file_name = $employee_attachment->file_name.'.'.$employee_attachment->file_extension;

                $file_directory = public_path().'/tieza/attachment/'.$employee_attachment->employee_id.'/'.$employee_attachment->file_type.'/'.$employee_attachment->file_name.'_'.$employee_attachment->token_code.'.'.$employee_attachment->file_extension;

                if(!file_exists($file_directory)) throw new Exception('File not Found.');
            }
            else
            {
                throw new Exception(trans('page.record_not_found'));
            }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response()->download($file_directory, $file_name);
    }  

    public function view_other_info(request $request, $option, $id, $target, $tid)
    {
        try
        {
            $this->is_personal_information_option_exist($option);

            $default_data = ['module' => $this->module, 'option' => $option, 'target' => $target, 'employee_id' => $id, 'item_id' => $tid];

            if($this->count_employee_by($id) == 0) throw new Exception(trans('page.no_record_found'));
            
            if($option == 'pds_file')
            {
                $default_data['this_url'] = url('personal_information/');

                if(Auth::user()->isUser())
                {
                    $default_data['this_url'] = url('user/personal_information/');
                }

                if($target == 'children')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.children.form']);
                }
                elseif($target == 'education')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.education.form', 'educational_level' => $this->educational_level, 'list_schools' => $this->list_schools(), 'list_courses' => $this->list_courses()]);
                }
                elseif($target == 'eligibility')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.eligibility.form', 'list_eligibilities' => $this->list_eligibilities()]);
                }
                elseif($target == 'other_info')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.other_info.form', 'category' => $request->input('category')]);
                }
                elseif($target == 'reference')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.reference.form']);
                }
                elseif($target == 'voluntary_work')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.voluntary_work.form', 'list_organizations' => $this->list_organizations()]);
                }
                elseif($target == 'training_program')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.training_program.form', 'list_trainings' => $this->list_trainings(), 'list_training_types' => $this->list_training_types(), 'list_training_description' => $this->get_training_description()]);
                }
                elseif($target == 'work_experience')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.work_experience.form', 'list_agencies' => $this->list_agencies(), 'list_positions' => $this->list_positions(), 'list_employment_status' => $this->list_employment_status(), 'departments' => $this->list_departments(), 'units' => $this->list_units(), 'divisions' => $this->list_divisions(), 'offices' => $this->list_offices(), 'designations' => $this->list_designations(), 'list_sectors' => $this->list_sectors(), 'employee' => $this->get_employee($id), 'list_assignments' => $this->list_assignments(), 'plantilla_employment_status' => $this->list_employment_status('0')]);
                }

                $file = 'personal_information.pds_file.view';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }

        return view($file, $data);
    }

    public function create_other_info(request $request, $option, $id, $target)
    {
        try
        {
            $this->is_personal_information_option_exist($option);

            $default_data = ['module' => $this->module, 'option' => $option, 'target' => $target, 'employee_id' => $id];

            if($this->count_employee_by($id) == 0) throw new Exception(trans('page.no_record_found'));

            if($option == 'pds_file')
            {
                $default_data['this_url'] = url('personal_information/');

                if(Auth::user()->isUser())
                {
                    $default_data['this_url'] = url('user/personal_information/');
                }

                if($target == 'children')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.children.form']);
                }
                elseif($target == 'education')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.education.form', 'educational_level' => $this->educational_level, 'list_schools' => $this->list_schools(), 'list_courses' => $this->list_courses()]);
                }
                elseif($target == 'eligibility')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.eligibility.form', 'list_eligibilities' => $this->list_eligibilities()]);
                }
                elseif($target == 'other_info')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.other_info.form', 'category' => $request->input('category')]);
                }
                elseif($target == 'reference')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.reference.form']);
                }
                elseif($target == 'voluntary_work')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.voluntary_work.form', 'list_organizations' => $this->list_organizations()]);
                }
                elseif($target == 'training_program')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.training_program.form', 'list_trainings' => $this->list_trainings(), 'list_training_types' => $this->list_training_types(), 'list_training_description' => $this->get_training_description()]);
                }
                elseif($target == 'work_experience')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.work_experience.form', 'list_agencies' => $this->list_agencies(), 'list_positions' => $this->list_positions(), 'list_employment_status' => $this->list_employment_status(), 'departments' => $this->list_departments(), 'units' => $this->list_units(), 'divisions' => $this->list_divisions(), 'offices' => $this->list_offices(), 'designations' => $this->list_designations(), 'list_sectors' => $this->list_sectors(), 'employee' => $this->get_employee($id), 'list_assignments' => $this->list_assignments(), 'plantilla_employment_status' => $this->list_employment_status('0')]);
                }

                $file = 'personal_information.pds_file.edit';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }

        return view($file, $data);
    }

    public function edit_other_info(request $request, $option, $id, $target, $tid)
    {
        try
        {
            $this->is_personal_information_option_exist($option);

            $default_data = ['module' => $this->module, 'option' => $option, 'target' => $target, 'employee_id' => $id, 'item_id' => $tid];

            if($this->count_employee_by($id) == 0) throw new Exception(trans('page.no_record_found'));
            
            if($option == 'pds_file')
            {
                $default_data['this_url'] = url('personal_information/');

                if(Auth::user()->isUser())
                {
                    $default_data['this_url'] = url('user/personal_information/');
                }

                if($target == 'children')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.children.form']);
                }
                elseif($target == 'education')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.education.form', 'educational_level' => $this->educational_level, 'list_schools' => $this->list_schools(), 'list_courses' => $this->list_courses()]);
                }
                elseif($target == 'eligibility')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.eligibility.form', 'list_eligibilities' => $this->list_eligibilities()]);
                }
                elseif($target == 'other_info')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.other_info.form', 'category' => $request->input('category')]);
                }
                elseif($target == 'reference')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.reference.form']);
                }
                elseif($target == 'voluntary_work')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.voluntary_work.form', 'list_organizations' => $this->list_organizations()]);
                }
                elseif($target == 'training_program')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.training_program.form', 'list_trainings' => $this->list_trainings(), 'list_training_types' => $this->list_training_types(), 'list_training_description' => $this->get_training_description()]);
                }
                elseif($target == 'work_experience')
                {
                    $data = array_merge($default_data, ['file' => 'personal_information.pds_file.201_update.work_experience.form', 'list_agencies' => $this->list_agencies(), 'list_positions' => $this->list_positions(), 'list_employment_status' => $this->list_employment_status(), 'departments' => $this->list_departments(), 'units' => $this->list_units(), 'divisions' => $this->list_divisions(), 'offices' => $this->list_offices(), 'designations' => $this->list_designations(), 'list_sectors' => $this->list_sectors(), 'employee' => $this->get_employee($id), 'list_assignments' => $this->list_assignments(), 'plantilla_employment_status' => $this->list_employment_status('0')]);
                }

                $file = 'personal_information.pds_file.edit';
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }

        return view($file, $data);
    }

    public function print_pds_form($option, $id)
    {
        // Page 1
        $employee = $this->get_employee_by_id($id);

        if($employee->gsis) $employee->gsis = $this->check_format_ids($employee->gsis, 10);
        if($employee->philhealth) $employee->philhealth = $this->check_format_ids($employee->philhealth, 12, 4);
        if($employee->pagibig) $employee->pagibig = $this->check_format_ids($employee->pagibig, 12, 4);
        if($employee->tin) $employee->tin = $this->check_format_ids($employee->tin, 12, 3, 12, STR_PAD_RIGHT);
        if($employee->sss) $employee->sss = $this->check_format_ids($employee->sss, 10);

        $residential_address = DB::table('cities')
        ->where('cities.id', $employee->residential_city_id)
        ->leftjoin('provinces', 'provinces.id', '=', 'cities.province_id')
        ->select('cities.*', 'provinces.name as province_name')
        ->first();

        $permanent_address = DB::table('cities')
        ->where('cities.id', $employee->permanent_city_id)
        ->leftjoin('provinces', 'provinces.id', '=', 'cities.province_id')
        ->select('cities.*', 'provinces.name as province_name')
        ->first();

        $blood_type = DB::table('blood_types')
        ->where('blood_types.id', $employee->blood_type_id)
        ->first();

        // list of children

        $list_employee_children = json_decode(json_encode($this->get_children_information(null, $id)),true);
        $chunk_employee_children = array_chunk($list_employee_children, 12);
        $page_1_max_children = count($chunk_employee_children);

        // list of schools

        $basic_education = config('params.basic_education');

        $list_employee_education_primary = json_decode(json_encode($this->get_education_background(null, $id, 1, 'DESC')),true); 
        $count_primary = count($list_employee_education_primary);

        $list_employee_education_secondary = json_decode(json_encode($this->get_education_background(null, $id, 2, 'DESC')),true); 
        $count_secondary = count($list_employee_education_secondary);

        $list_employee_education_vocational = json_decode(json_encode($this->get_education_background(null, $id, 3, 'DESC')),true); 
        $count_vocational = count($list_employee_education_vocational);

        $list_employee_education_college = json_decode(json_encode($this->get_education_background(null, $id, 4, 'DESC')),true); 
        $count_college = count($list_employee_education_college);

        $list_employee_education_graduate = json_decode(json_encode($this->get_education_background(null, $id, 5, 'DESC')),true); 
        $count_graduate = count($list_employee_education_graduate);

        $page_1_max_schools = max($count_primary, $count_secondary, $count_vocational, $count_college, $count_graduate);

        $page_1_max_schools = $page_1_max_schools > 1 ? $page_1_max_schools : 0;

        // Page 2
        
        // List of employee eligibility
        $list_employee_eligibility = json_decode(json_encode($this->list_civil_status_eligibility($id, 'DESC')),true);
        $chunk_employee_eligibility = array_chunk($list_employee_eligibility, 7);
        $page_2_max_eligibility = count($chunk_employee_eligibility);

        // List of work experience
        $list_employee_work_experience = json_decode(json_encode($this->get_work_experience(null, $id, 'DESC')),true); 
        $chunk_employee_work_experience = array_chunk($list_employee_work_experience, 28);
        $page_2_max_work_experience = count($chunk_employee_work_experience);

        
        // Page 3

        // List of employee voluntary work
        $list_employee_voluntary_work = json_decode(json_encode($this->get_voluntary_work(null, $id, 'DESC')),true);
        $chunk_employee_voluntary_work = array_chunk($list_employee_voluntary_work, 7);
        $page3_max_voluntary_work = count($chunk_employee_voluntary_work);

        // List of employee training
        $list_employee_training = json_decode(json_encode($this->get_training_program_attended(null, $id, 'DESC')),true);
        $chunk_employee_training = array_chunk($list_employee_training, 21);
        $page3_max_employee_training = count($chunk_employee_training);

        $this->get_other_info(null, $id, 0);

        $list_employee_other_info_hobbies = json_decode(json_encode($this->get_other_info(null, $id, '0')),true);
        $chunk_employee_other_info_hobbies = array_chunk($list_employee_other_info_hobbies, 7);
        $count_employee_other_info_hobbies = count($chunk_employee_other_info_hobbies);

        $list_employee_other_info_recognition = json_decode(json_encode($this->get_other_info(null, $id, '1')),true);
        $chunk_employee_other_info_recognition = array_chunk($list_employee_other_info_recognition, 7);
        $count_employee_other_info_recognition = count($chunk_employee_other_info_recognition);

        $list_employee_other_info_membership = json_decode(json_encode($this->get_other_info(null, $id, '2')),true);
        $chunk_employee_other_info_membership = array_chunk($list_employee_other_info_membership, 7);
        $count_employee_other_info_membership = count($chunk_employee_other_info_membership);

        $page3_max_other_info = max($count_employee_other_info_hobbies, $count_employee_other_info_recognition, $count_employee_other_info_membership);

        $page3_max_list_other_info = max(count($list_employee_other_info_hobbies), count($list_employee_other_info_membership), count($list_employee_other_info_membership));


        // Page 4
        $employee_pdsq = $this->get_pdsq($id);

        // List of employee reference
        $list_employee_reference = json_decode(json_encode($this->get_reference(null, $id)),true);
        $chunk_employee_reference = array_chunk($list_employee_reference, 3);
        $page4_max_reference = count($chunk_employee_reference);

        return view('personal_information.pds_file.report.pds.pds_form', [
        'employee' => $employee,
        'residential_address' => $residential_address, 
        'permanent_address' => $permanent_address, 
        'blood_type' => $blood_type,

        'chunk_employee_children' => $chunk_employee_children,
        'page_1_max_children' => $page_1_max_children,

        'basic_education' => $basic_education,

        'list_employee_education_primary' => $list_employee_education_primary,
        'count_primary' => $count_primary,
        'list_employee_education_secondary' => $list_employee_education_secondary,
        'count_secondary' => $count_secondary,
        'list_employee_education_vocational' => $list_employee_education_vocational,
        'count_vocational' => $count_vocational,
        'list_employee_education_college' => $list_employee_education_college,
        'count_college' => $count_college,
        'list_employee_education_graduate' => $list_employee_education_graduate,
        'count_graduate' => $count_graduate,
        'page_1_max_schools' => $page_1_max_schools,

        'list_employee_eligibility' => $list_employee_eligibility,
        'chunk_employee_eligibility' => $chunk_employee_eligibility,
        'page_2_max_eligibility' => $page_2_max_eligibility,

        'list_employee_work_experience' => $list_employee_work_experience,
        'chunk_employee_work_experience' => $chunk_employee_work_experience,
        'page_2_max_work_experience' => $page_2_max_work_experience,

        'list_employee_voluntary_work' => $list_employee_voluntary_work,
        'chunk_employee_voluntary_work' => $chunk_employee_voluntary_work,
        'page3_max_voluntary_work' => $page3_max_voluntary_work,

        'list_employee_training' => $list_employee_training,
        'chunk_employee_training' => $chunk_employee_training,
        'page3_max_employee_training' => $page3_max_employee_training,

        'list_employee_other_info_hobbies' => $list_employee_other_info_hobbies,
        'chunk_employee_other_info_hobbies' => $chunk_employee_other_info_hobbies,
        'count_employee_other_info_hobbies' => $count_employee_other_info_hobbies,

        'list_employee_other_info_recognition' => $list_employee_other_info_recognition,
        'chunk_employee_other_info_recognition' => $chunk_employee_other_info_recognition,
        'count_employee_other_info_recognition' => $count_employee_other_info_recognition,

        'list_employee_other_info_membership' => $list_employee_other_info_membership,
        'chunk_employee_other_info_membership' => $chunk_employee_other_info_membership,
        'count_employee_other_info_membership' => $count_employee_other_info_membership,

        'page3_max_other_info' => $page3_max_other_info,
        'page3_max_list_other_info' => $page3_max_list_other_info,

        'employee_pdsq' => $employee_pdsq,
        'list_employee_reference' => $list_employee_reference,
        'chunk_employee_reference' => $chunk_employee_reference,
        'page4_max_reference' => $page4_max_reference,


        ]);
    }

    public function print_pds_profile($option, $id)
    {
        return view('personal_information.pds_file.report.profile.print_profile', ['id' => $id]);
    }

    public function print_report(request $request)
    {
        
    }

    public function update_latest_work_exp_to_present(request $request)
    {
        try
        {
            $emp_id = $request->get('emp_id');

            $this->check_exist_employee($emp_id);

            if(!$latest_workexp = $this->get_latest_workexp($emp_id, '0')) throw new Exception('No Record Found.');

            DB::beginTransaction();

            DB::table('employee_work_experience')
            ->where('employee_work_experience.employee_id', $emp_id)
            ->update([
            'workexp_present' => '0'
            ]);

            DB::table('employee_work_experience')
            ->where('employee_work_experience.id', $latest_workexp->id)
            ->update([
            'workexp_present' => '1'
            ]);

            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response(['success' => ''], 201);
    }   
}
