<?php

namespace App\Http\Controllers;

use App\Http\Traits\Employee;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class EmployeeServiceRecordController extends Controller
{
    use Employee;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'personal_info';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $columns        = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'position_name'],
            ['data' => 'workexp_start_date'],
            ['data' => 'workexp_end_date']
        );
        return view('employee-service-record.index',
            [
                'module' => $this->module,
                'columns' => $columns,
                'option' => 'employee-service-record'
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $agencies           = $this->list_agencies();
        $departments        = $this->list_departments();
        $offices            = $this->list_offices();
        $plantilla_items    = $this->list_plantilla_items();
        $divisions          = $this->list_divisions();
        $positions          = $this->list_positions();
        $appointment_status = $this->list_appointment_status();
        $employment_status  = $this->list_employment_status();
        $salary_grade       = $this->list_salary_grade();
        $branches           = $this->list_branches();
        $locations          = $this->list_locations();
        $sections           = $this->list_sections();
        $units              = $this->list_units(); 
        $count_record       = $this->count_service_record($id);
        $employee           = $this->get_employee($id);
        $data               = $this->get_latest_workexp($id);
        
        return view('employee-service-record.create',
            [
                'module' => $this->module,
                'agencies' => $agencies,
                'departments' => $departments,
                'offices' => $offices,
                'plantilla_items' => $plantilla_items,
                'divisions' => $divisions,
                'positions' => $positions,
                'appointment_status' => $appointment_status,
                'employment_status' => $employment_status,
                'salary_grade' => $salary_grade,
                'branches' => $branches,
                'locations' => $locations,
                'sections' => $sections,
                'units' => $units,
                'data' => $data,
                'employee' => $employee,
                'employee_id' => $id,
                'count_record' => $count_record,
                'option' => 'employee-service-record'
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            
            $employee_id            = $request->get('employee_id');
            $workexp_start_date     = $request->get('workexp_start_date');
            $workexp_end_date       = $request->get('workexp_end_date');
            $workexp_present        = $request->get('workexp_present');
            $appointment_type       = $request->get('appointment_type');
            $appointment_status_id  = $request->get('appointment_status_id');
            $plantilla_item_id      = $request->get('plantilla_item_id');
            $position_id            = $request->get('position_id');
            $agency_id              = $request->get('agency_id');
            $department_id          = $request->get('department_id');
            $office_id              = $request->get('office_id');
            $division_id            = $request->get('division_id');
            $branch_id              = $request->get('branch_id');
            $location_id            = $request->get('location_id');
            $unit_id                = $request->get('unit_id');
            $employment_status_id   = $request->get('employment_status_id');
            $salary_grade           = $request->get('salary_grade');
            $salary_step            = $request->get('salary_step');
            $monthly_salary         = $request->get('monthly_salary');
            $lwop                   = $request->get('lwop');
            $lwop_date              = $request->get('lwop_date');
            $reason                 = $request->get('reason');
            $remarks                = $request->get('remarks');

            if ($workexp_present == 1) {
                $workexp_end_date = '';
                $empinfo = $this->count_empinfo_by($employee_id);
                if ($empinfo)
                {
                    $update_employee_info = [
                        'appointment_status_id' => $appointment_status_id,
                        'plantilla_item_id' => $plantilla_item_id,
                        'position_id' => $position_id,
                        'agency_id' => $agency_id,
                        'department_id' => $department_id,
                        'office_id' => $office_id,
                        'division_id' => $division_id,
                        'branch_id' => $branch_id,
                        'location_id' => $location_id,
                        'unit_id' => $unit_id,
                        'employment_status_id' => $employment_status_id,
                        'salary_grade' => $salary_grade,
                        'salary_step' => $salary_step,
                        'monthly_salary' => $monthly_salary,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];
                    DB::beginTransaction();
                    DB::table('employee_informations')
                        ->where('employee_id', '=', $employee_id)
                        ->update($update_employee_info);
                    DB::commit();
                }
                else
                {
                    $insert_employee_info = [
                        'employee_id' => $employee_id,
                        'appointment_status_id' => $appointment_status_id,
                        'plantilla_item_id' => $plantilla_item_id,
                        'position_id' => $position_id,
                        'agency_id' => $agency_id,
                        'department_id' => $department_id,
                        'office_id' => $office_id,
                        'division_id' => $division_id,
                        'branch_id' => $branch_id,
                        'location_id' => $location_id,
                        'unit_id' => $unit_id,
                        'employment_status_id' => $employment_status_id,
                        'salary_grade' => $salary_grade,
                        'salary_step' => $salary_step,
                        'monthly_salary' => $monthly_salary,
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];   
                    DB::beginTransaction();
                    DB::table('employee_informations')
                        ->insert($insert_employee_info);
                    DB::commit(); 
                }

                

                
            }
            $insert_data = [
                'employee_id' => $employee_id,
                'workexp_start_date' => $workexp_start_date,
                'workexp_end_date' => $workexp_end_date,
                'workexp_present' => $workexp_present,
                'appointment_type' => $appointment_type,
                'appointment_status_id' => $appointment_status_id,
                'plantilla_item_id' => $plantilla_item_id,
                'position_id' => $position_id,
                'agency_id' => $agency_id,
                'department_id' => $department_id,
                'office_id' => $office_id,
                'division_id' => $division_id,
                'branch_id' => $branch_id,
                'location_id' => $location_id,
                'unit_id' => $unit_id,
                'employment_status_id' => $employment_status_id,
                'salary_grade' => $salary_grade,
                'salary_step' => $salary_step,
                'monthly_salary' => $monthly_salary,
                'lwop' => $lwop,
                'lwop_date' => $lwop_date,
                'reason' => $workexp_start_date,
                'remarks' => $remarks,
                'government_service' => 1,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
            ];
            //dd($insert_data);

            
            DB::table('employee_work_experience')
                ->insert($insert_data);
            DB::commit();


        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return back()->with('danger', $e->getMessage());
            
            return response(['errors' => $data], 422);
        }

        return response(['errors' => $data], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agencies           = $this->list_agencies();
        $departments        = $this->list_departments();
        $offices            = $this->list_offices();
        $plantilla_items    = $this->list_plantilla_items();
        $divisions          = $this->list_divisions();
        $positions          = $this->list_positions();
        $appointment_status = $this->list_appointment_status();
        $employment_status  = $this->list_employment_status();
        $salary_grade       = $this->list_salary_grade();
        $branches           = $this->list_branches();
        $locations          = $this->list_locations();
        $sections           = $this->list_sections();
        $units              = $this->list_units(); 

        $data               = $this->get_service_record_by($id);
        return view('employee-service-record.edit',
            [
                'module' => $this->module,
                'agencies' => $agencies,
                'departments' => $departments,
                'offices' => $offices,
                'plantilla_items' => $plantilla_items,
                'divisions' => $divisions,
                'positions' => $positions,
                'appointment_status' => $appointment_status,
                'employment_status' => $employment_status,
                'salary_grade' => $salary_grade,
                'branches' => $branches,
                'locations' => $locations,
                'sections' => $sections,
                'units' => $units,
                'data' => $data,
                'id' => $id,
                'option' => 'employee-service-record'
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $workexp_start_date     = $request->get('workexp_start_date');
            $workexp_end_date       = $request->get('workexp_end_date');
            $workexp_present        = $request->get('workexp_present');
            $appointment_type       = $request->get('appointment_type');
            $appointment_status_id  = $request->get('appointment_status_id');
            $plantilla_item_id      = $request->get('plantilla_item_id');
            $position_id            = $request->get('position_id');
            $agency_id              = $request->get('agency_id');
            $department_id          = $request->get('department_id');
            $office_id              = $request->get('office_id');
            $division_id            = $request->get('division_id');
            $branch_id              = $request->get('branch_id');
            $location_id            = $request->get('location_id');
            $unit_id                = $request->get('unit_id');
            $employment_status_id   = $request->get('employment_status_id');
            $salary_grade           = $request->get('salary_grade');
            $salary_step            = $request->get('salary_step');
            $monthly_salary         = $request->get('monthly_salary');
            $lwop                   = $request->get('lwop');
            $lwop_date              = $request->get('lwop_date');
            $reason                 = $request->get('reason');
            $remarks                = $request->get('remarks');

            $update_data = [
                'workexp_start_date' => $workexp_start_date,
                'workexp_end_date' => $workexp_end_date,
                'workexp_present' => $workexp_present,
                'appointment_type' => $appointment_type,
                'appointment_status_id' => $appointment_status_id,
                'plantilla_item_id' => $plantilla_item_id,
                'position_id' => $position_id,
                'agency_id' => $agency_id,
                'department_id' => $department_id,
                'office_id' => $office_id,
                'division_id' => $division_id,
                'branch_id' => $branch_id,
                'location_id' => $location_id,
                'unit_id' => $unit_id,
                'employment_status_id' => $employment_status_id,
                'salary_grade' => $salary_grade,
                'salary_step' => $salary_step,
                'monthly_salary' => $monthly_salary,
                'lwop' => $lwop,
                'lwop_date' => $lwop_date,
                'reason' => $workexp_start_date,
                'remarks' => $remarks,
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
            ];
            DB::beginTransaction();
            DB::table('employee_work_experience')
                ->where('id', '=', $id)
                ->update($update_data);
            DB::commit();
        } catch (Exception $e) {
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            DB::table("employee_work_experience")
                ->where('id', '=',$id)
                ->update([
                'deleted_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()'),
                'updated_by' => Auth::user()->id
                ]); 

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return back()->with('danger', $e->getMessage());
            
            return response(['errors' => $data], 422);
        }
    }

    public function get_service_record_by_employee($id)
    {
        $data = $this->get_latest_workexp($id);
        return response(['data' => $data]);
    }
    
    public function get_service_record($id)
    {
        $data = $this->list_service_record($id);

        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) use ($id){
            return '<div class="tools text-center">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <a href="'.url('/employee-service-record/edit/'.$data->id).'" class="dropdown-item">
                                <i class="icon icon-left mdi mdi-eye"></i>Edit
                            </a>
                            <span class="dropdown-item" onclick="delete_record('.$data->id.', '.$id.');">
                                <i class="icon icon-left mdi mdi-delete"></i>Delete
                            </span>
                        </div>
                    </div>';
        });

        $datatables
        ->editColumn('workexp_end_date', function($data){
            return $data->workexp_end_date == '0000-00-00' ? 'Present' : $data->workexp_end_date;
        });

        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }
}
