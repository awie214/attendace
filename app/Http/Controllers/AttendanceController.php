<?php

namespace App\Http\Controllers;

use App\Http\Traits\Attendance;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class AttendanceController extends Controller
{
    use Attendance;

    protected $reports    = [
        'employees-absences' => 'Monthly Report on Employee\'s Absences',
        'undertimes-and-absences' => 'MONTHLY REPORT OF UNDERTIMES AND ABSENCES',
        'employees-tardiness' => 'Monthly Report on Employee\'s Tardiness',
        'employees-undertime' => 'Monthly Report on Employee\'s Undertime'
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->module = 'attendance';
    }

    public function index($option)
    {
        $array = array();
        if ($option == 'leave-application')
        {
            $folder = 'employees-leave';
            $json_url = url('/attendance/leave-application/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'employee_name'],
                ['data' => 'leave_name'],
                [ 'name' => 'filed_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'filed_date.display', "sort" => 'filed_date' ] ],
                [ 'name' => 'start_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'start_date.display', "sort" => 'start_date' ] ],
                [ 'name' => 'end_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'end_date.display', "sort" => 'end_date' ] ],
                ['data' => 'status'],
            );
        }
        if ($option == 'leave-cancellation-application')
        {
            $folder = 'employees-leave-cancellation';
            $json_url = url('/attendance/leave-cancellation-application/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'employee_name'],
                ['data' => 'leave_name'],
                [ 'name' => 'filed_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'filed_date.display', "sort" => 'filed_date' ] ],
                [ 'name' => 'start_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'start_date.display', "sort" => 'start_date' ] ],
                [ 'name' => 'end_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'end_date.display', "sort" => 'end_date' ] ],
                ['data' => 'status'],
            );
        }
        else if ($option == 'cto-application')
        {
            $folder = 'employees-cto';
            $json_url = url('/attendance/cto-application/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'employee_name'],
                ['data' => 'hours'],
                [ 'name' => 'filed_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'filed_date.display', "sort" => 'filed_date' ] ],
                [ 'name' => 'start_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'start_date.display', "sort" => 'start_date' ] ],
                [ 'name' => 'end_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'end_date.display', "sort" => 'end_date' ] ],
                ['data' => 'status'],
            );
        }
        else if ($option == 'overtime-application')
        {
            $folder = 'employees-overtime';
            $json_url = url('/attendance/overtime-application/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'employee_name'],
                ['data' => 'with_pay'],
                [ 'name' => 'filed_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'filed_date.display', "sort" => 'filed_date' ] ],
                [ 'name' => 'start_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'start_date.display', "sort" => 'start_date' ] ],
                [ 'name' => 'end_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'end_date.display', "sort" => 'end_date' ] ],
                ['data' => 'status'],
            );
        }
        else if ($option == 'change-shift-application')
        {
            $folder = 'employees-change-shift';
            $json_url = url('/attendance/change-shift-application/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'employee_name'],
                ['data' => 'work_schedule_name'],
                [ 'name' => 'filed_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'filed_date.display', "sort" => 'filed_date' ] ],
                [ 'name' => 'start_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'start_date.display', "sort" => 'start_date' ] ],
                [ 'name' => 'end_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'end_date.display', "sort" => 'end_date' ] ],
                ['data' => 'status'],
            );
        }
        else if ($option == 'office-authority-application')
        {
            $folder = 'employees-office-authority';
            $json_url = url('/attendance/office-authority-application/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'employee_name'],
                ['data' => 'absence_name'],
                [ 'name' => 'filed_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'filed_date.display', "sort" => 'filed_date' ] ],
                [ 'name' => 'start_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'start_date.display', "sort" => 'start_date' ] ],
                [ 'name' => 'end_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'end_date.display', "sort" => 'end_date' ] ],
                ['data' => 'status'],
            );
        }
        else if ($option == 'leave-monetization-application')
        {
            $folder = 'employees-leave-monetization';
            $json_url = url('/attendance/leave-monetization-application/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'employee_name'],
                [ 'name' => 'filed_date.timestamp', 'type' => 'num', 'data' => [ "_" => 'filed_date.display', "sort" => 'filed_date' ] ],
                ['data' => 'vl_amount'],
                ['data' => 'sl_amount'],
                ['data' => 'status'],
            );
        }
        else if ($option == 'leave-policy-group')
        {
            $folder = 'leave-policy-group';
            $json_url = url('/attendance/leave-policy-group/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'code'],
                ['data' => 'name'],
                ['data' => 'remarks']
            );
        }
        else if ($option == 'overtime-policy-group')
        {
            $folder = 'overtime-policy-group';
            $json_url = url('/attendance/overtime-policy-group/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'code'],
                ['data' => 'name'],
                ['data' => 'remarks']
            );
        }
        else if ($option == 'leave-policy')
        {
            $folder = 'leave-policy';
            $json_url = url('/attendance/leave-policy/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'leave_policy_group_name'],
                ['data' => 'leave_name'],
                ['data' => 'value']
            );
        }
        else if ($option == 'overtime-policy')
        {
            $folder = 'overtime-policy';
            $json_url = url('/attendance/overtime-policy/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'name'],
                ['data' => 'coc_rates_yearly'],
                ['data' => 'coc_rates_monthly']
            );
        }
        else if ($option == 'employee-setup')
        {
            $folder     = 'employee-setup';
            $json_url   = '';
            $columns    = '';
            $work_schedules         = $this->list_work_schedules();
            $leave_policy_group     = $this->list_leave_policy_group();
            $overtime_policy_group  = $this->list_overtime_policy_group();
            $array  = ['work_schedules' => $work_schedules, 'leave_policy_group' => $leave_policy_group, 'overtime_policy_group' => $overtime_policy_group];
        }
        else if ($option == 'daily-time-record')
        {
            $folder = 'daily-time-record';
            $json_url = '';
            $columns = '';
        }
        else if ($option == 'employee-credit-balance')
        {
            $folder = 'employee-credit-balance';
            $json_url = '';
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'credit_name'],
                ['data' => 'beginning_balance'],
                ['data' => 'beginning_date']
            );
        }
        else if ($option == 'dashboard')
        {
            $folder     = 'dashboard';
            $json_url   = '';
            $columns    = '';
            $employees_leave                = $this->count_application('employees_leave');
            $employees_overtime             = $this->count_application('employees_overtime');
            $employees_absence              = $this->count_application('employees_absence');
            $employees_cto                  = $this->count_application('employees_cto');
            $employees_change_shift         = $this->count_application('employees_change_shift');
            $employees_leave_monetization   = $this->count_application('employees_leave_monetization');
            $array = [
                'employees_leave' => $employees_leave,
                'employees_overtime' => $employees_overtime,
                'employees_absence' => $employees_absence,
                'employees_cto' => $employees_cto,
                'employees_change_shift' => $employees_change_shift,
                'employees_leave_monetization' => $employees_leave_monetization
            ];
        }
        else if ($option == 'reports')
        {
            $folder     = 'reports';
            $json_url   = '';
            $columns    = '';
            
            $array = [
                'reports' => $this->reports,
                'employees' => $this->list_employees()
            ];
        }
        $data = ['module' => $this->module, 'option' => $option, 'json_url' => $json_url, 'columns' => $columns, 'array' => $array];
        $file = 'attendance.'.$folder.'.index';
        return view($file, $data);
    }

    public function attendance_datatables($option)
    {
        $employee_id = 0;
        if(Auth::user()->isUser()) $employee_id = Auth::user()->employee_id;
        
        if ($option == 'leave-application')
        {
            $data = $this->list_employees_leave($employee_id);
        }
        else if ($option == 'leave-cancellation-application')
        {
            $data = $this->list_employees_forced_leave($employee_id);
        }
        else if ($option == 'cto-application')
        {
            $data = $this->list_employees_cto($employee_id);
        }
        else if ($option == 'change-shift-application')
        {
            $data = $this->list_employees_change_shift($employee_id);
        }
        else if ($option == 'overtime-application')
        {
            $data = $this->list_employees_overtime($employee_id);
        }
        else if ($option == 'office-authority-application')
        {
            $data = $this->list_employees_office_authority($employee_id);
        }
        else if ($option == 'leave-monetization-application')
        {
            $data = $this->list_employees_leave_monetization($employee_id);
        }
        else if ($option == 'leave-policy-group')
        {
            $data = $this->list_leave_policy_group();
        }
        else if ($option == 'overtime-policy-group')
        {
            $data = $this->list_overtime_policy_group();
        }
        else if ($option == 'leave-policy')
        {
            $data = $this->list_leave_policy();
        }
        else if ($option == 'overtime-policy')
        {
            $data = $this->list_overtime_policy();
        }
        else
        {
            $data = array();
        }
        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) use ($option) {
            if (
                $option != 'leave-policy-group' && 
                $option != 'overtime-policy-group' && 
                $option != 'leave-policy' && 
                $option != 'overtime-policy'
                )
            {
                if(Auth::user()->isUser())
                {
                    return '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/attendance/'.$option.'/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <span class="dropdown-item" onclick="delete_record(\''.$option.'\','.$data->id.')">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </span>
                                <span class="dropdown-item" onclick="print_record(\''.$option.'\','.$data->id.')">
                                    <i class="icon icon-left mdi mdi-print"></i>Print
                                </span>
                            </div>
                        </div>';
                }
                else
                {   
                    return '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/attendance/'.$option.'/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <span class="dropdown-item" onclick="delete_record(\''.$option.'\','.$data->id.')">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </span>
                                <span class="dropdown-item" onclick="approved_record(\''.$option.'\','.$data->id.')">
                                    <i class="icon icon-left mdi mdi-check"></i>Approved
                                </span>
                                <span class="dropdown-item" onclick="print_record(\''.$option.'\','.$data->id.')">
                                    <i class="icon icon-left mdi mdi-print"></i>Print
                                </span>
                            </div>
                        </div>';
                }
            }
            else
            {
                return '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/attendance/'.$option.'/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <span class="dropdown-item" onclick="delete_record(\''.$option.'\','.$data->id.')">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </span>
                            </div>
                        </div>';
            }
        });
        if ($option != 'leave-policy-group' && $option != 'overtime-policy-group' && $option != 'leave-policy' && $option != 'overtime-policy')
        {
            $datatables
            ->editColumn('status', function($data){
                if ($data->status == 1)
                {
                    $data->status = 'Pending';
                }
                elseif ($data->status == 2)
                {
                    $data->status = 'Approved';
                }
                elseif ($data->status == 3)
                {
                    $data->status = 'Cancelled';
                }
                elseif ($data->status == 4)
                {
                    $data->status = 'Disapproved';
                }
                return $data->status;
            });  
        }

        if(in_array($option, ['overtime-application', 'leave-application', 'cto-application', 'overtime-application', 'change-shift-application', 'office-authority-application', 'leave-monetization-application']))
        {
            if ($option == 'overtime-application')
            {
                $datatables
                ->editColumn('with_pay', function($data){
                    if ($data->with_pay == 0)
                    {
                        $data->with_pay = 'CTO';
                    }
                    elseif ($data->with_pay == 1)
                    {
                        $data->with_pay = 'OT Pay';
                    }
                    return $data->with_pay;
                });
            }

            $datatables->editColumn('filed_date', function($data){
                return [
                  'display' => e(date('m/d/Y', strtotime($data->filed_date))),
                  'timestamp' => strtotime($data->filed_date)
               ];
            });

            if(in_array($option, ['overtime-application', 'leave-application', 'cto-application', 'overtime-application', 'change-shift-application', 'office-authority-application']))
            {
                $datatables->editColumn('start_date', function($data){
                    return [
                      'display' => e(date('m/d/Y', strtotime($data->start_date))),
                      'timestamp' => strtotime($data->start_date)
                   ];
                })->editColumn('end_date', function($data){
                    return [
                      'display' => e(date('m/d/Y', strtotime($data->end_date))),
                      'timestamp' => strtotime($data->end_date)
                   ];
                });
            }
        }

        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }

    public function attendance_create($option)
    {
        $array  = array();
        if ($option == 'leave-application')
        {
            $folder = 'employees-leave';
            $array  = $this->list_leaves();
        }
        else if ($option == 'cto-application')
        {
            $folder = 'employees-cto';
        }
        else if ($option == 'overtime-application')
        {
            $folder = 'employees-overtime';
        }
        else if ($option == 'office-authority-application')
        {
            $folder = 'employees-office-authority';
            $array  = $this->list_absences();
        }
        else if ($option == 'change-shift-application')
        {
            $folder = 'employees-change-shift';
            $array  = $this->list_work_schedules();
        }
        else if ($option == 'leave-monetization-application')
        {
            $folder = 'employees-leave-monetization';
        }
        else if ($option == 'leave-policy')
        {
            $folder = $option;
            $leaves = $this->list_leaves();
            $leave_policy_group  = $this->list_leave_policy_group();
            $array  = ['leaves' => $leaves, 'leave_policy_group' => $leave_policy_group];
        }
        else
        {
            $folder = $option;
        }
        $file = 'attendance.'.$folder.'.create';
        $data = ['module' => $this->module, 'option' => $option, 'array' => $array];
        return view($file, $data);
    }

    public function attendance_edit($option, $id)
    {
        $array  = array();
        if ($option == 'leave-application')
        {
            $folder = 'employees-leave';
            $array  = $this->list_leaves();
        }
        else if ($option == 'cto-application')
        {
            $folder = 'employees-cto';
        }
        else if ($option == 'overtime-application')
        {
            $folder = 'employees-overtime';
        }
        else if ($option == 'office-authority-application')
        {
            $folder = 'employees-office-authority';
            $array  = $this->list_absences();
        }
        else if ($option == 'leave-monetization-application')
        {
            $folder = 'employees-leave-monetization';
        }
        else if ($option == 'leave-policy')
        {
            $folder = $option;
            $leaves = $this->list_leaves();
            $leave_policy_group  = $this->list_leave_policy_group();
            $array  = ['leaves' => $leaves, 'leave_policy_group' => $leave_policy_group];
        }
        else
        {
            $folder = $option;
        }

        $file = 'attendance.'.$folder.'.edit';
        $data = ['module' => $this->module, 'option' => $option, 'array' => $array, 'id' => $id];
        return view($file, $data);
    }

    public function store_application(Request $request, $option)
    {
        try {
            if ($option == 'leave-application')
            {
                $table              = 'employees_leave';
                $employees          = $request->get('employees');
                $leave_id           = $request->get('leave_id');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $force_leave        = $request->get('force_leave');
                $leave_location     = $request->get('leave_location');
                $sick_leave_reason  = $request->get('sick_leave_reason');
                $filed_date         = $request->get('filed_date');

                if(Auth::user()->isUser()) $employees = [Auth::user()->employee_id];

                
                DB::beginTransaction();
                foreach ($employees as $key => $value) {
                    $insert_data = [
                        'employee_id' => $value,
                        'filed_date' => $filed_date,
                        'leave_id' => $leave_id,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'force_leave' => $force_leave,
                        'leave_location' => $leave_location,
                        'sick_leave_reason' => $sick_leave_reason,
                        'status' => '1',
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];
                    DB::table($table)
                    ->insert($insert_data);
                }
                DB::commit();
            }
            else if ($option == 'cto-application')
            {
                $table              = 'employees_cto';
                $employees          = $request->get('employees');
                $hours              = $request->get('hours');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $filed_date         = $request->get('filed_date');

                if(Auth::user()->isUser()) $employees = [Auth::user()->employee_id];
                
                DB::beginTransaction();
                foreach ($employees as $key => $value) {
                    $insert_data = [
                        'employee_id' => $value,
                        'filed_date' => $filed_date,
                        'hours' => $hours,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'status' => '1',
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];
                    DB::table($table)
                    ->insert($insert_data);
                }
                DB::commit();
            }
            else if ($option == 'change-shift-application')
            {
                $table              = 'employees_change_shift';
                $employees          = $request->get('employees');
                $work_schedule_id   = $request->get('work_schedule_id');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $filed_date         = $request->get('filed_date');

                if(Auth::user()->isUser()) $employees = [Auth::user()->employee_id];

                
                DB::beginTransaction();
                foreach ($employees as $key => $value) {
                    $insert_data = [
                        'employee_id' => $value,
                        'filed_date' => $filed_date,
                        'work_schedule_id' => $work_schedule_id,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'status' => '1',
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];
                    DB::table($table)
                    ->insert($insert_data);
                }
                DB::commit();
            }
            else if ($option == 'overtime-application')
            {
                $table                   = 'employees_overtime';
                $employees               = $request->get('employees');
                $with_pay                = $request->get('with_pay');
                $overtime_classification = $request->get('overtime_classification');
                $start_date              = $request->get('start_date');
                $end_date                = $request->get('end_date');
                $filed_date              = $request->get('filed_date');
                $start_time              = $request->get('start_time');
                $end_time                = $request->get('end_time');

                if(Auth::user()->isUser()) $employees = [Auth::user()->employee_id];
                
                DB::beginTransaction();
                foreach ($employees as $key => $value) {
                    $insert_data = [
                        'employee_id' => $value,
                        'filed_date' => $filed_date,
                        'with_pay' => $with_pay,
                        'overtime_classification' => $overtime_classification,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'start_time' => $start_time,
                        'end_time' => $end_time,
                        'status' => '1',
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];
                    DB::table($table)
                    ->insert($insert_data);
                }
                DB::commit();
            }
            else if ($option == 'office-authority-application')
            {
                $table              = 'employees_absence';
                $employees          = $request->get('employees');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $filed_date         = $request->get('filed_date');
                $whole_day          = $request->get('whole_day');
                $start_time         = $request->get('start_time');
                $end_time           = $request->get('end_time');
                $control_number     = $request->get('control_number');
                $destination        = $request->get('destination');
                $absence_id         = $request->get('absence_id');

                if(Auth::user()->isUser()) $employees = [Auth::user()->employee_id];
                

                
                DB::beginTransaction();
                foreach ($employees as $key => $value) {
                    $insert_data = [
                        'employee_id' => $value,
                        'absence_id' => $absence_id,
                        'filed_date' => $filed_date,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'whole_day' => $whole_day,
                        'start_time' => $start_time,
                        'end_time' => $end_time,
                        'control_number' => $control_number,
                        'destination' => $destination,
                        'status' => '1',
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];
                    DB::table($table)
                    ->insert($insert_data);
                }
                DB::commit();
            }
            else if ($option == 'leave-monetization-application')
            {
                
            }    
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201);
    }

    public function update_application(Request $request, $option, $id)
    {
           
        try {
            DB::beginTransaction();

            if ($option == 'leave-application')
            {
                $table              = 'employees_leave';
                $leave_id           = $request->get('leave_id');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $force_leave        = $request->get('force_leave');
                $leave_location     = $request->get('leave_location');
                $sick_leave_reason  = $request->get('sick_leave_reason');
                $filed_date         = $request->get('filed_date');

                
                $update_data = [
                    'filed_date' => $filed_date,
                    'leave_id' => $leave_id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'force_leave' => $force_leave,
                    'leave_location' => $leave_location,
                    'sick_leave_reason' => $sick_leave_reason,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
            }
            else if ($option == 'cto-application')
            {
                $table              = 'employees_cto';
                $filed_date         = $request->get('filed_date');
                $hours              = $request->get('hours');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                
                $update_data = [
                    'filed_date' => $filed_date,
                    'hours' => $hours,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
            }
            else if ($option == 'change-shift-application')
            {
                $table              = 'employees_change_shift';
                $filed_date         = $request->get('filed_date');
                $work_schedule_id   = $request->get('work_schedule_id');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                
                $update_data = [
                    'filed_date' => $filed_date,
                    'work_schedule_id' => $work_schedule_id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
            }
            else if ($option == 'overtime-application')
            {
                $table              = 'employees_overtime';
                $filed_date         = $request->get('filed_date');
                $with_pay           = $request->get('with_pay');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $start_time         = $request->get('start_time');
                $end_time           = $request->get('end_time');
                $overtime_classification           = $request->get('overtime_classification');
                

                $update_data = [
                    'filed_date' => $filed_date,
                    'with_pay' => $with_pay,
                    'overtime_classification' => $overtime_classification,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
            }
            else if ($option == 'office-authority-application')
            {
                $table              = 'employees_absence';
                $employees          = $request->get('employees');
                $start_date         = $request->get('start_date');
                $end_date           = $request->get('end_date');
                $filed_date         = $request->get('filed_date');
                $whole_day          = $request->get('whole_day');
                $start_time         = $request->get('start_time');
                $end_time           = $request->get('end_time');
                $control_number     = $request->get('control_number');
                $destination        = $request->get('destination');
                $absence_id         = $request->get('absence_id');
                
                $update_data = [
                    'absence_id' => $absence_id,
                    'filed_date' => $filed_date,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'whole_day' => $whole_day,
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'control_number' => $control_number,
                    'destination' => $destination,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
            }
            else if ($option == 'leave-monetization-application')
            {
                
            } 

            DB::table($table)
            ->where('id', '=', $id)
            ->update($update_data);
            DB::commit();
            
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201);
    }

    public function destroy($option, $id)
    {
           
        try {
            if ($option == 'leave-application')
            {
                $table = 'employees_leave';
            }
            else if ($option == 'cto-application')
            {
                $table = 'employees_cto';
            }
            else if ($option == 'overtime-application')
            {
                $table = 'employees_overtime';
            }
            else if ($option == 'office-authority-application')
            {
                $table = 'employees_absence';
            }
            else if ($option == 'leave-monetization-application')
            {
                $table = 'employees_leave_monetization';
            } 
            else if ($option == 'leave-policy-group')
            {
                $table = 'leave_policy_group';
            }
            else if ($option == 'overtime-policy-group')
            {
                $table = 'overtime_policy_group';
            }
            else if ($option == 'leave-policy')
            {
                $table = 'leave_policy';
            }
            else if ($option == 'overtime-policy')
            {
                $table = 'overtime_policy';
            }
            else if ($option == 'employee-credit-balance')
            {
                $table = 'employees_credit_balance';
            }
            DB::beginTransaction();
            $delete_data = [
                'updated_by' => Auth::user()->id,
                'is_deleted' => 1,
                'updated_at' => DB::raw('now()')
            ];
            DB::table($table)
            ->where('id', '=', $id)
            ->update($delete_data);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201);
    }

    public function approve_application($option, $id)
    {
           
        try {
            if ($option == 'leave-application')
            {
                $table = 'employees_leave';
            }
            else if ($option == 'cto-application')
            {
                $table = 'employees_cto';
            }
            else if ($option == 'overtime-application')
            {
                $table = 'employees_overtime';
            }
            else if ($option == 'office-authority-application')
            {
                $table = 'employees_absence';
            }
            else if ($option == 'leave-monetization-application')
            {
                $table = 'employees_leave_monetization';
            } 
            DB::beginTransaction();
            $approve_data = [
                'updated_by' => Auth::user()->id,
                'approved_by' => Auth::user()->id,
                'status' => 2,
                'updated_at' => DB::raw('now()')
            ];
            DB::table($table)
            ->where('id', '=', $id)
            ->update($approve_data);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201);
    }

    public function get_application_details($option, $id)
    {
        if ($option == 'leave-application')
        {
            $table = 'employees_leave';
        }
        else if ($option == 'cto-application')
        {
            $table = 'employees_cto';
        }
        else if ($option == 'overtime-application')
        {
            $table = 'employees_overtime';
        }
        else if ($option == 'office-authority-application')
        {
            $table = 'employees_absence';
        }
        else if ($option == 'leave-monetization-application')
        {
            $table = '';
        }
        else
        {
            $table = '';
        }
        $data = $this->get_application_data($table,$id);

        return response(['data' => $data]);
    }

    public function get_attendance_info($id)
    {
        $data = $this->get_employee_information($id);
        return response(['data' => $data]);
    }

    public function store_policy(Request $request, $option)
    {
        try {
            DB::beginTransaction();
            if ($option == 'leave-policy-group')
            {
                $table              = 'leave_policy_group';
                $code               = $request->get('code');
                $name               = $request->get('name');
                
                $insert_data = [
                    'code' => $code,
                    'name' => $name,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
            }
            else if ($option == 'overtime-policy-group')
            {
                $table              = 'overtime_policy_group';
                $code               = $request->get('code');
                $name               = $request->get('name');
                
                $insert_data = [
                    'code' => $code,
                    'name' => $name,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
            }
            else if ($option == 'leave-policy')
            {
                /*
                    leave_policy_group_id : $("#leave_policy_group_id").val(),
                    leave_id : $("#leave_id").val(),
                    value : $("#value").val(),
                    max_force_leave : $("#max_force_leave").val(),
                    offset_late: offset_late,
                    offset_undertime: offset_undertime,
                    force_leave_applied: force_leave_applied,
                    affected_by_absent: affected_by_absent,
                    accumulating: $("#accumulating").val()
                */
                $table                  = 'leave_policy';
                $leave_policy_group_id  = $request->get('leave_policy_group_id');
                $leave_id               = $request->get('leave_id');
                $value                  = $request->get('value');
                $max_force_leave        = $request->get('max_force_leave');
                $offset_late            = $request->get('offset_late');
                $offset_undertime       = $request->get('offset_undertime');
                $force_leave_applied    = $request->get('force_leave_applied');
                $affected_by_absent     = $request->get('affected_by_absent');
                $accumulating           = $request->get('accumulating');

                
                $insert_data = [
                    'leave_policy_group_id' => $leave_policy_group_id,
                    'leave_id' => $leave_id,
                    'value' => $value,
                    'max_force_leave' => $max_force_leave,
                    'offset_late' => $offset_late,
                    'offset_undertime' => $offset_undertime,
                    'force_leave_applied' => $force_leave_applied,
                    'affected_by_absent' => $affected_by_absent,
                    'accumulating' => $accumulating,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
            }
            else if ($option == 'overtime-policy')
            {
                /*
                name : $("#name").val(),
                coc_rates_working_days : $("#coc_rates_working_days").val(),
                coc_rates_weekends : $("#coc_rates_weekends").val(),
                coc_rates_holidays : $("#coc_rates_holidays").val(),
                coc_rates_monthly : $("#coc_rates_monthly").val(),
                coc_rates_yearly : $("#coc_rates_yearly").val(),
                ot_rates_working_days : $("#ot_rates_working_days").val(),
                max_ot_working_days : $("#max_ot_working_days").val(),
                ot_rates_weekends : $("#ot_rates_weekends").val(),
                max_ot_weekends : $("#max_ot_weekends").val(),
                ot_rates_holiday : $("#ot_rates_holiday").val(),
                max_ot_holiday : $("#max_ot_holiday").val()
                */

                $table                  = 'overtime_policy';
                $leave_policy_group_id  = $request->get('leave_policy_group_id');
                $leave_id               = $request->get('leave_id');
                $value                  = $request->get('value');
                $max_force_leave        = $request->get('max_force_leave');
                $offset_late            = $request->get('offset_late');
                $offset_undertime       = $request->get('offset_undertime');
                $force_leave_applied    = $request->get('force_leave_applied');
                $affected_by_absent     = $request->get('affected_by_absent');
                $accumulating           = $request->get('accumulating');

                
                $insert_data = [
                    'name' => $request->get("name"),
                    'coc_rates_working_days' => $request->get("coc_rates_working_days"),
                    'coc_rates_weekends' => $request->get("coc_rates_weekends"),
                    'coc_rates_holidays' => $request->get("coc_rates_holidays"),
                    'coc_rates_monthly' => $request->get("coc_rates_monthly"),
                    'coc_rates_yearly' => $request->get("coc_rates_yearly"),
                    'ot_rates_working_days' => $request->get("ot_rates_working_days"),
                    'max_ot_working_days' => $request->get("max_ot_working_days"),
                    'ot_rates_weekends' => $request->get("ot_rates_weekends"),
                    'max_ot_weekends' => $request->get("max_ot_weekends"),
                    'ot_rates_holiday' => $request->get("ot_rates_holiday"),
                    'max_ot_holiday' => $request->get("max_ot_holiday"),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
            }

            DB::table($table)
            ->insert($insert_data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201);
    }

    public function update_policy(Request $request, $option, $id)
    {
        try {
            DB::beginTransaction();
            if ($option == 'leave-policy-group')
            {
                $table              = 'leave_policy_group';
                $code               = $request->get('code');
                $name               = $request->get('name');
                
                $update_data = [
                    'code' => $code,
                    'name' => $name,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
                
            }
            else if ($option == 'overtime-policy-group')
            {
                $table              = 'overtime_policy_group';
                $code               = $request->get('code');
                $name               = $request->get('name');
                
                $update_data = [
                    'code' => $code,
                    'name' => $name,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
                
            }
            else if ($option == 'leave-policy')
            {
                $table                  = 'leave_policy';
                $leave_policy_group_id  = $request->get('leave_policy_group_id');
                $leave_id               = $request->get('leave_id');
                $value                  = $request->get('value');
                $max_force_leave        = $request->get('max_force_leave');
                $offset_late            = $request->get('offset_late');
                $offset_undertime       = $request->get('offset_undertime');
                $force_leave_applied    = $request->get('force_leave_applied');
                $affected_by_absent     = $request->get('affected_by_absent');
                $accumulating           = $request->get('accumulating');

                
                $update_data = [
                    'leave_policy_group_id' => $leave_policy_group_id,
                    'leave_id' => $leave_id,
                    'value' => $value,
                    'max_force_leave' => $max_force_leave,
                    'offset_late' => $offset_late,
                    'offset_undertime' => $offset_undertime,
                    'force_leave_applied' => $force_leave_applied,
                    'affected_by_absent' => $affected_by_absent,
                    'accumulating' => $accumulating,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
            }
            else if ($option == 'overtime-policy')
            {
                $table                  = 'overtime_policy';
                $update_data = [
                    'name' => $request->get("name"),
                    'coc_rates_working_days' => $request->get("coc_rates_working_days"),
                    'coc_rates_weekends' => $request->get("coc_rates_weekends"),
                    'coc_rates_holidays' => $request->get("coc_rates_holidays"),
                    'coc_rates_monthly' => $request->get("coc_rates_monthly"),
                    'coc_rates_yearly' => $request->get("coc_rates_yearly"),
                    'ot_rates_working_days' => $request->get("ot_rates_working_days"),
                    'max_ot_working_days' => $request->get("max_ot_working_days"),
                    'ot_rates_weekends' => $request->get("ot_rates_weekends"),
                    'max_ot_weekends' => $request->get("max_ot_weekends"),
                    'ot_rates_holiday' => $request->get("ot_rates_holiday"),
                    'max_ot_holiday' => $request->get("max_ot_holiday"),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
            }

            DB::table($table)
            ->where('id', '=', $id)
            ->update($update_data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201);
    }

    public function get_policy_details($option, $id)
    {
        if ($option == 'leave-policy-group')
        {
            $table = 'leave_policy_group';
        }
        else if ($option == 'overtime-policy-group')
        {
            $table = 'overtime_policy_group';
        }
        else if ($option == 'leave-policy')
        {
            $table = 'leave_policy';
        }
        else if ($option == 'overtime-policy')
        {
            $table = 'overtime_policy';
        }
        $data = $this->get_policy_data($table, $id);
        return response(['data' => $data]);
    }

    public function update_attendance_info(Request $request)
    {
        
        $id                         = $request->get('id');
        $auto_dtr                   = $request->get('auto_dtr');
        $work_schedule_id           = $request->get('work_schedule_id');
        $leave_policy_group_id      = $request->get('leave_policy_group_id');
        $overtime_policy_group_id   = $request->get('overtime_policy_group_id');
        $biometrics_id              = $request->get('biometrics_id');
        
        try {
            DB::beginTransaction();
            $update_data = [
                'auto_dtr' => $auto_dtr,
                'work_schedule_id' => $work_schedule_id,
                'leave_policy_group_id' => $leave_policy_group_id,
                'overtime_policy_group_id' => $overtime_policy_group_id,
                'biometrics' => $biometrics_id,
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
            ];

            DB::table('employee_informations')
            ->where('id', '=', $id)
            ->update($update_data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201); 
    }

    public function save_log(Request $request)
    {
        try {
            DB::beginTransaction();

            $employee_id    = $request->get('employee_id');
            $log_date       = $request->get('log_date');
            $log_time       = $request->get('log_time');
            $entry_kind     = $request->get('entry_kind');

            $obj            = explode(":", $log_time);
            $hours          = $obj[0];
            $mins           = $obj[1];
            $time_in_minutes = ($hours * 60) + $mins;

            
            $check = $this->check_log($employee_id, $log_date, $entry_kind);
            if ($check)
            {
                $update_data = [
                    'employee_id' => $employee_id,
                    'attendance_date' => $log_date,
                    'attendance_time' => $time_in_minutes,
                    'entry_kind' => $entry_kind,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
                DB::table('employees_attendance')
                ->where('id', '=', $check->id)
                ->update($update_data);
            }
            else
            {
                $insert_data = [
                    'employee_id' => $employee_id,
                    'attendance_date' => $log_date,
                    'attendance_time' => $time_in_minutes,
                    'entry_kind' => $entry_kind,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                DB::table('employees_attendance')
                ->insert($insert_data);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201); 
    }

    public function preview_form($option, $id)
    {
        $info = array();
        if ($option == 'leave-application')
        {
            $folder = 'employees-leave';
            $array  = $this->list_leaves();
            $info   = $this->get_employee_leave($id);
            $date_count = $this->date_difference($info->start_date , $info->end_date);
            $info->remarks = $date_count + 1;
        }
        else if ($option == 'cto-application')
        {
            $folder = 'employees-cto';
        }
        else if ($option == 'overtime-application')
        {
            $folder = 'employees-overtime';
        }
        else if ($option == 'office-authority-application')
        {
            $folder = 'employees-office-authority';
            $array  = $this->list_absences();
        }
        else if ($option == 'leave-monetization-application')
        {
            $folder = 'employees-leave-monetization';
        }
        else if ($option == 'leave-policy')
        {
            $folder = $option;
            $leaves = $this->list_leaves();
            $leave_policy_group  = $this->list_leave_policy_group();
            $array  = ['leaves' => $leaves, 'leave_policy_group' => $leave_policy_group];
        }
        else
        {
            $folder = $option;
        }
        $file = 'attendance.'.$folder.'.report-form';
        $data = ['module' => $this->module, 'option' => $option, 'array' => $array, 'info' => $info];
        return view($file, $data);
    }

    public function preview_report($option,$report_type)
    {
        $report         = $this->reports;
        $file           = 'attendance.reports.'.$report_type;
        $report_title   = $report[$report_type];
        $employees      = $this->list_employees();
        $data           = ['module' => $this->module, 'report_title' => $report_title, 'employees' => $employees];

        
        return view($file, $data);
    }

    public function loan_credit_balance($id)
    {
        $data = $this->list_credit_balance($id);
        $option = 'employee-credit-balance';
        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) use ($option) {
           return '<div class="tools">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <!--<a href="'.url('/attendance/'.$option.'/'.$data->id.'/edit').'" class="dropdown-item">
                                <i class="icon icon-left mdi mdi-edit"></i>Edit
                            </a>-->
                            <span class="dropdown-item" onclick="delete_record(\''.$option.'\','.$data->id.')">
                                <i class="icon icon-left mdi mdi-delete"></i>Delete
                            </span>
                        </div>
                    </div>';
        });

        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }

    public function add_credit_balance($id)
    {
        $file = 'attendance.employee-credit-balance.create';
        $option = 'employee-credit-balance';
        $data = ['module' => $this->module, 'option' => $option, 'employee_id' => $id];
        return view($file, $data);
    }

    public function save_credit_balance(Request $request, $option)
    {
        try {
            DB::beginTransaction();
            $table               = 'employees_credit_balance';
            $employee_id         = $request->get('employee_id');
            $credit_name         = $request->get('credit_name');
            $effectivity_year    = $request->get('effectivity_year');
            $beginning_date      = $request->get('beginning_date');
            $beginning_balance   = $request->get('beginning_balance');
            $expiry_date         = $request->get('expiry_date');
            $force_leave         = $request->get('force_leave');
            
            $insert_data = [
                'employee_id' => $employee_id,
                'credit_name' => $credit_name,
                'effectivity_year' => $effectivity_year,
                'beginning_date' => $beginning_date,
                'beginning_balance' => $beginning_balance,
                'expiry_date' => $expiry_date,
                'force_leave' => $force_leave,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
            ];

            DB::table($table)
            ->insert($insert_data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201);
    }
}
