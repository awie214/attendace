<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use App\Http\Traits\Employee;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

Class PersonalInformationController extends Controller
{
    use Employee;

    /**    DATATABLES    **/

    private $personal_info_url;

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {

            $this->personal_info_url = url('personal_information/');

            if(Auth::user()->isUser())
            {
                $this->personal_info_url = url('user/personal_information/');
            }

            return $next($request);
        });
    }

    public function employees_datatables($option)
    {
        if($option == '201_file')
        {
            $data = $this->list_employees();

            $datatables = Datatables::of($data)
            ->addColumn('action', function($data){
                return '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>
                            </div>
                        </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        elseif($option == '201_update')
        {   
            $list_civilstatus = $this->list_civilstatus();
            $list_bloodtypes = $this->list_bloodtypes();
            $list_cities = $this->list_cities();
            $list_occupations = $this->list_occupations();

            $data = $this->list_employee_updates();

            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($option){
                return '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="javascript:void(0);" class="dropdown-item" onclick="delete_employee_update('.$data->eu_id.')">
                                    <i class="icon icon-left mdi mdi-delete"></i>&nbsp;Delete
                                </a>
                                <form
                                role="form"
                                method="POST"
                                action="'.url('/personal_information/201_update/'.$data->eu_id.'/delete').'" 
                                id="delete_emp_'.$data->eu_id.'">'.
                                csrf_field().'
                                </form>
                            </div>
                        </div>';
            })
            ->editColumn('eu_created_at', function($data){
                return date("d M Y h:m:s", strtotime($data->eu_created_at));
            })
            ->editColumn('employee_number', function($data){
                return '<a href='.url('personal_information/profile?id='.$data->id).' target=_blank>'.$data->employee_number.'</b>';
            })
            ->editColumn('field_name', function($data){
                if(in_array($data->field_name, array_column($this->fieldnames, 'field_name')))
                {
                    $key = array_search($data->field_name, array_column($this->fieldnames, 'field_name'));
                    $field = $this->fieldnames[$key]['input_name'];
                } 

                return trans('page.'.$field);
            })
            ->editColumn('old_value', function($data) use ($list_civilstatus, $list_bloodtypes, $list_cities, $list_occupations){
                $old_value = $data->old_value;

                if($old_value !== NULL)
                {
                    if(in_array($data->field_name, ['gender']))
                    {
                        $old_value = $old_value == 'M' ? 'Male' : 'Female'; 
                    }
                    elseif(in_array($data->field_name, ['filipino']))
                    {
                        $old_value = $old_value == '1' ? 'Yes' : 'No';
                    }
                    elseif(in_array($data->field_name, ['civil_status_id']))
                    {
                        if(in_array($old_value, ['1','2','3','4']))
                        {
                            $civil_status = $list_civilstatus->first(function($item) use ($old_value) { 
                                return $item->id == $old_value;
                            });

                            $old_value = $civil_status->name;
                        }
                        else
                        {
                            $old_value = $data->old_value;
                        }
                    }
                    elseif(in_array($data->field_name, ['blood_type_id']))
                    {
                        $blood_type = $list_bloodtypes->first(function($item) use ($old_value) { 
                                return $item->id == $old_value;
                        });

                        $old_value = $blood_type->name;
                    }
                    elseif(in_array($data->field_name, ['residential_city_id', 'permanent_city_id']))
                    {
                        $city = $list_cities->first(function($item) use ($old_value) { 
                                return $item->id == $old_value;
                        });

                        $old_value = $city->name;
                    }
                    elseif(in_array($data->field_name, ['list_occupations']))
                    {
                        $occupation = $list_occupations->first(function($item) use ($old_value) { 
                                return $item->id == $old_value;
                        });

                        $old_value = $occupation->name;
                    }
                    elseif(in_array($data->field_name, ['image_path']))
                    {
                        $old_value = 'avatar.png';
                    }
                }
            
                return $old_value;
            })
            ->editColumn('new_value', function($data) use ($list_civilstatus, $list_bloodtypes, $list_cities, $list_occupations){
                $new_value = $data->new_value;

                if($new_value !== NULL)
                {
                    if(in_array($data->field_name, ['gender']))
                    {
                        $new_value = $new_value == 'M' ? 'Male' : 'Female'; 
                    }
                    elseif(in_array($data->field_name, ['filipino']))
                    {
                        $new_value = $new_value == '1' ? 'Yes' : 'No';
                    }
                    elseif(in_array($data->field_name, ['civil_status_id']))
                    {
                        // except 5 = others

                        if(in_array($new_value, ['1','2','3','4']))
                        {
                            $civil_status = $list_civilstatus->first(function($item) use ($new_value) { 
                                return $item->id == $new_value;
                            });

                            $new_value = $civil_status->name;
                        }
                    }
                    elseif(in_array($data->field_name, ['blood_type_id']))
                    {
                        $blood_type = $list_bloodtypes->first(function($item) use ($new_value) { 
                                return $item->id == $new_value;
                        });

                        $new_value = $blood_type->name;
                    }
                    elseif(in_array($data->field_name, ['residential_city_id', 'permanent_city_id']))
                    {
                        $city = $list_cities->first(function($item) use ($new_value) { 
                                return $item->id == $new_value;
                        });

                        $new_value = $city->name;
                    }
                    elseif(in_array($data->field_name, ['list_occupations']))
                    {
                        $occupation = $list_occupations->first(function($item) use ($new_value) { 
                                return $item->id == $new_value;
                        });

                        $new_value = $occupation->name;
                    }
                    elseif(in_array($data->field_name, ['image_path']))
                    {
                        $new_value = 'avatar.png';
                    }
                }
                
                return $new_value;
            })
            ->rawColumns(['action', 'employee_number'])
            ->make(true);
        }

        return $datatables;
    }

    public function employees_children_datatables($id, $action = null)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->list_employee_children($id);

        $datatables = Datatables::of($data);

        $start_tool = $this->open_tag_tools;
        $end_tool = $this->close_tag_tools;
      
        $datatables->addColumn('action', function($data) use ($start_tool, $end_tool, $action){
            if($action == 'view')
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/children/'.$data->id.'/view'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>';
            }
            else
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/children/'.$data->id.'/edit'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a><a href="javascript:void(0);" class="dropdown-item" onclick="remove_children_information('.$data->id.')"><i class="icon icon-left mdi mdi-delete"></i>Delete</a>';
            }

            return $start_tool.$tools.$end_tool;
        });

        $datatables->editColumn('child_birthday', function($data){
                return date("m/d/Y", strtotime($data->child_birthday));
        })
        ->editColumn('child_gender', function($data){
            $child_gender = '';

            if($data->child_gender !== NULL)
            {
                $child_gender = $data->child_gender == 'M' ? 'Male' : 'Female';
            }

            return $child_gender;
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_education_background_datatables($id, $action = null)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->list_education_background($id);

        $education_level = $this->educational_level;
        $basic_education = config('params.basic_education');
        $course = json_decode(json_encode($this->list_courses()), true);

        $datatables = Datatables::of($data);

        $start_tool = $this->open_tag_tools;
        $end_tool = $this->close_tag_tools;
      
        $datatables->addColumn('action', function($data) use ($start_tool, $end_tool, $action){
            if($action == 'view')
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/education/'.$data->id.'/view'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>';
            }
            else
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/education/'.$data->id.'/edit'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a><a href="javascript:void(0);" class="dropdown-item" onclick="remove_education_background('.$data->id.')"><i class="icon icon-left mdi mdi-delete"></i>Delete</a>';
            }

            return $start_tool.$tools.$end_tool;
        });

        $datatables->editColumn('year_graduated', function($data){
            $year_graduated = $data->year_graduated;

            if($year_graduated == 0) $year_graduated = 'N/A';

            return $year_graduated;
        })
        ->editColumn('end_year', function($data){
            $end_year = $data->end_year;

            if($end_year == 0) $end_year = 'N/A';

            return $end_year;
        })
        ->editColumn('education_level', function($data) use ($education_level){

            if(!in_array($data->education_level, array_column($education_level, 'id'))) return null;

            $key = array_search($data->education_level, array_column($education_level, 'id'));

            return $education_level[$key]['name'];
        })
        ->editColumn('course_id', function($data) use ($basic_education, $course){

            if(in_array($data->education_level, ['1', '2']))
            {
                return $basic_education[$data->course_id];
            }
            else
            {
                if(!in_array($data->course_id, array_column($course, 'id'))) return null;

                $key = array_search($data->course_id, array_column($course, 'id'));

                return $course[$key]['name'];
            }
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_civil_service_eligibility_datatables($id, $action = null)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->list_civil_status_eligibility($id);

        $datatables = Datatables::of($data);

        $start_tool = $this->open_tag_tools;
        $end_tool = $this->close_tag_tools;
      
        $datatables->addColumn('action', function($data) use ($start_tool, $end_tool, $action){
            if($action == 'view')
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/eligibility/'.$data->id.'/view'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>';
            }
            else
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/eligibility/'.$data->id.'/edit'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a><a href="javascript:void(0);" class="dropdown-item" onclick="remove_civil_service_eligibility('.$data->id.')"><i class="icon icon-left mdi mdi-delete"></i>Delete</a>';
            }

            return $start_tool.$tools.$end_tool;
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_voluntary_work_datatables($id, $action = null)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->get_voluntary_work(null, $id);

        $datatables = Datatables::of($data);

        $start_tool = $this->open_tag_tools;
        $end_tool = $this->close_tag_tools;
      
        $datatables->addColumn('action', function($data) use ($start_tool, $end_tool, $action){
            if($action == 'view')
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/voluntary_work/'.$data->id.'/view'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>';
            }
            else
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/voluntary_work/'.$data->id.'/edit'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a><a href="javascript:void(0);" class="dropdown-item" onclick="remove_voluntary_work('.$data->id.')"><i class="icon icon-left mdi mdi-delete"></i>Delete</a>';
            }

            return $start_tool.$tools.$end_tool;
        });

        $datatables->editColumn('voluntarywork_start_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->voluntarywork_start_date))),
              'timestamp' => strtotime($data->voluntarywork_start_date)
           ];
        })->editColumn('voluntarywork_end_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->voluntarywork_end_date))),
              'timestamp' => strtotime($data->voluntarywork_end_date)
           ];
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_training_program_datatables($id, $action = null)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->get_training_program_attended(null, $id);

        $datatables = Datatables::of($data);

        $start_tool = $this->open_tag_tools;
        $end_tool = $this->close_tag_tools;
      
        $datatables->addColumn('action', function($data) use ($start_tool, $end_tool, $action){
            if($action == 'view')
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/training_program/'.$data->id.'/view'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>';
            }
            else
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/training_program/'.$data->id.'/edit'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a><a href="javascript:void(0);" class="dropdown-item" onclick="remove_training_program('.$data->id.')"><i class="icon icon-left mdi mdi-delete"></i>Delete</a>';
            }

            return $start_tool.$tools.$end_tool;
        });

        $datatables->editColumn('training_start_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->training_start_date))),
              'timestamp' => strtotime($data->training_start_date)
           ];
        })->editColumn('training_end_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->training_end_date))),
              'timestamp' => strtotime($data->training_end_date)
           ];
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_other_info_datatables($id, $type, $action = null)
    {   
        $id = $this->UserAuthReturnID($id);

        $data = $this->get_other_info(null, $id, $type);

        $datatables = Datatables::of($data);

        $start_tool = $this->open_tag_tools;
        $end_tool = $this->close_tag_tools;
      
        $datatables->addColumn('action', function($data) use ($start_tool, $end_tool, $action){
            if($action == 'view')
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/other_info/'.$data->id.'/view'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>';
            }
            else
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/other_info/'.$data->id.'/edit'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a><a href="javascript:void(0);" class="dropdown-item" onclick="remove_other_info('.$data->id.')"><i class="icon icon-left mdi mdi-delete"></i>Delete</a>';
            }

            return $start_tool.$tools.$end_tool;
        });
    
        $datatables->editColumn('other_info_category', function($data){

            $other_info_category = config('params.other_info_category');

            if(!in_array($data->other_info_category, array_keys($other_info_category))) return null;

            $key = array_search($data->other_info_category , array_keys($other_info_category));

            return $other_info_category[$key];
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_reference_datatables($id, $action = null)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->get_reference(null, $id);

        $datatables = Datatables::of($data);

        $start_tool = $this->open_tag_tools;
        $end_tool = $this->close_tag_tools;
      
        $datatables->addColumn('action', function($data) use ($start_tool, $end_tool, $action){
            if($action == 'view')
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/reference/'.$data->id.'/view'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>';
            }
            else
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/reference/'.$data->id.'/edit'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a><a href="javascript:void(0);" class="dropdown-item" onclick="remove_reference('.$data->id.')"><i class="icon icon-left mdi mdi-delete"></i>Delete</a>';
            }

            return $start_tool.$tools.$end_tool;
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_attachment_datatables($id)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->list_201_attachments($id);

        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) use ($id){
            return '<div class="tools text-center">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <a class="dropdown-item" href="'.asset('/tieza/attachment/'.$data->employee_id.'/'.$data->file_type.'/'.$data->file_name.'_'.$data->token_code.'.'.$data->file_extension).'" target="_blank"><i class="icon icon-left mdi mdi-eye"></i>Preview</a>
                            <span class="dropdown-item" onclick=download_file('.$data->id.',"'.$data->file_name.'.'.$data->file_extension.'")>
                                <i class="icon icon-left mdi mdi-download"></i>Download
                            </span>
                            <span class="dropdown-item" onclick="delete_record('.$data->id.', '.$id.');">
                                <i class="icon icon-left mdi mdi-delete"></i>Delete
                            </span>
                        </div>
                    </div>';
        });

        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    } 

    /** Employee Service Record - Datatables **/

    public function employees_service_record_datatables($id)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->list_service_record($id);

        $datatables = Datatables::of($data);

        if(Auth::user()->isAdmin())
        {
            $datatables->addColumn('action', function($data) use ($id){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/personal_information/employee_service_record/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <span class="dropdown-item" onclick="delete_record('.$data->id.', '.$id.');">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </span>
                            </div>
                        </div>';
            });
        }
        else
        {
            $datatables->addColumn('action', function($data) use ($id){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('user/personal_information/employee_service_record/'.$data->id.'/view').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>
                            </div>
                        </div>';
            });
        }

        /**

        $datatables->editColumn('workexp_start_date', function($data){
           // return date("m/d/Y", strtotime($data->workexp_start_date));
            return $data->workexp_start_date ? with(new Carbon($data->workexp_start_date))->format('m/d/Y') : '';
        })
        ->editColumn('workexp_end_date', function($data){

            if($data->workexp_present == '1') return 'Present';
            return $data->workexp_end_date ? with(new Carbon($data->workexp_end_date))->format('m/d/Y') : '';

            //return trim($data->workexp_end_date) ? date("m/d/Y", strtotime($data->workexp_end_date)) : '';
        });


            **/

        $datatables->editColumn('workexp_start_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->workexp_start_date))),
              'timestamp' => strtotime($data->workexp_start_date)
           ];
        })
        ->editColumn('workexp_end_date', function($data){
            if($data->workexp_present == '1') return [
            'display' => 'Present',
            'timestamp' => strtotime(date('m/d/Y h:m:s'))
            ];

            return [
              'display' => $data->workexp_end_date ? e(date('m/d/Y', strtotime($data->workexp_end_date))) : '',
              'timestamp' => strtotime($data->workexp_end_date)
            ];
        });


        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_work_experience_datatables($id, $action = null)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->get_work_experience(null, $id);

        $datatables = Datatables::of($data);

        $start_tool = $this->open_tag_tools;
        $end_tool = $this->close_tag_tools;
      
        $datatables->addColumn('action', function($data) use ($start_tool, $end_tool, $action){
            if($action == 'view')
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/work_experience/'.$data->id.'/view'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>';
            }
            else
            {
                $tools = '<a href="'.$this->personal_info_url.'/pds_file/'.$data->employee_id.'/work_experience/'.$data->id.'/edit'.'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a><a href="javascript:void(0);" class="dropdown-item" onclick="remove_work_experience('.$data->id.')"><i class="icon icon-left mdi mdi-delete"></i>Delete</a>';
            }

            return $start_tool.$tools.$end_tool;
        });

        $datatables->editColumn('government_service', function($data){
            return $data->government_service == '1' ? 'Yes' : 'No';
        })->editColumn('workexp_start_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->workexp_start_date))),
              'timestamp' => strtotime($data->workexp_start_date)
           ];
        })
        ->editColumn('workexp_end_date', function($data){
            if($data->workexp_present == '1') return [
            'display' => 'Present',
            'timestamp' => strtotime(date('m/d/Y h:m:s'))
            ];

            return [
              'display' => e(date('m/d/Y', strtotime($data->workexp_end_date))),
              'timestamp' => strtotime($data->workexp_end_date)
            ];
        });


        return $datatables->rawColumns(['action'])->make(true);
    }

    /** Employee Change Rate - Datatables **/

    public function employees_change_rate_datatables($id)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->get_employee_change_rate($id);

        $datatables = Datatables::of($data);

        if(Auth::user()->isAdmin())
        {
            $datatables->addColumn('action', function($data) use ($id){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/personal_information/change_rate/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <span class="dropdown-item" onclick="delete_record('.$data->id.', '.$id.');">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </span>
                            </div>
                        </div>';
            });
        }
        else
        {
            $datatables->addColumn('action', function($data) use ($id){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('user/personal_information/change_rate/'.$data->id.'/view').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>
                            </div>
                        </div>';
            });
        }

        $datatables->editColumn('effective_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->effective_date))),
              'timestamp' => strtotime($data->effective_date)
           ];
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_reassignment_datatables($id)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->get_employee_reassignment($id);

        $datatables = Datatables::of($data);

        if(Auth::user()->isAdmin())
        {
            $datatables->addColumn('action', function($data) use ($id){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/personal_information/reassignment/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <span class="dropdown-item" onclick="delete_record('.$data->id.', '.$id.');">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </span>
                            </div>
                        </div>';
            });
        }
        else
        {
            $datatables->addColumn('action', function($data) use ($id){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('user/personal_information/reassignment/'.$data->id.'/view').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>
                            </div>
                        </div>';
            });
        }

        $datatables->editColumn('effective_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->effective_date))),
              'timestamp' => strtotime($data->effective_date)
           ];
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    public function employees_designation_datatables($id)
    {
        $id = $this->UserAuthReturnID($id);

        $data = $this->get_employee_designation($id);

        $datatables = Datatables::of($data);

        if(Auth::user()->isAdmin())
        {
            $datatables->addColumn('action', function($data) use ($id){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/personal_information/designation/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <span class="dropdown-item" onclick="delete_record('.$data->id.', '.$id.');">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </span>
                            </div>
                        </div>';
            });
        }
        else
        {
            $datatables->addColumn('action', function($data) use ($id){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('user/personal_information/designation/'.$data->id.'/view').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>
                            </div>
                        </div>';
            });
        }

        $datatables->editColumn('effective_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->effective_date))),
              'timestamp' => strtotime($data->effective_date)
           ];
        });

        return $datatables->rawColumns(['action'])->make(true);
    }

    /**    API    **/

    public function get_children_information_by($id, $employee_id = null)
    {
        try
        {
            if($this->user()) $employee_id = $this->user()->employee_id;

            $children_information = $this->check_exist_children_information($id, $employee_id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['children_information' => $children_information]);
    } 

    public function get_education_background_by($id, $employee_id = null)
    {
        try
        {
            if($this->user()) $employee_id = $this->user()->employee_id;

            $education_background = $this->check_exist_employee_education($id, $employee_id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['education_background' => $education_background]);
    }  

    public function get_eligibility_by($id, $employee_id = null)
    {
        try
        {
            if($this->user()) $employee_id = $this->user()->employee_id;

            $eligibility = $this->check_exist_eligibility($id, $employee_id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['eligibility' => $eligibility]);
    }  

    public function get_employee_pdsq_by($id)
    {
        try
        {
            $id = $this->UserAuthReturnID($id);
            
            $pdsq = $this->get_pdsq($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['pdsq' => $pdsq]);
    }

    public function get_other_info_by($id, $employee_id = null)
    {
        try
        {
            if($this->user()) $employee_id = $this->user()->employee_id;

            $other_info = $this->check_exist_other_info($id, $employee_id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['other_info' => $other_info]);
    }

    public function get_reference_by($id, $employee_id = null)
    {
        try
        {
            if($this->user()) $employee_id = $this->user()->employee_id;

            $reference = $this->check_exist_reference($id, $employee_id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['reference' => $reference]);
    }

    public function get_voluntary_work_by($id, $employee_id = null)
    {
        try
        {
            if($this->user()) $employee_id = $this->user()->employee_id;

            $voluntary_work = $this->check_exist_voluntary_work($id, $employee_id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['voluntary_work' => $voluntary_work]);
    }

    public function get_training_program_by($id, $employee_id = null)
    {
        try
        {
            if($this->user()) $employee_id = $this->user()->employee_id;

            $training_program = $this->check_exist_training_program($id, $employee_id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['training_program' => $training_program]);
    }

    public function get_work_experience_by($id, $employee_id = null)
    {
        try
        {
            if($this->user()) $employee_id = $this->user()->employee_id;

            $work_experience = $this->check_exist_work_experience($id, $employee_id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['work_experience' => $work_experience]);
    }

    public function get_civilstatus()
    {
        try
        {
            $civil_status = $this->list_civilstatus();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['civil_status' => $civil_status]);
    }

    public function get_cities()
    {
        try
        {
            $city = $this->list_cities();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['city' => $city]);
    }

    public function get_city_by($id)
    {   
        try
        {
            if($this->count_city_by($id) == 0) throw new Exception();

            $city = $this->select_city_by($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['city' => $city]);
    }

    public function get_employees()
    {
        try
        {
            $employees = $this->list_employees();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employees' => $employees, 'count_employee' => count($employees)]);
    }

    public function get_bloodtypes()
    {
        try
        {
            $blood_type = $this->list_bloodtypes();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['blood_type' => $blood_type]);
    }

    public function get_occupations()
    {
        try
        {
            $occupation = $this->list_occupations();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['occupation' => $occupation]);
    }

    public function get_employee_by($id)
    {   
        try
        {
            if($this->count_employee_by($id) == 0) throw new Exception();

            $id = $this->UserAuthReturnID($id);

            $employee = $this->get_employee_by_id($id);

            if($employee->gsis) $employee->gsis = $this->check_format_ids($employee->gsis, 10);
            if($employee->philhealth) $employee->philhealth = $this->check_format_ids($employee->philhealth, 12, 4);
            if($employee->pagibig) $employee->pagibig = $this->check_format_ids($employee->pagibig, 12, 4);
            if($employee->tin) $employee->tin = $this->check_format_ids($employee->tin, 12, 3, 12, STR_PAD_RIGHT);
            //if($employee->sss) $employee->sss = $this->check_format_ids($employee->sss, 10, 3);
            
            $gender = config('params.sex');

            $civil_status = $this->list_civilstatus();

            $blood_type = $this->list_bloodtypes();

            $city = $this->list_cities();

            $occupation = $this->list_occupations();

            $workexp = $this->get_latest_workexp($id);

            // Residential Address

            $filtered_residential_city = $city->first(function ($value, $key) use ($employee) {
                if($value->id == $employee->residential_city_id) return 'test';
            });

            $residential_address = [$employee->residential_house_number, $employee->residential_street, $employee->residential_subdivision, $employee->residential_brgy];

            if(isset($filtered_residential_city->name))
            {
                $residential_address = array_merge($residential_address, [$filtered_residential_city->name, $filtered_residential_city->province_name, $employee->residential_zip_code]);
            }

            $full_residential_address = implode(' ',  array_filter($residential_address));


            // Permanent Address

            $filtered_permanent_city = $city->first(function ($value, $key) use ($employee) {
                if($value->id == $employee->permanent_city_id) return 'test';
            });

            $permanent_address = [$employee->permanent_house_number, $employee->permanent_street, $employee->permanent_subdivision, $employee->permanent_brgy];

            if(isset($filtered_permanent_city->name))
            {
                $permanent_address = array_merge($permanent_address, [$filtered_permanent_city->name, $filtered_permanent_city->province_name, $employee->permanent_zip_code]);
            }

            $full_permanent_address = implode(' ',  array_filter($permanent_address));

            $latest_reassignment = $this->get_latest_employee_reassignment($id);

            $latest_designation = $this->get_latest_employee_designation($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee' => $employee, 'gender' => $gender, 'civil_status' => $civil_status, 'blood_type' => $blood_type, 'city' => $city, 'occupation' => $occupation, 'workexp' => $workexp, 'full_residential_address' => strtoupper($full_residential_address), 'full_permanent_address' => strtoupper($full_permanent_address), 'latest_reassignment' => $latest_reassignment, 'latest_designation' => $latest_designation]);
    }

    public function filter_employee($filter)
    {
        try
        {
            $employees = $this->filter_employee_by($filter);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employees' => $employees, 'count_employee' => count($employees)]);
    }

    public function get_employee_information_by($id)
    {
        try
        {
            $id = $this->UserAuthReturnID($id);

            $info = $this->get_employee_information($id);

            $workexp = $this->get_latest_workexp($id);

            $reassignment = $this->get_latest_employee_reassignment($id);

            $designation = $this->get_latest_employee_designation($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['data' => $info, 'workexp' => $workexp, 'reassignment' => $reassignment, 'designation' => $designation]);
    }

    public function get_employee_service_record($id)
    {
        try
        {
            $id = $this->UserAuthReturnID($id);
            
            $data = $this->get_latest_workexp($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['data' => $data]);
    }

    public function generate_number($id, $year)
    {
        $check_year         = $this->check_employee_number_by_year($year);
        $employment_status  = $this->get_data_by_id('employment_status', $id);
        $category           = $employment_status->category_status;
        $type               = 'P';
        if ($category == 0) $type = 'NP';
        if ($check_year)
        {
            $last_number = $this->get_last_employee_number($type);
            $employee_number = $last_number->employee_number;
            $obj = explode("-", $employee_number);
            $employee_number = $year.'-'.str_pad(($obj[1]+1),4,"0",STR_PAD_LEFT).'-'.$type;
        }
        else
        {
            $employee_number = $year.'-'.'0001'.'-'.$type;
        }
        return response(['data' => $employee_number]);
    }

    public function get_employment_status($id)
    {
        try
        {
            $id = $this->UserAuthReturnID($id);
            
            $data = $this->get_employment_status_by($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['data' => $data]);
    }

    public function check_file_exist(request $request)
    {
        try
        {
            $file = false;

            $emp_id = $request->get('emp_id');
            $image_path = $request->get('image_path');

            $public_path = public_path('tieza/attachment/'.$emp_id.'/'.$image_path);

            if(file_exists($public_path))
            {
                $file = true;
            }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['file' => $file]);
    }

    public function filter_employee_by_(request $request)
    {
        try
        {
            $filter_by = $request->get('filter_by');
            $list_selected = $request->get('list_filter');
            $filter_value = $request->get('filter_value');

            $query = DB::table('employees')
            ->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
            ->where(function ($query) {
                 $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
                        ->orwhere('employee_informations.resign_date', '=', '0000-00-00')
                        ->orwhere('employee_informations.resign_date', null);
            })
            ->select('employees.*', 'employee_informations.id as emp_info_id')
            ->orderBy('last_name','asc')
            ->orderBy('first_name','asc');

            if($filter_by)
            {
                if($filter_by == 'position')
                {
                    $query->join('positions', 'positions.id', 'employee_informations.position_id');

                    if($list_selected) $query->where('positions.id', $list_selected);
                }
                elseif($filter_by == 'sector')
                {
                    $query->join('sectors', 'sectors.id', 'employee_informations.sector_id');

                    if($list_selected) $query->where('sectors.id', $list_selected);
                }
                elseif($filter_by == 'department')
                {
                    $query->join('departments', 'departments.id', 'employee_informations.department_id');

                    if($list_selected) $query->where('departments.id', $list_selected);
                }
                elseif($filter_by == 'division')
                {
                    $query->join('divisions', 'divisions.id', 'employee_informations.division_id');

                    if($list_selected) $query->where('divisions.id', $list_selected);
                }
                elseif($filter_by == 'travel_tax_unit')
                {
                    $query->join('units', 'units.id', 'employee_informations.unit_id');

                    if($list_selected) $query->where('units.id', $list_selected);
                }
                elseif($filter_by == 'entities')
                {
                    $query->join('offices', 'offices.id', 'employee_informations.office_id');

                    if($list_selected) $query->where('offices.id', $list_selected);
                }
                elseif($filter_by == 'employment_status')
                {
                    $query->join('employment_status', 'employment_status.id', 'employee_informations.employment_status_id');

                    if($list_selected) $query->where('employment_status.id', $list_selected);
                }
            }

            if($filter_value)
            {
                $query->where(function ($query) use ($filter_value) {
                    $query->where('employees.last_name', 'like', $filter_value.'%')
                           //->orwhere('employees.first_name', 'like', '%'.$filter.'%')
                           //->orwhere('employees.middle_name', 'like', '%'.$filter.'%')
                           //->orwhere('employees.extension_name', 'like', '%'.$filter.'%')
                           //->orwhere(DB::raw('CONCAT(employees.first_name," ",employees.last_name)'), 'like', $filter.'%')
                           //->orwhere(DB::raw('CONCAT(employees.last_name," ",employees.first_name)'), 'like', '%'.$filter.'%')
                           //->orwhere(DB::raw('CONCAT(employees.last_name," ",employees.extension_name)'), 'like', '%'.$filter.'%')
                           ;
                });
            }
            
            
            $data = $query->get();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employees' => $data, 'count_employee' => count($data)]);
    }

    public function get_employee_change_rate_by(request $request)
    {
        try
        {
            $id = $request->get('id');
            $employee_id = $request->get('employee_id');

            $this->UserAuthReturnID($employee_id);

            $data = $this->check_exist_employee_change_rate($employee_id, $id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee_change_rate' => $data]);
    }

    public function get_employee_reassignment_by(request $request)
    {
        try
        {
            $id = $request->get('id');
            $employee_id = $request->get('employee_id');

            $this->UserAuthReturnID($employee_id);

            $data = $this->check_exist_employee_reassignment($employee_id, $id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee_reassignment' => $data]);
    }

    public function get_employee_designation_by(request $request)
    {
        try
        {
            $id = $request->get('id');
            $employee_id = $request->get('employee_id');

            $this->UserAuthReturnID($employee_id);

            $data = $this->check_exist_employee_designation($employee_id, $id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee_designation' => $data]);
    }

    public function list_renewals()
    {

        $data = $this->list_employee_renewal();

        $datatables = Datatables::of($data);

        $datatables->addColumn('action', function($data){
            return '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/personal_information/renewal/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <a href="javascript:void(0);" class="dropdown-item" onclick="remove_renewal('.$data->id.')">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </a>
                            </div>
                    </div>';
        });

        $datatables->editColumn('start_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->start_date))),
              'timestamp' => strtotime($data->start_date)
           ];
        })->editColumn('end_date', function($data){
            return [
              'display' => e(date('m/d/Y', strtotime($data->end_date))),
              'timestamp' => strtotime($data->end_date)
           ];
        });


        return $datatables->rawColumns(['action'])->make(true);
    }
}