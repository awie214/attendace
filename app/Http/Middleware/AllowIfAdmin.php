<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AllowifAdmin
{
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
	        if(Auth::user()->isAdmin()) 
	        {
	            return $next($request);
	        }
	        else
	        {
	            return redirect('/');
	        }
	    }
	    else
	    {
	        return redirect('/');
	    }
    }
}
