<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use DB;
use Exception;

trait Payroll
{
	protected $payroll_option = ['employee_policy', 'loan_info', 'benefit_info', 'deduction_info'];

	protected function is_payroll_option_exist($option)
	{
		if(!in_array($option, $this->payroll_option)) throw new Exception(trans('page.not_found', ['attribute' => strtolower(trans('page.option'))]));

		return true;
	}

	protected function is_payroll_menu_exist($option)
	{
		if(!in_array($option, array_column($this->list_payroll_menu(), 'id'))) throw new Exception(trans('page.not_found', ['attribute' => strtolower(trans('page.option'))]));

		return true;
	}

	protected function list_payroll_menu()
	{
		return array(
            ['url' => url('/payroll/payroll_employee_info'), 'id' => 'payroll_employee_info', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.payroll_employee_info')],
            ['url' => url('/payroll/annual_tax_setup'), 'id' => 'annual_tax_setup', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.annual_tax_setup')]
        );
	}

	protected function list_employees_loan($employee_id)
	{
		return DB::table('employees_loan')
		->join('employees', 'employees.id', '=', 'employees_loan.employee_id')
		->leftjoin('loans', 'loans.id', '=', 'employees_loan.loan_id')
		->select('employees_loan.*','loans.name AS loan_name')
		->where('employees_loan.employee_id', '=', $employee_id)
		->where('employees_loan.is_deleted', null)
		->get();	
	}

	protected function list_employees_benefit($employee_id)
	{
		return DB::table('employees_benefit')
		->join('employees', 'employees.id', '=', 'employees_benefit.employee_id')
		->leftjoin('benefits', 'benefits.id', '=', 'employees_benefit.benefit_id')
		->select('employees_benefit.*','benefits.name AS benefit_name')
		->where('employees_benefit.employee_id', '=', $employee_id)
		->where('employees_benefit.is_deleted', null)
		->get();	
	}
	protected function list_employees_deduction($employee_id)
	{
		return DB::table('employees_deduction')
		->join('employees', 'employees.id', '=', 'employees_deduction.employee_id')
		->leftjoin('deductions', 'deductions.id', '=', 'employees_deduction.deduction_id')
		->select('employees_deduction.*','deductions.name AS deduction_name')
		->where('employees_deduction.employee_id', '=', $employee_id)
		->where('employees_deduction.is_deleted', null)
		->get();	
	}

	// Employee Policy

	protected function count_payroll_employee_policy($id)
	{
		return DB::table('payroll_employee_policy')
		->where('payroll_employee_policy.employee_id', $id)
		->count();
	}

	protected function get_payroll_employee_policy($id = null)
	{
		$query = DB::table('employees')
		->leftjoin('payroll_employee_policy', 'payroll_employee_policy.employee_id', 'employees.id')
		->select(DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'payroll_employee_policy.*');

		if($id)
		{
			return $query->where('employees.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function check_exist_payroll_employee_policy($id)
	{
		if($this->count_payroll_employee_policy($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_payroll_employee_policy($id);
	}

	// Loan Info

	protected function count_payroll_loan_info($id)
	{
		return DB::table('employees_loan')->where('employees_loan.id', $id)->where('employees_loan.deleted_at', null)->count();
	}

	protected function get_payroll_loan_info($id = null, $emp_id = null)
	{
		$query = DB::table('employees_loan')
		->where('employees_loan.deleted_at', null)
		->join('loans', 'loans.id', 'employees_loan.loan_id')
		->join('employees', 'employees.id', 'employees_loan.employee_id')
		->select(DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'employees_loan.*', 'loans.name as loan_name');

		if($emp_id) $query->where('employees_loan.employee_id', $emp_id);

		if($id)
		{
			return $query->where('employees_loan.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function check_exist_payroll_loan_info($id)
	{
		if($this->count_payroll_loan_info($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_payroll_loan_info($id);
	}

	// Benefit Info

	protected function count_payroll_benefit_info($id)
	{
		return DB::table('employees_benefit')->where('employees_benefit.id', $id)->where('employees_benefit.deleted_at', null)->count();
	}

	protected function get_payroll_benefit_info($id = null, $emp_id = null)
	{
		$query = DB::table('employees_benefit')
		->where('employees_benefit.deleted_at', null)
		->join('benefits', 'benefits.id', 'employees_benefit.benefit_id')
		->join('employees', 'employees.id', 'employees_benefit.employee_id')
		->select('employees_benefit.*', 'benefits.name as benefit_name', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'));

		if($emp_id) $query->where('employees_benefit.employee_id', $emp_id);

		if($id)
		{
			return $query->where('employees_benefit.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function check_exist_payroll_benefit_info($id)
	{
		if($this->count_payroll_benefit_info($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_payroll_benefit_info($id);
	}

	// Deduction Info

	protected function count_payroll_deduction_info($id)
	{
		return DB::table('employees_deduction')->where('employees_deduction.id', $id)->where('employees_deduction.deleted_at', null)->count();
	}

	protected function get_payroll_deduction_info($id = null, $emp_id = null)
	{
		$query = DB::table('employees_deduction')
		->where('employees_deduction.deleted_at', null)
		->join('deductions', 'deductions.id', 'employees_deduction.deduction_id')
		->join('employees', 'employees.id', 'employees_deduction.employee_id')
		->select('employees_deduction.*', 'deductions.name as deduction_name', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'));

		if($emp_id) $query->where('employees_deduction.employee_id', $emp_id);

		if($id)
		{
			return $query->where('employees_deduction.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function check_exist_payroll_deduction_info($id)
	{
		if($this->count_payroll_deduction_info($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_payroll_deduction_info($id);
	}
}
