<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyLeaveEarnings extends Model
{
    protected $table = 'monthly_leave_earned';
    protected $fillables = array('calendar_month','vl_equivalent','sl_equivalent','created_by');
}
