<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyLeaveEarnings extends Model
{
    protected $table = 'daily_leave_earned';
    protected $fillables = array('calendar_day','vl_equivalent','sl_equivalent','created_by');
}
