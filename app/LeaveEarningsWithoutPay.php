<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveEarningsWithoutPay extends Model
{
    protected $table = 'leave_earnings_without_pay';
    protected $fillables = array('days_present','days_lwop','earnings','created_by');
}
