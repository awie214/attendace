<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function get_full_name()
    {
        
    }
}
