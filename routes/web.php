<?php

Route::any('/', 'Auth\LoginController@showLoginForm');

Auth::routes();
// Dashboard
Route::any('/', 'WebController\HomeController@index');
Route::any('/dashboard', 'DashboardController@index')->name('dashboard');
Route::any('/account', 'WebController\AccountController@index');
Route::any('/account/update', 'WebController\AccountController@update_account');

Route::any('/user', 'WebController\HomeController@user_account');
Route::any('/user/create', 'WebController\HomeController@create_user');
Route::any('/user/store', 'WebController\HomeController@store_user');
Route::any('/user/{id}/delete', 'WebController\HomeController@delete_user');
Route::any('/user/{id}/reset-pw', 'WebController\HomeController@reset_password');
Route::any('/user/{id}/edit', 'WebController\HomeController@edit_user');
Route::any('/user/{id}/update', 'WebController\HomeController@update_user');


Route::any('/admin', 'WebController\HomeController@admin_account');
Route::any('/admin/create', 'WebController\HomeController@create_admin');
Route::any('/admin/store', 'WebController\HomeController@store_admin');
Route::any('/admin/{id}/delete', 'WebController\HomeController@delete_admin');
Route::any('/admin/{id}/edit', 'WebController\HomeController@edit_admin');
Route::any('/admin/{id}/update', 'WebController\HomeController@update_admin');
Route::any('/admin/datatables', 'WebController\HomeController@list_admin_datatable');

Route::any('/import', 'WebController\HomeController@import_logs');
Route::any('/import/create-file', 'WebController\HomeController@create_logs_file');


Route::any('/home', 'WebController\HomeController@index')->name('home');
Route::any('/upload/attendance', 'WebController\HomeController@save_time');
Route::any('/attendance/{id}/{month}/{year}/preview-dtr', 'WebController\HomeController@preview_dtr');
Route::any('attendance/daily-time-record/attendance/{id}/{month}/{year}/preview-dtr', 'WebController\HomeController@preview_dtr');
Route::any('home/attendance/{id}/{month}/{year}/preview-dtr', 'WebController\HomeController@preview_dtr');

Route::any('/attendance/daily-time-record', 'WebController\HomeController@show_employee_dtr');
Route::any('/attendance/employee/datatables', 'WebController\HomeController@list_employees_datatable');

Route::any('/upload-accounts', 'WebController\HomeController@upload_accounts');
Route::any('/upload-admin-accounts', 'WebController\HomeController@upload_admin_account');