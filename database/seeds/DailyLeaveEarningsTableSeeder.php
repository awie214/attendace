<?php

use Illuminate\Database\Seeder;
use App\DailyLeaveEarnings;

class DailyLeaveEarningsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        DailyLeaveEarnings::truncate();

        $data = [
            ['calendar_day' => '1', 'vl_equivalent' => '0.42', 'sl_equivalent' => '0.42', 'created_by' => '1'],
			['calendar_day' => '2', 'vl_equivalent' => '0.83', 'sl_equivalent' => '0.83', 'created_by' => '1'],
			['calendar_day' => '3', 'vl_equivalent' => '0.125', 'sl_equivalent' => '0.125', 'created_by' => '1'],
			['calendar_day' => '4', 'vl_equivalent' => '0.167', 'sl_equivalent' => '0.167', 'created_by' => '1'],
			['calendar_day' => '5', 'vl_equivalent' => '0.208', 'sl_equivalent' => '0.208', 'created_by' => '1'],
			['calendar_day' => '6', 'vl_equivalent' => '0.25', 'sl_equivalent' => '0.25', 'created_by' => '1'],
			['calendar_day' => '7', 'vl_equivalent' => '0.292', 'sl_equivalent' => '0.292', 'created_by' => '1'],
			['calendar_day' => '8', 'vl_equivalent' => '0.333', 'sl_equivalent' => '0.333', 'created_by' => '1'],
			['calendar_day' => '9', 'vl_equivalent' => '0.375', 'sl_equivalent' => '0.375', 'created_by' => '1'],
			['calendar_day' => '10', 'vl_equivalent' => '0.417', 'sl_equivalent' => '0.417', 'created_by' => '1'],
			['calendar_day' => '11', 'vl_equivalent' => '0.458', 'sl_equivalent' => '0.458', 'created_by' => '1'],
			['calendar_day' => '12', 'vl_equivalent' => '0.5', 'sl_equivalent' => '0.5', 'created_by' => '1'],
			['calendar_day' => '13', 'vl_equivalent' => '0.542', 'sl_equivalent' => '0.542', 'created_by' => '1'],
			['calendar_day' => '14', 'vl_equivalent' => '0.583', 'sl_equivalent' => '0.583', 'created_by' => '1'],
			['calendar_day' => '15', 'vl_equivalent' => '0.625', 'sl_equivalent' => '0.625', 'created_by' => '1'],
			['calendar_day' => '16', 'vl_equivalent' => '0.667', 'sl_equivalent' => '0.667', 'created_by' => '1'],
			['calendar_day' => '17', 'vl_equivalent' => '0.708', 'sl_equivalent' => '0.708', 'created_by' => '1'],
			['calendar_day' => '18', 'vl_equivalent' => '0.75', 'sl_equivalent' => '0.75', 'created_by' => '1'],
			['calendar_day' => '19', 'vl_equivalent' => '0.792', 'sl_equivalent' => '0.792', 'created_by' => '1'],
			['calendar_day' => '20', 'vl_equivalent' => '0.833', 'sl_equivalent' => '0.833', 'created_by' => '1'],
			['calendar_day' => '21', 'vl_equivalent' => '0.875', 'sl_equivalent' => '0.875', 'created_by' => '1'],
			['calendar_day' => '22', 'vl_equivalent' => '0.917', 'sl_equivalent' => '0.917', 'created_by' => '1'],
			['calendar_day' => '23', 'vl_equivalent' => '0.958', 'sl_equivalent' => '0.958', 'created_by' => '1'],
			['calendar_day' => '24', 'vl_equivalent' => '1', 'sl_equivalent' => '1', 'created_by' => '1'],
			['calendar_day' => '25', 'vl_equivalent' => '1.042', 'sl_equivalent' => '1.042', 'created_by' => '1'],
			['calendar_day' => '26', 'vl_equivalent' => '1.083', 'sl_equivalent' => '1.083', 'created_by' => '1'],
			['calendar_day' => '27', 'vl_equivalent' => '1.125', 'sl_equivalent' => '1.125', 'created_by' => '1'],
			['calendar_day' => '28', 'vl_equivalent' => '1.167', 'sl_equivalent' => '1.167', 'created_by' => '1'],
			['calendar_day' => '29', 'vl_equivalent' => '1.208', 'sl_equivalent' => '1.208', 'created_by' => '1'],
			['calendar_day' => '30', 'vl_equivalent' => '1.25', 'sl_equivalent' => '1.25', 'created_by' => '1']
        ];

        DB::table('daily_leave_earned')->insert($data);
    }
}
