<?php

use Illuminate\Database\Seeder;
use App\WorkingMinutesConversion;

class WorkingMinutesConversionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        WorkingMinutesConversion::truncate();

        $data = [
            ['minutes' => '0', 'equivalent' => '0', 'created_by' => '1'],
			['minutes' => '1', 'equivalent' => '0.002', 'created_by' => '1'],
			['minutes' => '2', 'equivalent' => '0.004', 'created_by' => '1'],
			['minutes' => '3', 'equivalent' => '0.006', 'created_by' => '1'],
			['minutes' => '4', 'equivalent' => '0.008', 'created_by' => '1'],
			['minutes' => '5', 'equivalent' => '0.010', 'created_by' => '1'],
			['minutes' => '6', 'equivalent' => '0.012', 'created_by' => '1'],
			['minutes' => '7', 'equivalent' => '0.015', 'created_by' => '1'],
			['minutes' => '8', 'equivalent' => '0.017', 'created_by' => '1'],
			['minutes' => '9', 'equivalent' => '0.019', 'created_by' => '1'],
			['minutes' => '10', 'equivalent' => '0.021', 'created_by' => '1'],
			['minutes' => '11', 'equivalent' => '0.023', 'created_by' => '1'],
			['minutes' => '12', 'equivalent' => '0.025', 'created_by' => '1'],
			['minutes' => '13', 'equivalent' => '0.027', 'created_by' => '1'],
			['minutes' => '14', 'equivalent' => '0.029', 'created_by' => '1'],
			['minutes' => '15', 'equivalent' => '0.031', 'created_by' => '1'],
			['minutes' => '16', 'equivalent' => '0.033', 'created_by' => '1'],
			['minutes' => '17', 'equivalent' => '0.035', 'created_by' => '1'],
			['minutes' => '18', 'equivalent' => '0.037', 'created_by' => '1'],
			['minutes' => '19', 'equivalent' => '0.040', 'created_by' => '1'],
			['minutes' => '20', 'equivalent' => '0.042', 'created_by' => '1'],
			['minutes' => '21', 'equivalent' => '0.044', 'created_by' => '1'],
			['minutes' => '22', 'equivalent' => '0.046', 'created_by' => '1'],
			['minutes' => '23', 'equivalent' => '0.048', 'created_by' => '1'],
			['minutes' => '24', 'equivalent' => '0.050', 'created_by' => '1'],
			['minutes' => '25', 'equivalent' => '0.052', 'created_by' => '1'],
			['minutes' => '26', 'equivalent' => '0.054', 'created_by' => '1'],
			['minutes' => '27', 'equivalent' => '0.056', 'created_by' => '1'],
			['minutes' => '28', 'equivalent' => '0.058', 'created_by' => '1'],
			['minutes' => '29', 'equivalent' => '0.060', 'created_by' => '1'],
			['minutes' => '30', 'equivalent' => '0.062', 'created_by' => '1'],
			['minutes' => '31', 'equivalent' => '0.065', 'created_by' => '1'],
			['minutes' => '32', 'equivalent' => '0.067', 'created_by' => '1'],
			['minutes' => '33', 'equivalent' => '0.069', 'created_by' => '1'],
			['minutes' => '34', 'equivalent' => '0.071', 'created_by' => '1'],
			['minutes' => '35', 'equivalent' => '0.073', 'created_by' => '1'],
			['minutes' => '36', 'equivalent' => '0.075', 'created_by' => '1'],
			['minutes' => '37', 'equivalent' => '0.077', 'created_by' => '1'],
			['minutes' => '38', 'equivalent' => '0.079', 'created_by' => '1'],
			['minutes' => '39', 'equivalent' => '0.081', 'created_by' => '1'],
			['minutes' => '40', 'equivalent' => '0.083', 'created_by' => '1'],
			['minutes' => '41', 'equivalent' => '0.085', 'created_by' => '1'],
			['minutes' => '42', 'equivalent' => '0.087', 'created_by' => '1'],
			['minutes' => '43', 'equivalent' => '0.090', 'created_by' => '1'],
			['minutes' => '44', 'equivalent' => '0.092', 'created_by' => '1'],
			['minutes' => '45', 'equivalent' => '0.094', 'created_by' => '1'],
			['minutes' => '46', 'equivalent' => '0.096', 'created_by' => '1'],
			['minutes' => '47', 'equivalent' => '0.098', 'created_by' => '1'],
			['minutes' => '48', 'equivalent' => '0.100', 'created_by' => '1'],
			['minutes' => '49', 'equivalent' => '0.102', 'created_by' => '1'],
			['minutes' => '50', 'equivalent' => '0.104', 'created_by' => '1'],
			['minutes' => '51', 'equivalent' => '0.106', 'created_by' => '1'],
			['minutes' => '52', 'equivalent' => '0.108', 'created_by' => '1'],
			['minutes' => '53', 'equivalent' => '0.110', 'created_by' => '1'],
			['minutes' => '54', 'equivalent' => '0.112', 'created_by' => '1'],
			['minutes' => '55', 'equivalent' => '0.115', 'created_by' => '1'],
			['minutes' => '56', 'equivalent' => '0.117', 'created_by' => '1'],
			['minutes' => '57', 'equivalent' => '0.119', 'created_by' => '1'],
			['minutes' => '58', 'equivalent' => '0.121', 'created_by' => '1'],
			['minutes' => '59', 'equivalent' => '0.123', 'created_by' => '1'],
			['minutes' => '60', 'equivalent' => '0.125', 'created_by' => '1'],
        ];

        DB::table('working_minutes_conversion')->insert($data);
    }
}
