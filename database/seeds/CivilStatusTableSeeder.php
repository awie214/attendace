<?php

use Illuminate\Database\Seeder;
use App\CivilStatusModel;

class CivilStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        CivilStatusModel::truncate();

        $civil_status = [
            ['code' => 'M', 'name' => 'Married', 'created_by' => '1'],
            ['code' => 'S', 'name' => 'Single', 'created_by' => '1'],
            ['code' => 'A', 'name' => 'Annulled', 'created_by' => '1'],
            ['code' => 'E', 'name' => 'Separated', 'created_by' => '1'],
        ];

        DB::table('civil_status')->insert($civil_status);
    }
}
