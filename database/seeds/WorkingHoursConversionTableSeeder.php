<?php

use Illuminate\Database\Seeder;
use App\WorkingHoursConversion;

class WorkingHoursConversionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        WorkingHoursConversion::truncate();

        $data = [
            ['hours' => '0', 'equivalent' => '0', 'created_by' => '1'],
			['hours' => '1', 'equivalent' => '0.125', 'created_by' => '1'],
			['hours' => '2', 'equivalent' => '0.250', 'created_by' => '1'],
			['hours' => '3', 'equivalent' => '0.375', 'created_by' => '1'],
			['hours' => '4', 'equivalent' => '0.500', 'created_by' => '1'],
			['hours' => '5', 'equivalent' => '0.625', 'created_by' => '1'],
			['hours' => '6', 'equivalent' => '0.750', 'created_by' => '1'],
			['hours' => '7', 'equivalent' => '0.875', 'created_by' => '1'],
			['hours' => '8', 'equivalent' => '1.000', 'created_by' => '1'],
			['hours' => '9', 'equivalent' => '1.125', 'created_by' => '1'],
			['hours' => '10', 'equivalent' => '1.250', 'created_by' => '1'],
        ];

        DB::table('working_hours_conversion')->insert($data);
    }
}
