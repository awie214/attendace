<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employee_informations')) {
            Schema::create('employee_informations', function (Blueprint $table) {
                $table->increments('id');
                $table->string('employee_id');
                $table->string('biometrics')->nullable();
                $table->string('policy_number')->nullable();
                $table->string('bp_number')->nullable();
                $table->date('hired_date')->nullable()->comment = 'Start date in the government';
                $table->date('assumption_date')->nullable()->comment = 'Start date in the agency';
                $table->date('resign_date')->nullable()->comment = 'End date in the agency';
                $table->date('rehired_date')->nullable()->comment = 'Rehired date in the agency';
                $table->date('start_date')->nullable()->comment = 'Start date in the agency for non-plantilla employees';
                $table->date('end_date')->nullable()->comment = 'End date in the agency for non-plantilla employees';
                $table->integer('work_schedule_id')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_informations');
    }
}
