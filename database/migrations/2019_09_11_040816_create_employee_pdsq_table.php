<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePdsqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employee_pdsq')) {
            Schema::create('employee_pdsq', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->boolean('34a')->nullable();
                $table->boolean('34b')->nullable();
                $table->string('34b_explanation')->nullable();
                $table->boolean('35a')->nullable();
                $table->string('35a_explanation')->nullable();
                $table->boolean('35b')->nullable();
                $table->string('35b_explanation')->nullable();
                $table->date('35b_date_filed')->nullable();
                $table->string('35b_case_status')->nullable();
                $table->boolean('36a')->nullable();
                $table->string('36a_explanation')->nullable();
                $table->boolean('37a')->nullable();
                $table->string('37a_explanation')->nullable();
                $table->boolean('38a')->nullable();
                $table->string('38a_explanation')->nullable();
                $table->boolean('38b')->nullable();
                $table->string('38b_explanation')->nullable();
                $table->boolean('39a')->nullable();
                $table->string('39a_explanation')->nullable();
                $table->boolean('40a')->nullable();
                $table->string('40a_explanation')->nullable();
                $table->boolean('40b')->nullable();
                $table->string('40b_explanation')->nullable();
                $table->boolean('40c')->nullable();
                $table->string('40c_explanation')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_pdsq');
    }
}
