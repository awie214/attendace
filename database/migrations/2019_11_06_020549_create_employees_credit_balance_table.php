<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesCreditBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employees_credit_balance')) {
            Schema::create('employees_credit_balance', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->string('credit_name');
                $table->integer('effectivity_year')->nullable();
                $table->date('effectivity_date')->nullable();
                $table->date('expiry_date')->nullable();
                $table->date('beginning_date')->nullable();
                $table->decimal('beginning_balance',6,2)->nullable();
                $table->integer('force_leave')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_credit_balance');
    }
}
