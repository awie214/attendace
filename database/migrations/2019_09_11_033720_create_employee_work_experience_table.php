<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeWorkExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employee_work_experience')) {
            Schema::create('employee_work_experience', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->date('workexp_start_date')->nullable();
                $table->date('workexp_end_date')->nullable();
                $table->boolean('workexp_present')->nullable();
                $table->integer('position_id')->nullable();
                $table->integer('plantilla_item_id')->nullable();
                $table->integer('agency_id')->nullable();
                $table->integer('location_id')->nullable();
                $table->integer('branch_id')->nullable();
                $table->integer('office_id')->nullable();
                $table->integer('department_id')->nullable();
                $table->integer('division_id')->nullable();
                $table->integer('salary_grade')->nullable();
                $table->integer('salary_step')->nullable();
                $table->integer('employment_status_id')->nullable();
                $table->integer('appointment_status_id')->nullable();
                $table->decimal('monthly_salary',15,2)->nullable();
                $table->boolean('government_service')->nullable();
                $table->string('new_employee_number')->nullable();
                $table->decimal('lwop',5,2)->nullable();
                $table->date('lwop_date')->nullable();
                $table->string('reason')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_work_experience');
    }
}
