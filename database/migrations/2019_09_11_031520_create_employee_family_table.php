<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeFamilyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employee_family')) {
            Schema::create('employee_family', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->string('father_last_name')->nullable();
                $table->string('father_first_name')->nullable();
                $table->string('father_middle_name')->nullable();
                $table->string('father_extension_name')->nullable();
                $table->string('mother_last_name')->nullable();
                $table->string('mother_first_name')->nullable();
                $table->string('mother_middle_name')->nullable();
                $table->string('spouse_last_name')->nullable();
                $table->string('spouse_first_name')->nullable();
                $table->string('spouse_middle_name')->nullable();
                $table->string('spouse_extension_name')->nullable();
                $table->string('occupation_id')->nullable();
                $table->string('employer_name')->nullable();
                $table->string('business_address')->nullable();
                $table->string('telephone_number')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_family');
    }
}
