<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeVoluntaryWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employee_voluntary_work')) {
            Schema::create('employee_voluntary_work', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->date('voluntarywork_start_date')->nullable();
                $table->date('voluntarywork_end_date')->nullable();
                $table->boolean('voluntarywork_present')->nullable();
                $table->integer('organization_id')->nullable();
                $table->integer('voluntarywork_total_hours')->nullable();
                $table->string('work_nature')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_voluntary_work');
    }
}
