<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOvertimePolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('overtime_policy')) {
            Schema::create('overtime_policy', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('overtime_policy_group_id');
                $table->boolean('with_pay')->nullable();
                $table->boolean('forfeited_if_late')->nullable();
                $table->boolean('forfeited_if_undertime')->nullable();
                $table->boolean('forfeited_if_leave')->nullable();
                $table->boolean('forfeited_if_absent_prev_day')->nullable();
                $table->string('prev_days')->nullable();
                $table->integer('max_ot_working_days')->nullable();
                $table->integer('max_ot_weekends')->nullable();
                $table->integer('max_ot_holidays')->nullable();
                $table->integer('max_ot_month')->nullable();
                $table->decimal('coc_rates_working_days',5,2)->nullable();
                $table->decimal('coc_rates_weekends',5,2)->nullable();
                $table->decimal('coc_rates_holidays',5,2)->nullable();
                $table->integer('max_coc_year')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overtime_policy');
    }
}
