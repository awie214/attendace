<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesDeductionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employees_deduction')) {
            Schema::create('employees_deduction', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->date('start_date')->nullable();
                $table->date('end_date')->nullable();
                $table->date('date_terminated')->nullable();
                $table->integer('deduction_id');
                $table->integer('amount')->nullable();
                $table->date('pay_period')->nullable();
                $table->boolean('terminated')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_deduction');
    }
}
