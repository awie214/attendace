<?php

return [

//modules
'personal_info' => 'Personal Information',
'attendance' => 'Attendance',


'module' => 'Module',
'pds' => 'PDS',
'service_record' => 'Service Record',
'attendance' => 'Attendance',
'payroll' => 'Payroll',

// page
'add_new_record' => 'Add New Record',
'update_record' => 'Updating Record',
'delete_record' => 'Deleting Record',
'action' => 'Action',
'options' => 'Options',
'view' => 'View',
'edit' => 'Edit',
'delete' => 'Delete',
'save' => 'Save',
'cancel' => 'Cancel',

'pds_attachment' => 'PDS Attachment',
'add_pds_attachment' => 'Add PDS Attachment',
'download_pds_attachment' => 'Download PDS Attachment',
'delete_pds_attachment' => 'Delete PDS Attachment',


'pds_update' => 'PDS Update',

'add_new_application' => 'Add New Application',
'update_application' => 'Updating Application',
'delete_application' => 'Deleting Application',
'approve_application' => 'Approving Application',

// alerts
'added_successfully' => 'The :attribute has been added successfully!',
'updated_successfully' => 'The :attribute has been updated successfully!',
'deleted_successfully' => 'The :attribute has been deleted successfully!',

'check_inputs' => 'Please check input fields.',
'no_record_found' => 'No Record Found.',

'add_this' => 'Are you sure you want to add this?',
'delete_this' => 'Are you sure you want to delete this?',
'update_this' => 'Are you sure you want to update this?',
'continue_this' => 'Are you sure you want to continue?',
'download_this' => 'Are you sure you want to download this?',
'logout_this' => 'Are you sure you want to logout this?',
'exit_this' => 'Are you sure you want to exit?',
'deactivate_this' => 'Are you sure you want to deactivate this?',

'apply_this' => 'Are you sure you want to apply this?',
'approve_this' => 'Are you sure you want to approve this?',
'cancel_this' => 'Are you sure you want to cancel?',

'do_continue' => 'Do you want to continue?',


// left-nav 
'import' => 'Import Logs',
'home' => 'Home',
'dashboard' => 'Dashboard',
'dashboard0' => 'Dashboard', // Personal Information
'dashboard1' => 'Dashboard', // Attendance
'dashboard2' => 'Dashborad', // Payroll
'menu' => 'Menu',
'file_setup' => 'File Setup',
'pds_file' => 'PDS File',
'201_file' => '201 File',
'201_update' => '201 Update',
'employee_information' => 'Employee Information',
'service_record' => 'Service Record',
'reports' => 'Reports',
'configurations' => 'Configurations',
'list_configuration' => 'List Configurations',
'payroll_employee_setup' => 'Employee Setup',
'employee_policy' => 'Employee Policy',
'loans_info' => 'Loan Info',
'benefits_info' => 'Benefit Info',
'deductions_info' => 'Deduction Info',


/** file-setup **/

//label
'code' => 'Code',
'remarks' => 'Remarks',
'name' => 'Name',

'general' => 'General',

'branch' => 'Branch',
'branches' => 'Branches',
'add_branches' => 'Add New Branch',
'edit_branches' => 'Edit Branch',
'delete_branches' => 'Delete Branch',

'section' => 'Section',
'sections' => 'Sections',
'add_sections' => 'Add New Section',
'edit_sections' => 'Edit Section',
'delete_sections' => 'Delete Section',

'location' => 'Location',
'locations' => 'Locations',
'add_locations' => 'Add New Location',
'edit_locations' => 'Edit Location',
'delete_locations' => 'Delete Location',

'unit' => 'Travel Tax Unit',
'units' => 'Travel Tax Units',
'add_units' => 'Add New Travel Tax Unit',
'edit_units' => 'Edit Travel Tax Unit',
'delete_units' => 'Delete Travel Tax Unit',

// agencies
'agency' => 'Agency',
'agencies' => 'Agencies',
'add_agencies' => 'Add New Agency',
'edit_agencies' => 'Edit Agency',
'delete_agencies' => 'Delete Agency',

//departments
'department' => 'Department',
'departments' => 'Departments',
'add_departments' => 'Add New Department',
'edit_departments' => 'Edit Department',
'delete_departments' => 'Delete Department',


//departments
'designation' => 'Detailed to the other Agency',
'designations' => 'Detailed to the other Agency',
'add_designations' => 'Add New Detailed to the other Agency',
'edit_designations' => 'Edit Detailed to the other Agency',
'delete_designations' => 'Delete Detailed to the other Agency',

//offices
'office' => 'Entity',
'offices' => 'Entities',
'add_offices' => 'Add New Entity',
'edit_offices' => 'Edit Entity',
'delete_offices' => 'Delete Entity',

//divisions
'division' => 'Division',
'divisions' => 'Divisions',
'add_divisions' => 'Add New Division',
'edit_divisions' => 'Edit Division',
'delete_divisions' => 'Delete Division',

//positions
'position' => 'Position',
'positions' => 'Positions',
'add_positions' => 'Add New Position',
'edit_positions' => 'Edit Position',
'delete_positions' => 'Delete Position',

//appointment_status
'appointment_status' => 'Nature of Appointment',
'add_appointment_status' => 'Add New Nature of Appointment',
'edit_appointment_status' => 'Edit Nature of Appointment',
'appointment_type' => 'Appointment Type',
'delete_appointment_status' => 'Delete Nature of Appointment',

//employment_status
'employment_status' => 'Employment Status',
'add_employment_status' => 'Add New Employment Status',
'edit_employment_status' => 'Edit Employment Status',
'delete_employment_status' => 'Delete Employment Status',

//salary_grade
'salary_grade' => 'Salary Grade',
'add_salary_grade' => 'Add New Salary Grade',
'edit_salary_grade' => 'Edit Salary Grade',
'delete_salary_grade' => 'Delete Salary Grade',
'step' => 'Step',
'tranche' => 'Tranche',
'amount' => 'Amount',

//plantilla_items
'plantilla_items' => 'Plantilla Items',
'add_plantilla_items' => 'Add New Plantilla Item',
'edit_plantilla_items' => 'Edit Plantilla Item',
'delete_plantilla_items' => 'Delete Plantilla Item',
'position_id' => 'Position',
'salary_step' => 'Salary Step',
'salary_amount' => 'Salary Amount',
'position_level' => 'Position Level',
'position_classification' => 'Position Classfication',

//countries
'country' => 'Country',
'countries' => 'Countries',
'add_countries' => 'Add New Country',
'edit_countries' => 'Edit Country',
'delete_countries' => 'Delete Country',

//occupations
'occupation' => 'Occupation',
'occupations' => 'Occupations',
'add_occupations' => 'Add New Occupation',
'edit_occupations' => 'Edit Occupation',
'delete_occupations' => 'Delete Occupation',

//citizenships
'citizenship' => 'Citizenship',
'citizenships' => 'Citizenships',
'add_citizenships' => 'Add New Citizenship',
'edit_citizenships' => 'Edit Citizenship',
'delete_citizenships' => 'Delete Citizenship',

//cities
'city' => 'City',
'cities' => 'Cities',
'add_cities' => 'Add New City',
'edit_cities' => 'Edit City',
'delete_cities' => 'Delete City',
'province_id' => 'Province',
'zip_code' => 'Zip Code',

//provinces
'province' => 'Province',
'provinces' => 'Provinces',
'add_provinces' => 'Add New Province',
'edit_provinces' => 'Edit Province',
'delete_provinces' => 'Delete Province',

//schools
'school' => 'School',
'schools' => 'Schools',
'add_schools' => 'Add New School',
'edit_schools' => 'Edit School',
'delete_schools' => 'Delete School',
'primary_level' => 'Primary Level',
'secondary_level' => 'Secondary Level',
'vocational_level' => 'Vocational Level',
'college_level' => 'College Level',
'masters_level' => 'Masters Level',

//courses
'course' => 'Course',
'courses' => 'Courses',
'add_courses' => 'Add New Course',
'edit_courses' => 'Edit Course',
'delete_courses' => 'Delete Course',
'course_level' => 'Course Level',

//eligibilities
'eligibilities' => 'Eligibilities',
'add_eligibilities' => 'Add New Eligibility',
'edit_eligibilities' => 'Edit Eligibility',
'delete_eligibilities' => 'Delete Eligibility',

//organizations
'organization' => 'Organization',
'organizations' => 'Organizations',
'add_organizations' => 'Add New Organization',
'edit_organizations' => 'Edit Organization',
'delete_organizations' => 'Delete Organization',

//providers
'providers' => 'Providers/Sponsor',
'add_providers' => 'Add New Provider/Sponsor',
'edit_providers' => 'Edit Provider/Sponsor',
'delete_providers' => 'Delete Provider/Sponsor',
'training_institution' => 'Training Institution',
'address' => 'Address',
'education' => 'Education',
'expertise' => 'Expertise',
'experience' => 'Experience',

//trainings
'trainings' => 'Training Programs',
'add_trainings' => 'Add New Training Program',
'edit_trainings' => 'Edit Training Program',
'delete_trainings' => 'Delete Training Program',
'training_type' => 'Training Type',

'semester' => 'Semester',
'year' => 'Year',
'provider_id' => 'Provider',
'training_description' => 'Training Description',
'proposed_attendees' => 'Proposed Attendees',
'venue' => 'Venue',
'training_start_date' => 'Start Date',
'training_end_date' => 'End Date',
'cost_per_employee' => 'Cost Per Employee',
'slot' => 'Slots',
'approved_budget' => 'Approved Budget',
'training_deadline_date' => 'Deadline Date',

// benefits
'benefits' => 'Benefits',
'add_benefits' => 'Add New Benefits',
'edit_benefits' => 'Edit Benefit',
'delete_benefits' => 'Delete Benefit',
'tax_type' => 'Tax Type',
'computation_type' => 'Computation Type', 
'amount' => 'Amount',

//adjustments
'adjustments' => 'Adjustments',
'add_adjustments' => 'Add New Adjustment',
'edit_adjustments' => 'Edit Adjustment',
'delete_adjustments' => 'Delete Adjustment',

//deductions
'deduction' => 'Deduction',
'deductions' => 'Deductions',
'add_deductions' => 'Add New Deduction',
'edit_deductions' => 'Edit Deduction',
'delete_deductions' => 'Delete Deduction',
'payroll_group' => 'Payroll Group',

//loans
'loans' => 'Loans',
'add_loans' => 'Add New Loan',
'edit_loans' => 'Edit Loan',
'delete_loans' => 'Delete Loan',
'loan_type' => 'Loan Type',

//responsibility
'responsibility_center' => 'Responsibility Center',
'responsibility_centers' => 'Responsibility Centers',
'add_responsibility_centers' => 'Add New Responsibility Center',
'edit_responsibility_centers' => 'Edit Responsibility Center',
'delete_responsibility_centers' => 'Delete Responsibility Center',

//banks
'bank' => 'Bank',
'banks' => 'Banks',
'add_banks' => 'Add New Bank',
'edit_banks' => 'Edit Bank',
'delete_banks' => 'Delete Bank',
'branch_name' => 'Branch Name',
'account_no' => 'Account No',

// Bank Account
'bank_account' => 'Bank Account',
'bank_accounts' => 'Bank Accounts',
'add_bank_accounts' => 'Add New Bank Account',
'edit_bank_accounts' => 'Edit Bank Account',
'delete_bank_accounts' => 'Delete Bank Account',
'delete_bank_accounts' => 'Delete Bank Account',

//philhealth policy
'philhealth_policy' => 'Philhealth Policy',
'add_philhealth_policy' => 'Add New Philhealth Policy',
'edit_philhealth_policy' => 'Edit Philhealth Policy',
'delete_philhealth_policy' => 'Delete Philhealth Policy',

'pay_period' => 'Pay Period',
'semi_monthly' => 'Semi-Monthly',
'monthly' => 'Monthly',

'deduction_period' => 'Deduction Period',
'both' => 'Both',
'first_half' => 'First Half',
'second_half' => 'Second Half',

'policy_type' => 'Policy Type',
'system_generated' => 'System Generated',
'inputted' => 'Inputted',

'based_on' => 'Based on',
'monthly_salary' => 'Monthly Salary',
'gross_salary' => 'Gross Salary',
'gross_taxable' => 'Gross taxable',

//pagibig policy
'pagibig_policy' => 'Pagibig Policy',
'add_pagibig_policy' => 'Add New Pagibig Policy',
'edit_pagibig_policy' => 'Edit Pagibig Policy',
'delete_pagibig_policy' => 'Delete Pagibig Policy',

//gsis policy
'gsis_policy' => 'GSIS Policy',
'add_gsis_policy' => 'Add New GSIS Policy',
'edit_gsis_policy' => 'Edit GSIS Policy',
'delete_gsis_policy' => 'Delete GSIS Policy',

// wage rate
'wage_rate' => 'Wage Rate',
'add_wage_rate' => 'Add New Wage Rate',
'edit_wage_rate' => 'Edit Wage Rate',
'delete_wage_rate' => 'Delete Wage Rate',
'effective_date' => 'Effective Date',

// tax_table
'tax_table' => 'Tax Table',
'add_tax_table' => 'Add New Tax Table',
'edit_tax_table' => 'Edit Tax Table',
'delete_tax_table' => 'Delete Tax Table',

'salary_bracket_level1' => 'Salary Bracket Level 1',
'salary_bracket_level2' => 'Salary Bracket Level 2',
'salary_bracket_level3' => 'Salary Bracket Level 3',
'salary_bracket_level4' => 'Salary Bracket Level 4',
'salary_bracket_level5' => 'Salary Bracket Level 5',
'salary_bracket_level6' => 'Salary Bracket Level 6',

'daily' => 'Daily',
'weekly' => 'Weekly',
'semi_monthly' => 'Semi-Monthly',
'monthly' => 'Monthly',

'compensation_level' => 'Compensation Level',
'minimum_withholding_tax' => 'Minimum Withholding Tax',

// annual tax table
'annual_tax_table' => 'Annual Tax Table',
'add_annual_tax_table' => 'Add New Annual Tax Table',
'edit_annual_tax_table' => 'Edit Annual Tax Table',
'delete_annual_tax_table' => 'Delete Annual Tax Table',

// tax policy

'tax_policy' => 'Tax Policy',

//absences

'absences' => 'Absences',
'add_absences' => 'Add New Absence',
'edit_absences' => 'Edit Absence',
'delete_absences' => 'Delete Absence',
'absence_type' => 'Absence Type',

//leaves
'leaves' => 'Leaves',
'add_leaves' => 'Add New Leave',
'edit_leaves' => 'Edit Leave',
'delete_leaves' => 'Delete Leave',

//Holidays
'holiday' => 'Holiday',
'holidays' => 'Holidays',
'add_holidays' => 'Add New Holiday',
'edit_holidays' => 'Edit Holiday',
'delete_holidays' => 'Delete Holiday',
'start_date' => 'Start Date',
'end_date' => 'End Date',
'legal_holiday' => 'Is legal holiday?',
'yearly' => 'Applied Every Year',

//office_suspensions
'office_suspensions' => 'Office Suspensions',
'add_office_suspensions' => 'Add New Office Suspension',
'edit_office_suspensions' => 'Edit Office Suspension',
'delete_office_suspensions' => 'Delete Office Suspension',

'start_time' => 'Start time',
'whole_day' => 'Is whole day?',
'time_in_required' => 'Is time in required?',

//providers
'providers' => 'Providers/Sponsor',
'add_providers' => 'Add New Provider/Sponsor',
'edit_providers' => 'Edit Provider/Sponsor',
'delete_providers' => 'Delete Provider/Sponsor',
'training_institution' => 'Training Institution',
'address' => 'Address',
'education' => 'Education',
'expertise' => 'Expertise',
'experience' => 'Experience',

//trainings
/**
'trainings' => 'Trainings',
'add_trainings' => 'Add New Training',
'training_type' => 'Training Type',
'semester' => 'Semester',
'year' => 'Year',
'provider_id' => 'Provider',
'training_description' => 'Training Description',
'proposed_attendees' => 'Proposed Attendees',
'venue' => 'Venue',
'training_start_date' => 'Start Date',
'training_end_date' => 'End Date',
'cost_per_employee' => 'Cost Per Employee',
'slot' => 'Slots',
'approved_budget' => 'Approved Budget',
'training_deadline_date' => 'Deadline Date',**/

//work_schedules
'work_schedules' => 'Work Schedules',
'add_work_schedules' => 'Add New Work Schedule',
'edit_work_schedules' => 'Edit Work Schedule',
'delete_work_schedules' => 'Delete Work Schedule',
'schedule_type' => 'Schedule Type',
'am_in' => 'AM (In)',
'mid_out' => 'MID (Out)',
'mid_in' => 'MID (In)',
'pm_out' => 'PM (Out)',
'strict_mid' => 'Strict MID',
'flexi_time' => 'Flexi Time',
'restday' => 'Restday',

'monday' => 'Monday',
'tuesday' => 'Tuesday',
'wednesday' => 'Wednesday',
'thursday' => 'Thursday',
'friday' => 'Friday',
'saturday' => 'Saturday',
'sunday' => 'Sunday',

// Sectors
'sectors' => 'Sectors',
'add_sectors' => 'Add New Sector',
'edit_sectors' => 'Edit Sector',
'delete_sectors' => 'Delete Sector',
'sector' => 'Sector',

// Signatories
'signatories' => 'Signatories',
'add_signatories' => 'Add New Signatory',
'edit_signatories' => 'Edit Signatory',
'delete_signatories' => 'Delete Signatory',
'signatory' => 'Signatory',


// personal info
'employee_no' => 'Employee No',
'last_name'=> 'Last Name',
'first_name' => 'First Name',

'add-201-file' => 'Add New 201 File',
'add-201-update' => 'Add New 201 Update',

'add_201_file' => 'Add New 201 File',
'add_201_update' => 'Add New 201 Update',

'family_children_info' => 'Family & Children Info',
'educ_eligibility' => 'Educ & Eligibility',
'work_experience' => 'Work Experience',
'merberships_training' => 'Memberships & Trainings',
'references' => 'References',

'if_any' => '(if any)',
'optional' => ' (optional)',
'same_as_above' => 'Same as above',
'please_select' => '--- Please Select ---',
'please_select_option' => 'Please select option',
'please_specify' => 'If Other/s, Please specify',

'personal_information' => 'Personal Information',
'surname' => 'Surname',
'middle_name' => 'Middle Name',
'ext_name' => 'Ext Name',
'sex' => 'Sex',
'civil_status' => 'Civil Status',
'birth_date' => 'Birth Date',
'birth_place' => 'Birth Place',
'height' => 'Height (m)',
'weight' => 'Weight (Kg)',
'blood_type' => 'Blood Type',
'gsis_id_no' => 'GSIS ID No.',
'pagibig_id_no' => 'PAGIBIG ID No.',
'philhealth_no' => 'PHILHEALTH No.',
'sss_no' => 'SSS No.',
'tin_no' => 'TIN No.',
'agency_emp_no' => 'Agency Employee No.',
'tel_no' => 'Telephone No.',
'mobile_no' => 'Mobile No.',
'contact_no' => 'Contact No',
'email_address' => 'E-mail Address',
'is_filipino' => 'Is Filipino?',
'dual_citizenship' => 'Dual Citizenship?',
'residential_address' => 'Residential Address',
'permanent_address' => 'Permanent Address',

'house_blk_lot_no' => 'House/Block/Lot No.',
'street' => 'Street',
'subdivision' => 'Subdivision/Village',
'barangay' => 'Barangay',
'city_municipality' => 'City/Municipality',
'province' => 'Province',
'zip_code' => 'Zip Code',

'residential_house_number' => 'Residential House Number',
'residential_street' => 'Residential Street',
'residential_subdivision' => 'Residential Subdivision',
'residential_brgy' => 'Residential Brgy',
'residential_city' => 'Residential City',

'permanent_house_number' => 'Permanent House Number',
'permanent_street' => 'Permanent Street',
'permanent_subdivision' => 'Permanent Subdivision',
'permanent_brgy' => 'Permanent Brgy',
'permanent_city' => 'Permanent City',

'father_last_name' => 'Father Last Name',
'father_first_name' => 'Father First Name',
'father_middle_name' => 'Father Middle Name',
'father_extension_name' => 'Father Ext Name',
'mother_last_name' => 'Mother Last Name',
'mother_first_name' => 'Mother First Name',
'mother_middle_name' => 'Mother Middle Name',
'spouse_last_name' => 'Spouse Last Name',
'spouse_first_name' => 'Spouse First Name',
'spouse_middle_name' => 'Spouse Middle Name',
'spouse_extension_name' => 'Spouse Ext Name',
'spouse_occupation' => 'Spouse Occupation',
'spouse_employer_name' => 'Spouse Employer Name',
'spouse_business_address' => 'Spouse Business Address',
'spouse_telephone_number' => 'Spouse Telephone Number',
'telephone_number' => 'Telephone Number',


'male' => 'Male',
'female' => 'Female',

'yes' => 'Yes',
'no' => 'No',

'nickname' => 'Nickname',

'field_name' => 'Field Name',
'old_value' => 'Old Value',
'new_value' => 'New Value',

'add_201_update' => 'Add New 201 Update',

'list_employees' => 'List of Employees',
'filter_value' => 'Filter Value',

'edit_employee_information' => 'Edit Employee Information',

'employee_service_record' => 'Employee Service Record',
'add_employee_service_record' => 'Add New Service Record',
'delete_employee_service_record' => 'Delete Service Record',
'edit_employee_service_record' => 'Edit Service Record',

//Attendance Related
'leave-policy' => 'Leave Policy',
'overtime-policy' => 'Overtime Policy',
'leave-policy-group' => 'Leave Policy Group',
'overtime-policy-group' => 'Overtime Policy Group',
'employee-setup' => 'Employee Setup',
'daily-time-record' => 'Employee\'s DTR',
'employee-credit-balance' => 'Emp. Credit Balance',
'change-shift-application' => 'Change Shift Application',
'overtime-application' => 'Overtime Application',
'leave-cancellation-application' => 'Leave Cancellation',
'leave-application' => 'Leave Application',
'cto-application' => 'CTO Application',
'office-authority-application' => 'Office Authority Application',
'leave-monetization-application' => 'Leave Monetization',
'attendance-reports' => 'Reports',

'application' => 'Application',
'policy' => 'Policy',

'payroll' => 'Payroll',
'annual_tax_setup' => 'Annual Tax Setup',
'payroll_employee_info' => 'Payroll Employee Info',
'loan_history' => 'Loan History',
'transaction' => 'Transaction',
'regular_payroll' => 'Regular Payroll',
'plantilla' => 'Plantilla',
'non_plantilla' => 'Non Plantilla',
'overtime_pay' => 'Overtime Pay',

'description' => 'Description',

'total_loan' => 'Total Loan',
'loan_balance' => 'Loan Balance',
'amortization' => 'Amortization',

'benefits_info' => 'Benefits Info',
'add_benefits_info' => 'Add New Benefit',
'add_loan_info' => 'Add New Loan',
'add_deduction' => 'Add New Deduction',

'report' => 'Report',
'report_name' => 'Report Name',

'201_attachment' => '201 Attachment',
'file_name' => 'File Name',
'file_type' => 'File Type',

'add_201_attachment' => 'Add New 201 Attachment',
'delete_201_attachment' => 'Delete 201 Attachment',
'download_201_attachment' => 'Download 201 Attachment',

'profile' => 'Profile',
'profile_picture' => 'Profile Picture',
'image_path' => 'Image',
'start_downloading' => 'The File is start downloading...',

'created_at' => 'Created At',


'employer_name' => 'Employer Name',
'business_address' => 'Business Address',	




'edit_pds_file' => 'Edit PDS File',
'add_pds_file' => 'Add New PDS File',

'add_children' => 'Add Children Information',
'edit_children' => 'Edit Children Information',
'view_children' => 'View Children Information',

'add_education' => 'Add Educational Background',
'edit_education' => 'Edit Educational Background',
'view_education' => 'View Educational Background',


'add_eligibility' => 'Add Civil Service Eligibility',
'edit_eligibility' => 'Edit Civil Service Eligibility',
'view_eligibility' => 'View Civil Service Eligibility',

'category_status' => 'Category Status',


'other_info' => 'Other Information',
'add_other_info' => 'Add Other Information',
'edit_other_info' => 'Edit Other Information',
'view_other_info' => 'View Other Information',


'reference' => 'Reference',
'add_reference' => 'Add Reference',
'edit_reference' => 'Edit Reference',
'view_reference' => 'View Reference',

'voluntary_work' => 'Voluntary Work',
'add_voluntary_work' => 'Add Voluntary Work',
'edit_voluntary_work' => 'Edit Voluntary Work',
'view_voluntary_work' => 'View Voluntary Work',

'training_program' => 'Training Program Attended',
'add_training_program' => 'Add Training Program Attended',
'edit_training_program' => 'Edit Training Program Attended',
'view_training_program' => 'View Training Program Attended',

'work_experience' => 'Work Experience',
'add_work_experience' => 'Add Work Experience',
'edit_work_experience' => 'Edit Work Experience',
'view_work_experience' => 'View Work Experience',

'family_background' => 'Family Background',


'training_types' => 'Training Types',
'add_training_types' => 'Add Training Types',
'edit_training_types' => 'Edit Training Type',
'delete_training_types' => 'Delete Training Type',

'not_found' => 'The :attribute is not found.',
'record' => 'Record',
'view_pds_file' => 'View PDS File',
'view_employee_service_record' => 'View Employee Service Record',


'announcements' => 'Announcements',
'announcement' => 'Announcement',
'add_announcement' => 'Add New Announcement',
'edit_announcement' => 'Edit Announcement',
'delete_announcement' => 'Delete Announcement',

'quotations' => 'Quotes',
'quotation' => 'Quote',
'add_quotation' => 'Add New Quote',
'edit_quotation' => 'Edit Quote',
'delete_quotation' => 'Delete Quote',
'set_active' => 'Set Active',

'status' => 'Status',

'system_config' => 'System Config',

'user_account' => 'User Account',
'admin_account' => 'Admin Account',

'assignment' => 'Place of Assignment',
'assignments' => 'Place of Assignments',
'add_assignments' => 'Add New Place of Assignment',
'edit_assignments' => 'Edit Place of Assignment',
'delete_assignments' => 'Delete Place of Assignment',


'tshirt_size' => 'T shirt Size',

'hr_action' => 'HR Action',
'change_rate' => 'Change of Rate',
'add_change_rate' => 'Add New Change of Rate',
'edit_change_rate' => 'Edit Change of Rate',
'delete_change_rate' => 'Delete Change of Rate',
'effective_date' => 'Effective Date',
'old_rate' => 'Old Rate',
'new_rate' => 'New Rate',


'reassignment' => 'Reassignment',
'reassignment_no' => 'Reassignment No',
'add_reassignment' => 'Add New Reassignment',
'edit_reassignment' => 'Edit Reassignment',
'delete_reassignment' => 'Delete Reassignment',

'designation' => 'Designation',
'position_title' => 'Position Title',
'add_designation' => 'Add New Designation',
'edit_designation' => 'Edit Designation',
'delete_designation' => 'Delete Designation',

'multiple_training' => 'Multiple Training',
'add_multiple_training' => 'Adding Multiple Trainings',

'renewal' => 'Renewal For Non Plantilla',
'add_renewal' => 'Add New Renewal',

'rate' => 'Rate',

// System Config
'administrator' => 'Administrator',
'add_administrator' => 'Add New Administrator',
'edit_administrator' => 'Edit Administrator',
'delete_administrator' => 'Delete Administrator',

'employee' => 'Employee',
'add_employee' => 'Add New Employee',
'edit_employee' => 'Edit Employee',

'username' => 'Username',
'old_password' => 'Old Password',
'new_password' => 'New Password',
'confirm_password' => 'Confirm New Password',
'user_status' => 'Employee Type',

'employee_type' => 'Employee Type',

// Account
'account' => 'Change Password',
'edit_account' => 'Edit Account',
'logout' => 'Logout',


// Payroll
'edit_employee_policy' => 'Edit Employee Policy',
'add_employee_policy' => 'Add Employee Policy',

'government_policy_contribution' => 'Government Policy & Contribution',

'loan_info' => 'Loan Info', 
'add_loan_info' => 'Add New Loan',
'edit_loan_info' => 'Edit Loan',
'delete_loan_info' => 'Delete Loan',

'loan' => 'Loan',
'date_granted' => 'Date Granted',
'date_terminated' => 'Date Terminated',
'total_loan_amount' => 'Total Loan Amount',
'loan_balance' => 'Loan Balance',
'amortization' => 'Amortization',
'terminated' => 'Terminated',

'benefit' => 'Benefit',
'benefit_info' => 'Benefit Info',
'add_benefit_info' => 'Add New Benefit',
'edit_benefit_info' => 'Edit Benefit',
'delete_benefit_info' => 'Delete Benefit',

'deduction_info' => 'Deduction Info',
'add_deduction_info' => 'Add New Deduction',
'edit_deduction_info' => 'Edit Deduction',
'delete_deduction_info' => 'Delete Deduction',



];