		@if(Auth::user()->pw_update == 0)
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label" style="color: red;">Change password for the first time***</label>
                        
                    </div>
                </div>
            </div>
        </div>
        @endif

		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.old_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="old_password" id="old_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.new_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="new_password" id="new_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.confirm_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="confirm_password" id="confirm_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        @section('additional-scripts')
            <script type="text/javascript">
                $("#save_btn").click(function() {
                    var frm, text, title, success;

                    frm = document.querySelector('#edit_form');
                    title = "{{ __('page.edit_'.$option) }}";
                    text = "{{ __('page.update_this') }}";
                    success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";

                    Swal.fire({
                        title: title    ,
                        text: text,
                        confirmButtonClass: 'btn btn-primary',
                        showCloseButton: true,
                        showCancelButton: true,
                        customClass: 'colored-header colored-header-primary'
                    }).then((result) => {
                        if(result.value){
                            var formData = new FormData();

                            @foreach($default_inputs as $key => $val)
                                formData.append("{{ $val['input_name'] }}", $("#{{ $val['input_name'] }}").val());
                            @endforeach

                            axios.post(frm.action, formData)
                            .then((response) => {
                                Swal.fire({
                                    text: success,
                                    type: 'success',
                                    customClass: 'content-text-center',
                                    showConfirmButton: true,
                                    timer: 1500
                                }).then((value) => {
                                    @if(Auth::user()->level == 0)
                                        window.location.href="{{ url('/attendance/daily-time-record') }}";
                                    @elseif(Auth::user()->level == 1)
                                        window.location.href="{{ url('/home') }}";
                                    @endif
                                	
                                });
                            })
                            .catch((error) => {
                                if(typeof(error.response.data.errors) == 'string') {
                                    Swal.fire({
                                    text: error.response.data.errors,
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false
                                    });

                                    return false;
                                }

                                Swal.fire({
                                    text: "{{ __('page.check_inputs') }}",
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                                const errors = error.response.data.errors
                                const firstItem = Object.keys(errors)[0]
                                const firstItemDOM = document.getElementById(firstItem)
                                const firstErrorMessage = errors[firstItem][0]
                                firstItemDOM.scrollIntoView()
                                
                                clearErrors()

                                if(jQuery.inArray(firstItem, ['test']) !== -1)
                                {
                                    $('#frm_input_' + firstItem).addClass('has-error')
                                    document.getElementById('error-' + firstItem).insertAdjacentHTML('afterend', `<div class="text-danger">${firstErrorMessage}</div>`)
                                }
                                else
                                {
                                	firstItemDOM.insertAdjacentHTML('afterend', `<div class="text-danger">${firstErrorMessage}</div>`)
                                	firstItemDOM.classList.add('border', 'border-danger')
                                }
                            });
                           
                            
                        }
                    })
                });

				function clearErrors() {
                    const errorMessages = document.querySelectorAll('.text-danger')
                    errorMessages.forEach((element) => element.textContent = '')

                    const formControls = document.querySelectorAll('.form-control')
                    formControls.forEach((element) => element.classList.remove('border', 'border-danger'))

                    const formGroups = document.querySelectorAll('.form-group')
                    formGroups.forEach((element) => element.classList.remove('has-error'))
               	}
            </script>
        @endsection