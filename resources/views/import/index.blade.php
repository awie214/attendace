@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
    <link rel="stylesheet" href="{{ asset('beagle-assets/lib/sweetalert2/sweetalert2.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('beagle-assets/css/app.css') }}" type="text/css"/>

@endsection

@section('content')
    @include('others.main_content', [
    'option' => $option, 
    'title' => $title, 
    'has_icon' => $icon,
    'has_file' => $file,
    'has_footer' => 'yes', 
    ])
@endsection

@section('scripts')
    @yield('additional-scripts')
    <script src="{{ asset('/axios/axios.min.js') }}"></script>
    <script src="{{ asset('beagle-assets/lib/sweetalert2/sweetalert2.min.js') }}" type="text/javascript"></script>

    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        Swal = Swal.mixin({
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-secondary',
            buttonsStyling: false
        });
        $(document).ready(function(){
            $("#create_file").click(function () {
                Swal.fire({
                    title: "Import Attendance Logs",
                    text: "Do you want to create import file?",
                    confirmButtonText: 'Confirm',
                    confirmButtonClass: 'btn btn-success',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-success'
                }).then((result) => {
                    if (result.value) {
                        axios.post("{{url('import/')}}/create-file", {
                            date_from : $("#date_from").val(),
                            date_to : $("#date_to").val(),
                        })
                        .then((response) => {
                            Swal.fire({
                                text: "File is ready to download.",
                                type: 'success',
                                customClass: 'content-text-center',
                                confirmButtonClass: 'btn',
                            }).then(function (result) {
                                $("#download_file").attr('href','{{ asset("attendance/attendance-logs.json") }}');
                            });
                        })
                        .catch((error) => {
                            Swal.fire({
                                text: error.response.data.errors,
                                type: 'warning',
                                customClass: 'content-text-center',
                                showConfirmButton: false
                            });
                        });
                    }
                })
            });
        });
    </script>
@endsection
