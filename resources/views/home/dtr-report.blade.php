<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
    <link rel="stylesheet" href="{{ asset('beagle-assets/css/app.css') }}" type="text/css"/>
    <style type="text/css">
        body {
            font-family: cambria;
            font-size: 8pt;
            background: white;
            font-weight: 600;
        }
        .rpt-title {
            font-size: 10pt;
            letter-spacing: 2px;
            font-weight: 600;
        }
        th {
            text-align: center;
            background: #c3bbbb;
        }
        td {
            padding: 0px;
            font-size: 6pt;
            white-space: nowrap;
        }
        .rpt-footer {
            width: 100%;
            height: 100px;
            background-image: url("{{ asset('img/footer.png') }}");
            background-repeat: no-repeat;
            background-size: 100%;

        }
        .header {
             background-size: 100%;
             background-image:url("{{ asset('img/header.png') }}");
             background-repeat: no-repeat;
        }
        .border {border: 1px solid black;}
        .border-bottom-black { border-bottom: 1px solid black; }
       
        @media print {
            .noPrint {display: none;}
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="row noPrint margin-top">
                <div class="col-lg-6 text-right">
                    <button type="button" class="btn btn-success btn-space" onclick="self.print()">Print DTR</button>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <img src="{{ asset('/beagle-assets/img/tieza-small-logo.jpg') }}" alt="logo" height="70px">
                            <br>
                            <b>TOURISM INFRASTRUCTURE AND ENTERPRISE ZONE AUTHORITY</b>
                            <br><br>
                            <b>DAILY TIME RECORD</b>
                            <br>
                            <b>{{ $month }} {{ $year }}</b> 
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-2 col-2">
                            <b>
                                Name
                            </b>
                        </div>
                        <div class="col-lg-1 col-1">
                            :
                        </div>
                        <div class="col-lg-9 col-8">
                            <b>
                                {{ $employee->last_name }}, {{ $employee->first_name }} {{ $employee->middle_name[0] }}.
                            </b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-2">
                            <b>
                                ID&nbsp;No.
                            </b>
                        </div>
                        <div class="col-lg-1 col-1">
                            :
                        </div>
                        <div class="col-lg-9 col-8">
                            <b>
                                {{ $employee->employee_number }}
                            </b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-2">
                            <b>
                                Time
                            </b>
                        </div>
                        <div class="col-lg-1 col-1">
                            :
                        </div>
                        <div class="col-lg-9 col-8">
                            <b>
                                {{ $data['summary']['work_schedule_name'] }}
                            </b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-3">
                            <b>
                                Position
                            </b>
                        </div>
                        <div class="col-lg-1 col-1">
                            :
                        </div>
                        <div class="col-lg-9 col-8">
                            <b>
                                 
                            </b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-3">
                            <b>
                                Department
                            </b>
                        </div>
                        <div class="col-lg-1 col-1">
                            :
                        </div>
                        <div class="col-lg-9 col-8">
                            <b>
                                 
                            </b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-3">
                            <b>
                                Division
                            </b>
                        </div>
                        <div class="col-lg-1 col-1">
                            :
                        </div>
                        <div class="col-lg-9 col-8">
                            <b>
                                 
                            </b>
                        </div>
                    </div>
                    
                    <div class="row margin-top">
                        <div class="col-lg-12">
                            <table width="100%" border="1">
                                <thead>
                                    <tr>
                                        <th style="width: 14%;" rowspan="2">Day</th>
                                        <th colspan="2">A.M.</th>
                                        <th colspan="2">P.M.</th>
                                        <th>TA/UT</th>
                                        <th style="width: 30%;" rowspan="2">REMARKS</th>
                                    </tr>
                                    <tr>
                                        <th style="width: 11%;">Arrival</th>
                                        <th style="width: 11%;">Departure</th>
                                        <th style="width: 11%;">Arrival</th>
                                        <th style="width: 11%;">Departure</th>
                                        <th style="width: 10%;">hr:mm</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @php
                                        foreach ($data['attendance'] as $key => $value) {

                                            $attendance_date    = $value['attendance_date'];
                                            $day                = $value['day'];
                                            $day_name           = $value['day_name'];
                                            $week               = $value['week'];
                                            $time_in            = $value['time_in'];
                                            $lunch_out          = $value['lunch_out'];
                                            $lunch_in           = $value['lunch_in'];
                                            $time_out           = $value['time_out'];
                                            $tardy              = $value['tardy'];
                                            $undertime          = $value['undertime'];
                                            $excess_time        = $value['excess_time'];
                                            $consume_time       = $value['consume_time'];
                                            $remarks            = $value['remarks'];
                                            $total_tardy_ut     = $value['total_tardy_ut'];

                                            if ($time_in == '00:00') $time_in = ':';
                                            if ($lunch_out == '00:00') $lunch_out = ':';
                                            if ($lunch_in == '00:00') $lunch_in = ':';
                                            if ($time_out == '00:00') $time_out = ':';
                                            if ($tardy == '00:00') $tardy = ':';
                                            if ($time_in == '00:00') $time_in = ':';
                                            if ($total_tardy_ut == '00:00') $total_tardy_ut = ':';


                                            echo '<tr style="font-size: 8pt;">';
                                            echo '<td style="height: 24px; padding-right: 2px;" class="text-right">
                                                '.$value['dtr_date'].'
                                            </td>';
                                            echo '<td class="text-center">'.$time_in.'</td>';
                                            echo '<td class="text-center">'.$lunch_out.'</td>';
                                            echo '<td class="text-center">'.$lunch_in.'</td>';
                                            echo '<td class="text-center">'.$time_out.'</td>';
                                            echo '<td class="text-center">'.$total_tardy_ut.'</td>';
                                            echo '<td class="text-center">'.$remarks.'</td>';
                                            echo '</tr>';

                                        }
                                    @endphp
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <div class="row margin-top">
                        <div class="col-lg-12">
                            <table width="100%" style="font-size: 7pt;">
                                <tr>
                                    <td colspan="9" class="text-center">
                                        <b>TOTAL SUMMARY</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:10%">
                                        Tardiness
                                        <br>
                                        TA Freq
                                        <br>
                                        Undertime
                                        <br>
                                        UT Freq
                                        <br>
                                        Total TA/UT
                                    </td>
                                    <td style="width:3%">
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                    </td>
                                    <td style="width:12%">
                                        {{ $data['summary']['total_tardy'] }} | {{ $data['summary']['tardy_equivalent'] }}
                                        <br>
                                        {{ $data['summary']['tardy_count'] }}
                                        <br>
                                        {{ $data['summary']['total_undertime'] }} | {{ $data['summary']['undertime_equivalent'] }}
                                        <br>
                                        {{ $data['summary']['undertime_count'] }}
                                        <br>
                                        {{ $data['summary']['undertime_equivalent'] + $data['summary']['tardy_equivalent'] }}
                                    </td>
                                    <td style="width:15%" valign="top">
                                        Days Worked (DW)
                                        
                                    </td>
                                    <td style="width:3%" valign="top">
                                        :
                                        
                                    </td>
                                    <td style="width:12%" valign="top">
                                        {{ $data['summary']['present_count'] }}
                                    </td>
                                    <td style="width: 45%;">&nbsp;</td>
                                    
                                </tr>
                            </table>
                        </div>
                    </div> -->
                    <div class="row margin-top">
                        <div class="col-md-12">
                            <table width="100%" style="font-size: 5pt;">
                                <tr>
                                    <td colspan="9" class="text-center">
                                        <b>TOTAL SUMMARY</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:15%">
                                        Days Worked (DW)
                                        <br>
                                        Absences
                                        <br>
                                        Overtime
                                        <br>
                                        Total Days Worked
                                        <br>
                                        Less TA/UT
                                    </td>
                                    <td style="width:3%">
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                        <br>
                                        &nbsp;
                                        <br>
                                        :
                                    </td>
                                    <td style="width:12%">
                                        {{ $data['summary']['present_count'] }}
                                        <br>
                                        {{ $data['summary']['absent_count'] }}
                                        <br>
                                        0
                                        <br>
                                        &nbsp;
                                        <br>
                                        0
                                    </td>

                                    <td style="width:15%">
                                        Tardiness
                                        <br>
                                        TA Freq
                                        <br>
                                        Undertime
                                        <br>
                                        UT Freq
                                        <br>
                                        Total TA/UT
                                    </td>
                                    <td style="width:3%">
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                    </td>
                                    <td style="width:12%">
                                        {{ $data['summary']['total_tardy'] }} | {{ $data['summary']['tardy_equivalent'] }}
                                        <br>
                                        {{ $data['summary']['tardy_count'] }}
                                        <br>
                                        {{ $data['summary']['total_undertime'] }} | {{ $data['summary']['undertime_equivalent'] }}
                                        <br>
                                        {{ $data['summary']['undertime_count'] }}
                                        <br>
                                        {{ $data['summary']['undertime_equivalent'] + $data['summary']['tardy_equivalent'] }}
                                    </td>

                                    <td style="width:15%">
                                        Leave
                                        <br>
                                        Restday
                                        <br>
                                        Special Holiday
                                        <br>
                                        Legal Holiday
                                        <br>
                                        No Lunch
                                    </td>
                                    <td style="width:3%">
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                        <br>
                                        :
                                    </td>
                                    <td style="width:12%">
                                        {{ $data['summary']['leave_count'] }}
                                        <br>
                                        {{ $data['summary']['restday_count'] }}
                                        <br>
                                        {{ $data['summary']['special_holiday_count'] }}
                                        <br>
                                        {{ $data['summary']['legal_holiday_count'] }}
                                        <br>
                                        {{ $data['summary']['no_lunch_count'] }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-lg-12" style="text-indent: 5%;">
                            I CERTIFY on my honor that the above is a true and correct report of the hours of work performed, record of which was made daily at the time of arrival and departure from office.
                        </div>
                    </div>
                    <br>
                    <div class="row margin-top">
                        <div class="col-lg-12 text-center">
                            <b>
                                _______________________________________________
                                <br>
                                Employee's Signature
                            </b>
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-lg-12 text-center">
                            <b>
                                _______________________________________________
                                <br>
                                Verified as to the prescribed office hours. (In-Charge)
                            </b>
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-lg-6">
                            <b>

                                Printed by: 
                            </b>
                        </div>
                        <div class="col-lg-6">
                            <b>
                                Verified by:
                            </b>
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-lg-6">
                            RUNDATE: {{ date("Y.m.d",time()) }}
                        </div>
                        <div class="col-lg-6">
                            RUNTIME: {{ date("H:i:s",time()) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-12 rpt-footer">

                </div>
            </div>
        </div>
    </div>
    @include('layouts.auth-partials.scripts')
    @yield('scripts')
</body>

</html>