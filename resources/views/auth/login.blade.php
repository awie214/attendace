@extends('layouts.master-guest')

@section('css')
    <style type="text/css">
        .event-background{
            background: url("beagle-assets/img/bg-test.png");
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #007acc;
            color: white;
            text-align: center;
            margin: 0;
            font-size: 12pt;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center text-light h-25"><h3 class="mb-4">Online Attendance System</h3></div>
        </div>

        <div class="row">
            <div class="col-xl-6 offset-xl-1 col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-8 text-center">
                <img src="{{ asset('img/tieza-logo.png') }}" alt="logo" class="ml-0 img-fluid" style="width: 30em;">
            </div>
            <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="splash-container">
                    <div class="card card-border-color card-border-color-primary">
                        <div class="card-header">
                            <span class="splash-description">Please enter your user information.</span>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <input id="username" type="text" placeholder="Username" autocomplete="off" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                                    @if($errors->has('username'))
                                        <ul class="parsley-errors-list filled" id="parsley-id-5">
                                            <li class="parsley-required">{{ $errors->first('username') }}</li>
                                        </ul>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                                    @if($errors->has('password'))
                                        <ul class="parsley-errors-list filled" id="parsley-id-5">
                                            <li class="parsley-required">{{ $errors->first('password') }}</li>
                                        </ul>
                                    @endif
                                </div>

                                <div class="form-group row login-tools">
                                    <div class="col-4 login-remember">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" {{ old('remember') ? 'checked' : '' }}><span class="custom-control-label">Remember Me</span>
                                        </label>
                                    </div>
                                    
                                </div>

                                <div class="form-group login-submit">
                                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Sign me in</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
    <div class="row footer">
        <div class="col-lg-12 text-center">
            &copy; Copyright 2020<br>Management Information System Department
        </div>
    </div>
@endsection