@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
    <link rel="stylesheet" href="{{ asset('beagle-assets/lib/sweetalert2/sweetalert2.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('beagle-assets/css/app.css') }}" type="text/css"/>

@endsection

@section('content')
    @include('others.main_content', [
    'option' => $option, 
    'title' => $title, 
    'has_icon' => $icon,
    'has_file' => $file,
    'has_footer' => 'yes', 
    ])
@endsection

@section('scripts')
    @yield('additional-scripts')
    <script src="{{ asset('/axios/axios.min.js') }}"></script>
    <script src="{{ asset('beagle-assets/lib/sweetalert2/sweetalert2.min.js') }}" type="text/javascript"></script>

    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        Swal = Swal.mixin({
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-secondary',
            buttonsStyling: false
        });
        $(document).ready(function(){
            load_datables('#employee_tbl', '{{$json_url}}', {!! json_encode($columns) !!});
        });

        function reset_pw(id)
        {
            var frm = document.querySelector('#delete_emp_' + id);

            Swal.fire({
                title: "Reset Employee Password",
                text: "Do you want to reset password?",
                confirmButtonText: 'Confirm',
                confirmButtonClass: 'btn btn-success',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-success'
            }).then((result) => {
                if (result.value) {
                    axios.post("{{url('user/')}}/" + id + "/reset-pw")
                    .then((response) => {
                        Swal.fire({
                            text: "Password Successfully Reset",
                            type: 'success',
                            customClass: 'content-text-center',
                            confirmButtonClass: 'btn',
                        });

                        load_datables('#employee_tbl', '{{$json_url}}', {!! json_encode($columns) !!});
                    })
                    .catch((error) => {
                        Swal.fire({
                            text: error.response.data.errors,
                            type: 'warning',
                            customClass: 'content-text-center',
                            showConfirmButton: false
                        });
                    });
                }
            })
        }
        function delete_employee(id)
        {
            var frm = document.querySelector('#delete_emp_' + id);

            Swal.fire({
                title: "Delete Employee Data",
                text: "{{ __('page.delete_this') }}",
                confirmButtonText: 'Confirm',
                confirmButtonClass: 'btn btn-danger',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-danger'
            }).then((result) => {
                if (result.value) {
                    axios.post(frm.action)
                    .then((response) => {
                        Swal.fire({
                            text: "Successfully Deleted",
                            type: 'success',
                            customClass: 'content-text-center',
                            confirmButtonClass: 'btn',
                        });

                        load_datables('#employee_tbl', '{{$json_url}}', {!! json_encode($columns) !!});
                    })
                    .catch((error) => {
                        Swal.fire({
                            text: error.response.data.errors,
                            type: 'warning',
                            customClass: 'content-text-center',
                            showConfirmButton: false
                        });
                    });
                }
            })
        }
    </script>
@endsection
