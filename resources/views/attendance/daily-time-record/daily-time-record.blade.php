<div class="row">
        <div class="col-lg-12">
            <div class="card card-border-color card-border-color-primary card-table">
                <div class="card-header card-header-divider">
                    <div class="row">
                        <div class="col-lg-6">
                            <b>List of Employees</b>
                        </div>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2 text-right">
                            <select class="form-control form-control-xs"
                                    id="month"
                                    name="month">
                                    <option value="0">MONTH</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <select class="form-control form-control-xs criteria"
                                    id="year"
                                    name="year">
                                    <option value="0">YEAR</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                            </select>
                        </div>
                    </div>
                  
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="employee_tbl" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr class="text-center">
                                        <th style="width: 20%;">{{ __('page.action') }}</th>
                                        <th style="width: 20%;">Last Name</th>
                                        <th style="width: 20%;">First Name</th>
                                        <th style="width: 20%;">Employee Number</th>
                                        <th style="width: 20%;">Department</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
                <div class="card-footer">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>