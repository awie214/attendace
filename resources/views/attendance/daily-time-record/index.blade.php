@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
    <link rel="stylesheet" href="{{ asset('beagle-assets/lib/sweetalert2/sweetalert2.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('beagle-assets/css/app.css') }}" type="text/css"/>

@endsection
@section('content')
    @include(
        'others.main_content', 
        [
            'option' => $option, 
            'module' => $module,
            'title' => $option,
            'has_icon' => 'icon mdi mdi-time', 
            'has_file' => 'attendance.daily-time-record.daily-time-record'
        ]
    )
@endsection

@section('scripts')
    <script src="{{ asset('/axios/axios.min.js') }}"></script>
    <script src="{{ asset('beagle-assets/lib/sweetalert2/sweetalert2.min.js') }}" type="text/javascript"></script>

    @include('layouts.auth-partials.datatables-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //initialize the javascript
            App.init();
            load_datables('#employee_tbl', '{{$json_url}}', {!! json_encode($columns) !!});
            $("#year").val("{{ date('Y',time()) }}").trigger('change');
            $("#month").val("{{ date('m',time()) }}").trigger('change');
        });
        function viewDTR(id)
        {
            var year = $("#year").val();
            var month = $("#month").val();

            var href = window.location + '/attendance/' + id + '/' + month + '/' + year + '/preview-dtr';
            window.open(href, '_blank');
        }
    </script>
@endsection
