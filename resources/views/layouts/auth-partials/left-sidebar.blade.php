<!-- Left Sidebar -->
<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper">
        <a href="#" class="left-sidebar-toggle">{{ __('page.'.$module) }}</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">{{ __('page.menu') }}</li>
                        @if(Auth::user()->level == 0)
                            @include('others.sidebar_li', ['selected_module' => $module, 'menu_name' => 'daily-time-record', 'url' => '/attendance/daily-time-record', 'icon' => 'icon mdi mdi-time'])
                        @elseif(Auth::user()->level == 1)
                            @include('others.sidebar_li', ['selected_module' => $module, 'menu_name' => 'home', 'url' => '/home', 'icon' => 'icon mdi mdi-home'])
                        @elseif(Auth::user()->level == 2)
                            @include('others.sidebar_li', 
                                [
                                    'selected_module' => $module,
                                    'menu_name' => 'user_account',
                                    'url' => '/user',
                                    'icon' => 'icon mdi mdi-account'
                                ]
                            )
                            @include('others.sidebar_li', 
                                [
                                    'selected_module' => $module,
                                    'menu_name' => 'admin_account',
                                    'url' => '/admin',
                                    'icon' => 'icon mdi mdi-account'
                                ]
                            )
                            @include('others.sidebar_li', 
                                [
                                    'selected_module' => $module,
                                    'menu_name' => 'import',
                                    'url' => '/import',
                                    'icon' => 'icon mdi mdi-storage'
                                ]
                            )
                        @endif
                        @include('others.sidebar_li', 
                            [
                                'selected_module' => $module,
                                'menu_name' => 'account',
                                'url' => '/account',
                                'icon' => 'icon mdi mdi-key'
                            ]
                        )
                        </a>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

