<div class="row">
        <div class="col-lg-12">
            <div class="card card-border-color card-border-color-primary card-table">
                <div class="card-header card-header-divider">
                    <div class="row">
                        <div class="col-lg-6">
                            <b>List of Admins</b>
                        </div>
                        <div class="col-lg-6 text-right">
                            <a href="{{ url('admin/create') }}" class="btn btn-space btn-info">
                                <i class="icon icon-left mdi mdi-plus"></i>{{ __('page.add_new_record') }}
                            </a>
                        </div>

                    </div>
                  
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="employee_tbl" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr class="text-center">
                                        <th style="width: 20%;">{{ __('page.action') }}</th>
                                        <th style="width: 20%;">Name</th>
                                        <th style="width: 20%;">Username</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
                <div class="card-footer">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>