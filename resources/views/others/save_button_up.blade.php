			
			<a href="javascript:void(0);" class="btn btn-space btn-primary" title="{{ __('page.save') }}" id="save_btn"><i class="icon icon-left mdi mdi-file-plus"></i>{{ __('page.save') }}</a>
            <a href="{{ $cancel_url }}" class="btn btn-space btn-danger" title="{{ __('page.cancel') }}"><i class="icon icon-left mdi mdi-close"></i>{{ __('page.cancel') }}</a>