<div class="page-header">
    <img src="{{ asset('img/tieza_header.jpg') }}" width="100%" height="80%" />
    <div class="centered-address" data-html="true"><b>6th & 7th Floors, Tower 1 <br>
    Double Dragon Plaza <br>
    Double Dragon Meridian Park <br>
    Macapagal Avenue corner <br>
    Edsa Extension<br>
    1302 Bay Area, Pasay City<br></b>
    </div>
    <div class="centered-contact" data-html="true" style="top: 28%;">
        &nbsp;<b>(+632) 8249-5900 Local-625</b>
    </div>

    <div class="centered-contact" data-html="true" style="top: 49%;color:#4285f4">
        <b>adminservices@tieza.gov.ph</b>
    </div>
    <div class="centered-contact" data-html="true" style="top: 62%;">
        <b>www.tieza.gov.ph</b>
    </div>     
</div>

<div class="page-footer" style="width: 100%;">
    <img src="{{ asset('img/footer.png') }}" width="100%">
</div>