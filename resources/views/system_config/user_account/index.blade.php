@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    <aside class="page-aside">
        <div class="aside-content">
            <div class="aside-header py-2 d-xl-none d-lg-none">
                    <button class="navbar-toggle" data-target="#aside-spy" data-toggle="collapse" type="button"><span class="icon mdi mdi-caret-down"></span></button><h1>&nbsp;&nbsp;List of Account</h1>
            </div>
            <div class="aside-nav collapse" id="aside-spy">
                @if($option == 'employee')
                    @include('others.list_employees')
                @elseif($option == 'administrator')
                    @include('others.list_administrator')
                @endif
            </div>
        </div>
    </aside>

    @php 
        $data = [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_frm' => 'yes',
        'has_footer' => 'yes', 
        'isSave' => 'yes',
        'frm_method' => 'POST',
        'frm_action' => url('system_config/'.$option.'/update'),
        'frm_id' => 'edit_form',
        'cancel_url' => url('system_config/'.$option) 
        ];

        if($option == 'administrator') $data['add_url'] = $system_config_url.'/'.$option.'/create';
    @endphp

    @include('others.main_content', $data)
@endsection

@section('scripts')
    @yield('additional-scripts')

    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            $('#main-content').addClass('be-aside');
        });
    </script>
@endsection