        
        <div class="form-group row" id="frm_input_name">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">{{ __('page.name')}}</label>
                    <select class="select2 select2-xs" 
                            name="name" 
                            id="name">   
                            <option value="">{{ __('page.please_select') }}</option>
                            @foreach($list_employees as $key => $val)
                                <option value="{{ $val->id }}">{{ $val->full_name }}</option>
                            @endforeach
                    </select>
                <div class="select_error" id="error-name"></div>
            </div>
        </div>

		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.username') }}</label>

                        <div class="float-right"><a href="javascript:void(0);" id="populate_id" onclick="populate_agency_id()"><span class="mdi mdi-refresh"></span>&nbsp;Populate Agency ID</a></div>

                        <input type="text" class="form-control form-control-xs" name="username" id="username" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.email_address') }}</label>
                        <input type="email" class="form-control form-control-xs" name="email_address" id="email_address" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.new_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="new_password" id="new_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.confirm_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="confirm_password" id="confirm_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row" id="frm_input_employee_type">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">{{ __('page.employee_type')}}</label>
                    <select class="select2 select2-xs" 
                           	name="employee_type" 
                           	id="employee_type">   
                            <option value="">{{ __('page.please_select') }}</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                    </select>
                <div class="select_error" id="error-employee_type"></div>
            </div>
        </div>

       	<input type="hidden" name="emp_id" id="emp_id" />

        @section('additional-scripts')
            <script type="text/javascript">

                $(document).ready(function(){
                    @if($frm_id == 'add_form')
                        $('#name').attr('disabled', false);
                    @elseif($frm_id == 'edit_form')
                        $('#name').attr('disabled', true);
                    @endif
                });

                $("#save_btn").click(function() {

                    var frm, text, title, success, emp_id;

                    frm = document.querySelector('#edit_form');

                    emp_id = $('#emp_id').val();

                    if(emp_id)
                    {
                        title = "{{ __('page.edit_'.$option) }}";
                        text = "{{ __('page.update_this') }}";
                        success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    }
                    else
                    {
                        title = "{{ __('page.add_'.$option) }}";
                        text = "{{ __('page.add_this') }}";
                        success = "{{ __('page.added_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    }

                    const swal_continue = alert_continue(title, text);
                    swal_continue.then((result) => {
                        if(result.value){
                            clearErrors();

                            var formData = new FormData();

                            formData.append('emp_id', emp_id)

                            @foreach($default_inputs as $key => $val)
                                formData.append("{{ $val['input_name'] }}", $("#{{ $val['input_name'] }}").val());
                            @endforeach

                            axios.post(frm.action, formData)
                            .then((response) => {
                                const swal_success = alert_success(success);
                                swal_success.then((value) => {
                                    clearErrors();
                                });
                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                                else
                                {
                                    alert_warning("{{ __('page.check_inputs') }}");

                                    const firstItem = Object.keys(errors)[0];
                                    const firstItemDOM = document.getElementById(firstItem);
                                    const firstErrorMessage = errors[firstItem][0];

                                    firstItemDOM.scrollIntoView();
                                    showErrors(firstItem, firstErrorMessage, firstItemDOM, ['user_status', 'name']);
                                }
                            });
                        }
                    })
                });

                function select_employee(id)
                {
                    clearErrors();
                    clearInputs();

                    $('#employee_type').attr('disabled', false);

                    axios.get("{{ url('/employee/account') }}/" + id + '/json')
                    .then(function(response){
                        const employee = response.data.employee;

                        $("#emp_id").val(employee['id'])
                        $('#name').val(employee['employee_id']).trigger('change')
                        $('#username').val(employee['username'])
                        $('#email_address').val(employee['email'])
                        $('#new_password').val('')
                        $('#confirm_password').val('')
                        $('#employee_type').val(employee['user_status']).trigger('change')

                        if(employee['id'] == {{ Auth::user()->id }})
                        {
                            $('#employee_type').attr('disabled', true);
                        }
                    }).catch((error) => {
                        const errors = error.response.data.errors;

                        if(typeof(errors) == 'string')
                        {
                            alert_warning(errors);

                            if(errors == '{{ __('page.no_record_found') }}')
                            {
                                const swal_warning = alert_warning(errors);
                                
                                swal_warning.then((result) => {
                                    const swal_continue = alert_continue("{{ __('page.add_'.$option) }}", "{{ __('page.do_continue') }}");

                                    swal_continue.then((result) => {
                                        if(result.value){
                                            $('#name').val(id).trigger('change');
                                        }
                                    });
                                });

                                return false;
                            }
                        }
                    });
                }

                function populate_agency_id()
                {
                    var id = $('#name').val();

                    axios.get("{{ url('/employees/json') }}/" + id)
                    .then(function(response){
                        const employee = response.data.employee;

                        $('#username').val(employee['employee_number']);
                    }).catch((error) => {
                        if(typeof(error.response.data.errors) == 'string') {
                            Swal.fire({
                                text: error.response.data.errors,
                                type: 'warning',
                                customClass: 'content-text-center',
                                showConfirmButton: false,
                                timer: 1500
                            });

                            return false;
                        }
                    });
                }
            </script>
        @endsection