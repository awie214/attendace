		
		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.name') }}</label>
                        <input type="text" class="form-control form-control-xs" name="name" id="name" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.username') }}</label>
                        <input type="text" class="form-control form-control-xs" name="username" id="username" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.email_address') }}</label>
                        <input type="email" class="form-control form-control-xs" name="email_address" id="email_address" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.new_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="new_password" id="new_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.confirm_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="confirm_password" id="confirm_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row" id="frm_input_employee_type">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">{{ __('page.employee_type')}}</label>
                    <select class="select2 select2-xs" 
                           	name="employee_type" 
                           	id="employee_type">   
                            <option value="">{{ __('page.please_select') }}</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                    </select>
                <div class="select_error" id="error-employee_type"></div>
            </div>
        </div>

       	<input type="hidden" name="emp_id" id="emp_id" />

        @section('additional-scripts')
            <script type="text/javascript">
                $("#save_btn").click(function() {

                    var frm, text, title, success;


                    @if($frm_id == 'add_form')
                    	frm = document.querySelector('#add_form');
                        title = "{{ __('page.add_'.$option) }}";
                        text = "{{ __('page.add_this') }}";
                        success = "{{ __('page.added_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    @elseif($frm_id == 'edit_form')
                        frm = document.querySelector('#edit_form');
                        title = "{{ __('page.edit_'.$option) }}";
                        text = "{{ __('page.update_this') }}";
                        success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    @endif


                    Swal.fire({
                        title: title    ,
                        text: text,
                        confirmButtonClass: 'btn btn-primary',
                        showCloseButton: true,
                        showCancelButton: true,
                        customClass: 'colored-header colored-header-primary'
                    }).then((result) => {
                        if(result.value){
                        	var emp_id = $('#emp_id').val();
                            var formData = new FormData();

                            formData.append('emp_id', emp_id)

                            @foreach($default_inputs as $key => $val)
                                formData.append("{{ $val['input_name'] }}", $("#{{ $val['input_name'] }}").val());
                            @endforeach

                            axios.post(frm.action, formData)
                            .then((response) => {
                                Swal.fire({
                                    text: success,
                                    type: 'success',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((value) => {
                                	@if($frm_id == 'add_form')
                                		Swal.fire({
                                        title: "{{ __('page.add_'.$option) }}",
                                        text: "Do you want to add another {{ trans('page.'.$option) }}?",
                                        confirmButtonClass: 'btn btn-primary',
                                        showCloseButton: true,
                                        showCancelButton: true,
                                        customClass: 'colored-header colored-header-primary'
                                        }).then((result) => {
                                            if (result.value) {
                                                @foreach($default_inputs as $key => $val) 
                                                    $('#{{ $val['input_name'] }}').val('').trigger('change') 
                                                @endforeach

                                                clearErrors()
                                            }else {
                                                window.location.href="{{ $cancel_url }}";
                                            }
                                        })
                                    @else
                                    	select_account(emp_id)
                                    @endif
                                });
                            })
                            .catch((error) => {
                                if(typeof(error.response.data.errors) == 'string') {
                                    Swal.fire({
                                    text: error.response.data.errors,
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false
                                    });

                                    return false;
                                }

                                Swal.fire({
                                    text: "{{ __('page.check_inputs') }}",
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                                const errors = error.response.data.errors
                                const firstItem = Object.keys(errors)[0]
                                const firstItemDOM = document.getElementById(firstItem)
                                const firstErrorMessage = errors[firstItem][0]
                                firstItemDOM.scrollIntoView()
                                
                                clearErrors()

                                if(jQuery.inArray(firstItem, ['employee_type']) !== -1)
                                {
                                    $('#frm_input_' + firstItem).addClass('has-error')
                                    document.getElementById('error-' + firstItem).insertAdjacentHTML('afterend', `<div class="text-danger">${firstErrorMessage}</div>`)
                                }
                                else
                                {
                                	firstItemDOM.insertAdjacentHTML('afterend', `<div class="text-danger">${firstErrorMessage}</div>`)
                                	firstItemDOM.classList.add('border', 'border-danger')
                                }
                            });
                           
                            
                        }
                    })
                });

				function clearErrors() {
                    const errorMessages = document.querySelectorAll('.text-danger')
                    errorMessages.forEach((element) => element.textContent = '')

                    const formControls = document.querySelectorAll('.form-control')
                    formControls.forEach((element) => element.classList.remove('border', 'border-danger'))

                    const formGroups = document.querySelectorAll('.form-group')
                    formGroups.forEach((element) => element.classList.remove('has-error'))
               	}

               	@if($frm_id == 'edit_form')
	                function select_account(id)
	                {
	                	clearErrors()

	                	$('#employee_type').attr('disabled', false);

	                	axios.get("{{ url('/administrator') }}/" + id + '/json')
	                    .then(function(response){
	                    	const administrator = response.data.administrator;

	                    	$("#emp_id").val(administrator['id'])
	                    	$('#name').val(administrator['name'])
	                        $('#username').val(administrator['username'])
	                        $('#email_address').val(administrator['email'])
	                        $('#new_password').val('')
	                        $('#confirm_password').val('')
	                        $('#employee_type').val(administrator['user_status']).trigger('change')

	                        if(administrator['id'] == {{ Auth::user()->id }})
	                        {
	                        	$('#employee_type').attr('disabled', true);
	                        }
	                    }).catch((error) => {
	                        if(typeof(error.response.data.errors) == 'string') {
	                            Swal.fire({
	                                text: error.response.data.errors,
	                                type: 'warning',
	                                customClass: 'content-text-center',
	                                showConfirmButton: false,
	                                timer: 1500
	                            });

	                            return false;
	                        }
	                    });
	                }
                @endif
            </script>
        @endsection