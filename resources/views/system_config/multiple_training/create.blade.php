@extends('layouts.master-auth')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/multiselect/css/multi-select.css') }}"/>
@endsection

@section('content')
    @include('others.main_content', [
    'option' => $option, 
    'title' => 'add_'.$option, 
    'has_icon' => 'mdi mdi-plus-circle',
    'has_file' => $file,
    'has_footer' => 'yes', 
    'isSave' => 'yes', 
    'cancel_url' => $cancel_url,
    'has_frm' => 'yes',
    'frm_method' => 'POST',
    'frm_action' => $frm_action,
    'frm_id' => 'add_form'
    ])
</div>
@endsection

@section('scripts')
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/multiselect/js/jquery.multi-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/lib/quicksearch/jquery.quicksearch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-assets/js/app-form-multiselect.js') }}" type="text/javascript"></script>

   	@yield('additional-scripts')

   	<script type="text/javascript">
        $(document).ready(function(){
            App.init();
            App.formElements();
            load_list_employees();
            $('#employee-list').multiSelect({
                selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search'>",
                selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' disabled>",
                afterInit: function(ms){
                    var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                    }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
                },
                afterSelect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });
      	});
        function load_list_employees()
        {
            $("#employee-list").empty();
            axios.get("{{ url('/employees/json') }}")
            .then(function(response){
                const employees     = response.data.employees;
                const total_record  = response.data.count_employee;
                var str = '';
                $.each(employees, function( key, value ) {
                    str += "<option value='"+ value['id'] +"'>"+ value['last_name'] + ", " + value['first_name'] +"</option>";
                });
                $("#employee-list").append(str);
                $('#employee-list').multiSelect( 'refresh' );
            })
            .catch(function(error){
                Swal.fire({
                    text: "Something error. Please contact administrator.",
                    type: 'warning',
                    customClass: 'content-text-center',
                    showConfirmButton: false,
                    timer: 1500
                });  
            })
        }
    </script>
@endsection