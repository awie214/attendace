        
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.quotation') }}</label>
                        <textarea class="form-control form-control-xs" name="quotation_" id="quotation_"></textarea>
                    </div>
                </div>
            </div>
        </div>

        @section('additional-scripts')
            <script type="text/javascript">
                $("#save_btn").click(function() {

                    var frm, text, title, success;

                    @if(isset($item_id))
                        frm = document.querySelector('#edit_form');
                        title = "{{ __('page.edit_'.$option) }}";
                        text = "{{ __('page.update_this') }}";
                        success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    @else
                        frm = document.querySelector('#add_form');
                        title = "{{ __('page.add_'.$option) }}";
                        text = "{{ __('page.add_this') }}";
                        success = "{{ __('page.added_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    @endif


                    Swal.fire({
                        title: title    ,
                        text: text,
                        confirmButtonClass: 'btn btn-primary',
                        showCloseButton: true,
                        showCancelButton: true,
                        customClass: 'colored-header colored-header-primary'
                    }).then((result) => {
                        if(result.value){
                            var formData = new FormData();

                            @foreach($default_inputs as $key => $val)
                                formData.append("{{ $val['input_name'] }}", $("#{{ $val['input_name'] }}").val());
                            @endforeach

                            axios.post(frm.action, formData)
                            .then((response) => {
                                Swal.fire({
                                    text: success,
                                    type: 'success',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((value) => {
                                    @if(isset($item_id))
                                        window.location.href="{{ $cancel_url }}";
                                    @else
                                        Swal.fire({
                                        title: "{{ __('page.add_'.$option) }}",
                                        text: "Do you want to add another {{ trans('page.'.$option) }}?",
                                        confirmButtonClass: 'btn btn-primary',
                                        showCloseButton: true,
                                        showCancelButton: true,
                                        customClass: 'colored-header colored-header-primary'
                                        }).then((result) => {
                                            if (result.value) {
                                                @foreach($default_inputs as $key => $val) 
                                                    $('#{{ $val['input_name'] }}').val('').trigger('change') 
                                                @endforeach

                                                clearErrors()
                                            }else {
                                                window.location.href="{{ $cancel_url }}";
                                            }
                                        })
                                    @endif
                                });
                            })
                            .catch((error) => {
                                if(typeof(error.response.data.errors) == 'string') {
                                    Swal.fire({
                                    text: error.response.data.errors,
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false
                                    });

                                    return false;
                                }

                                Swal.fire({
                                    text: "{{ __('page.check_inputs') }}",
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                                const errors = error.response.data.errors
                                const firstItem = Object.keys(errors)[0]
                                const firstItemDOM = document.getElementById(firstItem)
                                const firstErrorMessage = errors[firstItem][0]
                                firstItemDOM.scrollIntoView()
                                
                                clearErrors()

                                firstItemDOM.insertAdjacentHTML('afterend', `<div class="text-danger">${firstErrorMessage}</div>`)
                                firstItemDOM.classList.add('border', 'border-danger')
                            });
                           
                            function clearErrors() {
                                const errorMessages = document.querySelectorAll('.text-danger')
                                errorMessages.forEach((element) => element.textContent = '')

                                const formControls = document.querySelectorAll('.form-control')
                                formControls.forEach((element) => element.classList.remove('border', 'border-danger'))

                            }
                        }
                    })
                });

                @if(isset($item_id))
                     $(document).ready(function(){
                        load_quotation({{ $item_id }});
                    });

                    function load_quotation(id)
                    {
                        axios.get("{{ url('/quotation') }}/" + id + '/json')
                        .then(function(response){
                            $('#quotation_').val(response.data.quotation.quotation).trigger('change')
                        }).catch((error) => {
                            if(typeof(error.response.data.errors) == 'string') {
                                Swal.fire({
                                    text: error.response.data.errors,
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                                return false;
                            }
                        }); 
                    }
                @endif
            </script>
        @endsection