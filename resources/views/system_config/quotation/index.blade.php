@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    @include('others.main_content', ['option' => $option, 'title' => $option, 'has_icon' => $icon, 'has_file' => $file, 'add_url' => $system_config_url.'/'.$option.'/create'])
@endsection

@section('scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.dataTables();

            load_datables('#{{ $default_table_id }}', "{{ $default_json_url }}", {!! json_encode($default_columns) !!}, 1, 'asc');
        });

            function delete_record(id)
            {
                Swal.fire({
                    title: "{{ __('page.delete_'.$option) }}",
                    text: "{{ __('page.delete_this') }}",
                    confirmButtonClass: 'btn btn-danger',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-danger'
                }).then((result) => {
                    if (result.value) {
                        axios.post("{{ url('/system_config/quotation') }}/" + id + '/delete')
                        .then((response) => {
                            Swal.fire({
                                text: "{{ __('page.deleted_successfully', ['attribute' => trans('page.quotation')]) }}",
                                type: 'success',
                                customClass: 'content-text-center',
                                confirmButtonClass: 'btn',
                            }).then((result) => {
                                load_datables('#{{ $default_table_id }}', "{{ $default_json_url }}", {!! json_encode($default_columns) !!}, 1, 'asc');
                            });
                        })
                        .catch((error) => {
                            Swal.fire({
                                text: error.response.data.errors,
                                type: 'warning',
                                customClass: 'content-text-center',
                                showConfirmButton: false
                            });
                        });
                    }
                })

                return true;
            }

            function set_active(id)
            {
                Swal.fire({
                    title: "{{ __('page.set_active') }}",
                    text: "{{ __('page.apply_this') }}",
                    confirmButtonClass: 'btn btn-primary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-primary'
                }).then((result) => {
                    if (result.value) {
                        axios.post("{{ url('/quotation') }}/" + id + '/set_active')
                        .then((response) => {
                            Swal.fire({
                                text: "{{ __('page.updated_successfully', ['attribute' => trans('page.quotation')]) }}",
                                type: 'success',
                                customClass: 'content-text-center',
                                confirmButtonClass: 'btn',
                            }).then((result) => {
                                load_datables('#{{ $default_table_id }}', "{{ $default_json_url }}", {!! json_encode($default_columns) !!}, 1, 'asc');
                            });
                        })
                        .catch((error) => {
                            Swal.fire({
                                text: error.response.data.errors,
                                type: 'warning',
                                customClass: 'content-text-center',
                                showConfirmButton: false
                            });
                        });
                    }
                })

                return true;
            }

    </script>
@endsection