@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    @include('others.main_content', ['option' => $option, 'title' => $option, 'has_icon' => $icon, 'has_file' => $file, 'add_url' => $system_config_url.'/'.$option.'/create'])
@endsection

@section('scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.dataTables();

            @if($option == 'announcement')
                load_datables('#{{ $default_table_id }}', "{{ $default_json_url }}", {!! json_encode($default_columns) !!}, 2, 'asc');
            @endif
        });

        @if($option == 'announcement')
            function delete_record(id)
            {
                Swal.fire({
                    title: "{{ __('page.delete_'.$option) }}",
                    text: "{{ __('page.delete_this') }}",
                    confirmButtonClass: 'btn btn-danger',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-danger'
                }).then((result) => {
                    if (result.value) {
                        axios.post("{{ url('/system_config/announcement') }}/" + id + '/delete')
                        .then((response) => {
                            Swal.fire({
                                text: "{{ __('page.deleted_successfully', ['attribute' => trans('page.announcement')]) }}",
                                type: 'success',
                                customClass: 'content-text-center',
                                confirmButtonClass: 'btn',
                            }).then((result) => {
                                load_datables('#{{ $default_table_id }}', "{{ $default_json_url }}", {!! json_encode($default_columns) !!}, 2, 'asc');
                            });
                        })
                        .catch((error) => {
                            Swal.fire({
                                text: error.response.data.errors,
                                type: 'warning',
                                customClass: 'content-text-center',
                                showConfirmButton: false
                            });
                        });
                    }
                })

                return true;
            }
        @endif

    </script>
@endsection
